#!/usr/bin/env python

import argparse
from peel import server

parser = argparse.ArgumentParser(description='Orange Peel Server')

parser.add_argument('-a', '--address',
                    dest='address',
                    default='127.0.0.1',
                    help='IP address to run the server under')
parser.add_argument('-p', '--port',
                    dest='port',
                    default=8000,
                    help='Port to run the server under')
args = parser.parse_args()

if __name__ == '__main__':

    server.service.start(address=args.address,
                 port=args.port)