import requests
import os
import os.path
import exceptions as err
import requests.exceptions
import threading
import socket
import util
import const
import time



class Client(object):
    def __init__(self, host=None, port=5000):
        self.host = host or "localhost"
        self.port = port

        d = self.ping()
        self.site = d["site"]
        self.connect_seconds = d["seconds"]

    def ping(self):
        """
        Get a brief response from the site daemon. Usually used to check that it's online.
        @returns A brief amount of information about this site daemon.
        """
        r = self._get("", raw=True)
        d = util.convert_response(r.json())
        d["seconds"] = util.total_seconds(r.elapsed)
        return d

    def get_config(self):
        """
        Get configuration data for the site.
        @returns The configuration data for the site.
        """
        return self._get("config")

    def get_sites(self, update=False):
        """
        Find running Freight daemons.
        @param update If True, update our local site cache by pinging the other sites.
        @returns A list of other visible daemons, in ascending ping-time order.
        @note The current site is always the first item in the list.
        """
        u = util.Param(update) if update else None
        doc = self._get("sites", update=u)
        return doc["sites"]

    def get_files(self):
        """
        Get list of docs describing files at this site.
        @returns List of docs describing files at this site.
        """
        doc = self._get("files")
        return doc["files"]

    def get_file(self, path):
        """
        Get information about a file at this site.
        @param path File path.
        @returns Doc describing the file.
        """
        return self._get("files", path=path)

    def set_file_hash(self, path, lmd, filesize, hsh):
        """
        Set the hash for the given file, if the given lmd and filesize are correct.
        @param path File path.
        @param lmd last-modified-time of the file (datetime object).
        @param filesize Size of the file.
        @param hsh Hash to associate with the file. This would usually be a SHA1, but the
            actual hash function to use is up to the client.
        @returns The new file doc if correct. A FreightIntegrityError is thrown if the provided
            file details (lmd, filesize) don't match the file.
        """
        return self._put("files",
            path=path,
            last_modified=lmd,
            filesize=filesize,
            hash=hsh)

    def get_traffic(self, edges=None, restrict=False, start=None, stop=None, step=None):
        """
        Get the total current inbound and outbound bandwidth usage, in KBps.
        @param edges A list of (src_site,dest_site) tuples. Only files being copied between these
            sites are added to the total. None means 'any site'. For example, [('la',None)] would
            give all outbound traffic from LA; [('la',None),(None,'la')] would give all outbound
            and inbound traffic to LA. [(None,None)] is a special case, which gives all outbound
            and inbound traffic for all sites. When None, defaults to all inbound and outbound 
            traffic to the current site only.
        @param restrict If True, only return bandwidth info for exactly the edges asker for. If
            False, extra edges, that still fall inside of the requested edges, are returned. For
            example, the edge ('la','london') overlaps with ('la',None).
        @param start The time to sample bandwidth at, as a datetime object. A negative float is
            also accepted, and in this case the sample will be this many seconds in the past. If
            None, bandwidth is sampled at the current time.
        @param stop Return samples at step intervals between start and stop. Like 'start', can be
            a datetime object, a negative float, or None. Ignored if step is None.
        @param step Time between samples, in seconds (float is supported). This is also the size
            of each sample - bandwidth is averaged over each step.
        @returns A dict of bandwidth samples keyed by edge. Eg: {('la',None): [403.5]}
        """
        r = util.Param(restrict) if restrict else None
        doc = self._get("traffic", \
            edges=edges, \
            restrict=r,
            start=start,
            stop=stop,
            step=step
        )

        traffic = {}
        for k,v in doc.iteritems():
            toks = k.split(',')
            assert(len(toks) == 2)
            src,dest = [(None if x=="null" else str(x)) for x in toks]
            traffic[(src,dest)] = v

        return traffic

    def get_log_entries(self, handle, since=None):
        """
        Get the log entries associated with a file copy or transfer.
        @param handle Handle of the object being queried (eg a copy or transfer)
        @param since Only return log entries created after this datetime (utc).
        @returns List of log entries.
        """
        doc = self._get("logs/%s" % handle, since=since)
        return doc["logs"]

    def copy_file(self, src_path, dest_path, src_site=None, dest_site=None, \
        src_host=None, dest_host=None, transfer_backend=None, test_only=False, block=False, \
        verbose=False, callback=None, **backend_settings):
        """
        Copy a file from one site to another using the current site as controller.
        @param src_path The path(s) to copy the file from. Can be a string or list.
        @param dest_path The path to copy the file to.
        @param src_site The site to copy the file from, current site if None.
        @param dest_site The site to copy the file to, current site if None.
        @param src_host The host to copy the file from. If None, src_path is expected to be on a
            NAS that is visible to the freight daemon and related services at the source site.
        @param dest_host The host to copy the file onto. If None, dest_path is expected to be on a
            NAS that is visible to the freight daemon and related services at the dest site.
        @param transfer_backend The backend to execute the copy with. If not supplied, the
            default will be used according to this site's config settings.
        @param test_only Only test if the copy is possible. Returns None.
        @param block If True, block the calling thread until the copy is complete. In this case,
            the return value of the function is a pair (handle,doc), where doc is the final status
            of the copy.
        @param verbose If True, print progress and log entries for the copy to stdout.
        @param backend_settings Extra runtime settings for the transfer backend.
        @returns A file copy handle.
        """
        dtb_settings = util.strip_nulls(backend_settings) or None

        # make all paths absolute - relative path with non-local site or host is invalid
        if hasattr(src_path, "__iter__"):
            src_path_ = [self._make_abs(x, src_site, src_host) for x in src_path]
            src_paths = src_path
        else:
            src_path_ = self._make_abs(src_path, src_site, src_host)
            src_paths = [src_path_]

        # early-out exists check on src path where possible
        if src_host and util._is_localhost(src_host):
            for src_p in src_paths:
                if not os.path.isfile(src_p):
                    open(src_p) # cause IOError

        # 'localhost' here doesn't mean 'localhost' on freight server
        if src_host and util._is_localhost(src_host):
            src_host = socket.gethostname()
        if dest_host and util._is_localhost(dest_host):
            dest_host = socket.gethostname()

        doc = self._post("copies",
            src_path=src_path_,
            dest_path=self._make_abs(dest_path, dest_site, dest_host),
            src_site=src_site,
            dest_site=dest_site,
            src_host=src_host,
            dest_host=dest_host,
            transfer_backend=transfer_backend,
            test_only=test_only,
            backend_settings=dtb_settings)

        handle = doc["copy_handle"]
        if block or callback:
            kwargs = dict( \
                copy_handle=handle,
                verbose=verbose,
                callback=callback)
            if block:
                return self._wait_on_copy(**kwargs)
            else:
                th = threading.Thread(target=self._wait_on_copy, kwargs=kwargs)
                th.setDaemon(True)
                th.start()

        return handle

    def get_file_copies(self, controller_site=None, status=None, order=None, all=False):
        """
        Get a list of file copies.
        @param controller_site One of:
            - None: Only show copies controlled by the current site.
            - str: Only show copies controlled by the specified site.
            - "ALL": show copies regardless of controller site.
        @param status Only get copies with the given status (you can also supply a list)
        @param order Sort the returned copies by the given order (see const.CopyOrder)
        @param all If True, return all info about the file copy, not just status info.
        @returns A list of copy entries.
        """
        a = util.Param(all) if all else None

        doc = self._get("copies",
            controller_site=controller_site,
            status=status,
            order=order,
            all=a)
        return doc["copies"]

    def get_file_copy_status(self, copy_handle, all=False):
        """
        Get status information about the given file copy.
        @param copy_handle Handle of the file copy.
        @param all If True, return all info about the file copy, not just status info.
        @returns None if status couldn't be determined, otherwise:<br>
        {<br>
            "status": CopyStatus.XXX,<br>
            [ "result": CopyResult.XXX, ]<br>
            [ "percent_complete": [-1-100], ]<br>
            [ "message": str, ]<br>
            [ "exit_code": int, ]<br>
            [ "data": ? ]<br>
        }
        - status: The current status of the copy (see const.CopyStatus).
        - result: The result of the copy (see const.ResultStatus).
        - percent_complete: How far the copy has gone (0..100, or -1 if unknown).
        - message: Error or success message.
        - exit_code: The exit code of the file transfer process, if applicable.
        - data: Backend-specific data that the backend implementation has provided.
        """
        a = util.Param(all) if all else None
        return self._get("copies/%s" % copy_handle, all=a)

    def _make_abs(self, path, site=None, host=None):
        path = os.path.expanduser(path)
        if os.path.isabs(path):
            return path
        elif (site and (site != self.site)) or (not util._is_localhost(host)):
            raise ValueError("Path must be absolute: %s" % path)
        return os.path.abspath(path)

    def _wait_on_copy(self, copy_handle, verbose, callback):
        time.sleep(0.1)
        last_status_doc = {}
        since = None

        while True:
            if verbose:
                logs = self.get_log_entries(copy_handle, since=since)
                if logs:
                    for log in logs:
                        host_str = "%s@%s" % (log["user"], log["host"])
                        dtb = log.get("transfer_backend")
                        site_str = ("%s@%s" % (dtb, log["site"])) if dtb else log["site"]
                        print "[%s,%s] %s" % (host_str, site_str, log["msg"])
                    since = logs[-1]["created"]

            if last_status_doc.get("status") == const.CopyStatus.ENDED:
                if callback: callback(copy_handle, last_status_doc)
                return (copy_handle, last_status_doc)

            status_doc = self.get_file_copy_status(copy_handle)
            if status_doc:
                if status_doc != last_status_doc:
                    if verbose:
                        print util.get_copy_status_string(status_doc)
                    
                    last_status_doc = status_doc
                    if status_doc["status"] == const.CopyStatus.ENDED:
                        continue

            time.sleep(1)

    def _full_url(self, url):
        return ("http://%s:%d/freight/%s" % (self.host, self.port, url)).rstrip('/')

    def _get(self, url, raw=False, **kwargs):
        return self._comm(requests.get, url, raw, **kwargs)

    def _post(self, url, raw=False, **kwargs):
        return self._comm(requests.post, url, raw, **kwargs)

    def _put(self, url, raw=False, **kwargs):
        return self._comm(requests.put, url, raw, **kwargs)

    def _comm(self, fn, url, raw=False, **kwargs):
        args = util.strip_nulls(kwargs)
        args2 = util.split_request_args(args)
        if "data" in args2:
            args2["headers"] = {'Content-type': 'application/json'}

        try:
            r = fn(self._full_url(url), **args2)
        except requests.exceptions.RequestException as e:
            err._raise(e)

        if util.echo_curl:
            print "### " + util.to_curl_cmd(r.request)

        doc = util.convert_response(r.json())
        e = err.FreightException.decode_json(doc)
        if e: raise e
        return r if raw else doc
