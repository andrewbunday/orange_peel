import os
import os.path
from datetime import datetime
from contrib.helpy import sequence
import socket
import urllib2
import hashlib
import const
import yaml
import json



class Param(object):
    def __init__(self, val):
        self.val = val
        self.param_val = str(val)

        if isinstance(val, bool):
            self.param_val = str(val).lower()


datetime_format = "%Y-%m-%dT%H:%M:%S.%f"

datetime_keys = frozenset([ \
    "last_modified",
    "created",
    "updated"
])


def pretty_print(doc):
    s = yaml.safe_dump(doc, default_flow_style=False)
    print s


def convert_response(doc, key=None):
    if key and key in datetime_keys:
        return datetime.strptime(doc, datetime_format)
    if isinstance(doc, dict):
        return dict((k,convert_response(v,k)) for k,v in doc.iteritems())
    elif isinstance(doc, list):
        return [convert_response(x) for x in doc]
    else:
        return doc


def split_request_args(args):
    params = {}
    data = {}

    def _convert(val):
        if isinstance(val, datetime):
            return val.strftime(datetime_format)
        else:
            return val

    for k,v in args.iteritems():
        if isinstance(v, Param):
            params[k] = _convert(v.param_val)
        else:
            data[k] = _convert(v)

    d = {}
    if params:
        d["params"] = params
    if data:
        d["data"] = json.dumps(data)
    return d


def to_curl_cmd(request):
    cmd = "curl -X%s %s" % (request.method, request.url)
    if request.body:
        cmd += " -H 'Content-Type: application/json'"
        cmd += " -d '%s'" % urllib2.unquote(request.body)
    return cmd


def strip_nulls(d):
    return dict((k,v) for k,v in d.iteritems() if v is not None)


def as_bool(v):
    return (v.lower() in ("1", "true", "yes")) \
        if isinstance(v, basestring) else bool(v)


def _is_localhost(host):
    return (host in (None, "localhost", "127.0.0.1", socket.gethostname()))


def file_stat(path):
    ts = os.path.getmtime(path)
    lmd = datetime.utcfromtimestamp(ts)
    filesize = os.path.getsize(path)
    return dict(
        last_modified=lmd,
        filesize=filesize
    )


def get_hash(path):
    h = hashlib.sha1()
    with open(path, 'r') as f:
        h.update(f.read())
    return h.hexdigest()


# datetime.timedelta.total_seconds() only available in python-2.7+
def total_seconds(time_delta):
    return time_delta.seconds + \
        (time_delta.days * 24 * 3600) + \
        (time_delta.microseconds / 1000000.0)


def get_copy_status_string(doc):
    status = doc.get("status", const.CopyStatus.PENDING)
    s = const.CopyStatus.label[status]

    if status == const.CopyStatus.ACTIVE:
        pc = doc.get("percent_complete", -1)
        s += "(?%%)" if (pc < 0) else "(%g%%)" % pc
    elif status == const.CopyStatus.ENDED:
        result = doc.get("result")
        if result is not None:
            rs = const.CopyResult.label[result]
            s += "(%s)" % rs
    
    return s


def pstring(path):
    if hasattr(path, "__iter__"):
        if len(path) == 1:
            return path[0]
        else:
            npaths = len(path)
            seqs = sequence.create_sequences(path, frame_regex='')
            nseqs = len(seqs)

            if nseqs < npaths:
                seq = seqs[0] if (nseqs == 1) else max(seqs, key=lambda x:len(x))
                i = seq.get_num_frames()
                j = seq.get_end_frame() - seq.get_start_frame() + 1
                s = "%s(%d/%d)" % (str(seq), i, j)
                if nseqs > 1:
                    s += "(+%d more)" % (npaths - i)
                return s
            else:
                return "%s(+%d more)" % (path[0], len(path)-1)
    else:
        return path


def location_string(site, path, host=None):
    s = site
    if host: s += ",%s" % host
    return "%s:%s" % (s, pstring(path))


def get_copy_summary_string(doc, show_all=False):
    controller="%s@%s" % (doc["transfer_backend"], doc["controller_site"])
    src = location_string(doc["src_site"], doc["src_path"], doc.get("src_host"))
    dest = location_string(doc["dest_site"], doc["dest_path"], doc.get("dest_host"))    
    status = get_copy_status_string(doc)
    s = "[%s]: %s -> %s [%s]" % (controller, src, dest, status)

    if show_all:
        exit_code = doc.get("exit_code")
        msg = doc.get("message")
        toks = []
        if exit_code is not None:
            toks.append("Exited with: %s" % str(exit_code))
        if msg:
            toks.append("Message: %s" % msg)
        if toks:
            s += " (%s)" % str(", ").join(toks)

    return s


def to_enum(val, enum_class, list_allowed=False):
    if val is None:
        return val

    if list_allowed:
        if isinstance(val, basestring) and ',' in val:
            toks = val.strip(',').split(',')
            return [to_enum(x, enum_class) for x in toks]
        elif hasattr(val, "__iter__"):
            return [to_enum(x, enum_class) for x in val]

    if isinstance(val, basestring):
        try:
            val = int(val)
        except:
            pass

    if isinstance(val, basestring):
        d = dict((k,v) for k,v in enum_class.label.iteritems() if v==val.upper())
        if d: return d.keys()[0]
    elif val in enum_class.label:
        return val

    labels = str(", ").join([x.lower() for x in enum_class.label.values()])
    raise ValueError("Expected one of %s values - %s" % (enum_class.__name__, labels))


echo_curl = as_bool(os.getenv("FREIGHT_ECHO_CURL"))
