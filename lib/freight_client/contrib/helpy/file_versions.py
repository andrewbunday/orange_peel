"""
@namespace helpy.file_versions Functions for performing version-related operations on filepaths. By
default, version numbers are expected to match the regex 'v[0-9]+'.
"""
import re
import os
import os.path
import string_
import iterable_utils
import osys
import glob



def get_version(path, version_regex=None):
    """
    Given a path, return the version associated with it or None if no version information could be 
    found. The version is the last matching occurrance of the version regex in the given path.
    @param version_regex Optional regex string to match to versions other than 'v001' form.
    @returns (version,padding,version-string), or None if no version info was found.
    """
    ver_re = version_regex or _g_ver_regex
    vers = string_.alphanumeric_split(path, number_regex=ver_re, strip_alpha=True)
    if vers:
        _re = re.compile(ver_re)
        vertoks = _re.findall(path)
        ver = vers[-1]
        return (ver[0], ver[1], vertoks[-1])


def replace_version(path, new_version, new_padding=None, version_regex=None):
    """
    Given a path, return the same path but changed to the given version. This also works when a
    version number is also present in the file name - for example, "/foo/v01/bah_v01.exr", if
    replaced with version 3, would become "/foo/v03/bah_v03.exr".
    @param new_version The replacement version to apply.
    @param new_padding New padding to apply, defaults to current.
    @param version_regex Optional regex string to match to versions other than 'v001' form.
    @returns The path with version replaced, or the path unchanged if no version was found.
    """
    ver_re = version_regex or _g_ver_regex
    toks = string_.alphanumeric_split(path, number_regex=ver_re)
    indexes = [i for i,tok in enumerate(toks) if isinstance(tok, tuple)]
    if indexes:
        old_tok = toks[indexes[-1]]
        new_tok = (new_version, (old_tok[1] if new_padding is None else new_padding))
        toks = iterable_utils.replace(toks, old_tok, new_tok)
        return string_.alphanumeric_merge(toks)
    else:
        return path


def get_versions(path, matchAll=False, version_regex=None):
    """
    Given a path, return the ordered list of versions (with padding) of other paths in the file
    system that are associated with it. Consider:
    - /foo/bah/v001/render
    - /foo/bah/v002/render
    - /foo/bah/v004/exr

    get_versions() given any of these paths will return [(1,3),(2,3),(4,3)]. get_versions("/foo/bah") 
    will also return the same result, because it directly contains versioned subdirectories.

    @param matchAll In the example above, get_versions("/foo/bah/v002/render") will include version 
        4, even though "/foo/bah/v004/render" does not exist. If matchAll is set to True, only
        whole matches will be included.
    @param version_regex Optional regex string to match to versions other than 'v001' form.
    @returns An ordered list of (version,padding,version-string) tuples, or an empty list if no 
        versions were found.
    """
    if not os.path.exists(path):
        open(path) # raise IOError
    path = path.rstrip(os.path.sep)

    ver_re = version_regex or _g_ver_regex
    path2 = path if os.path.isdir(path) else os.path.dirname(path)

    def _get_ver_paths(path_):
        toks = string_.alphanumeric_split(path_, number_regex=ver_re)
        indexes = [i for i,tok in enumerate(toks) if isinstance(tok, tuple)]
        if indexes:
            toks[indexes[-1]] = '*'
            wildpath = string_.alphanumeric_merge(toks)
            while '*' in os.path.dirname(wildpath):
                wildpath = os.path.dirname(wildpath)
            return glob.glob(wildpath)
        else:
            return []

    paths = _get_ver_paths(path2)
    if not paths:
        # check for path containing version subdirs
        for dirpath in osys.list_dirs(path2):
            paths = _get_ver_paths(dirpath)
            if paths:
                break

    vers = []
    for path_ in paths:
        ver = get_version(path_, version_regex=version_regex)
        if ver is not None:
            vers.append(ver)

    if matchAll:
        vers2 = []
        for ver in vers:
            path_ = replace_version(path, ver[0], ver[1], version_regex=version_regex)
            if os.path.exists(path_):
                vers2.append(ver)
        vers = vers2

    return sorted(vers)


def get_next_free_version(path, version_regex=None):
    """
    Return a new version (with padding) one larger than the latest version associated with this 
    path, as well as the new path. Consider:
    - /foo/bah/v001/render
    - /foo/bah/v002/render
    - /foo/bah/v004/exr/foo.v004.exr

    get_next_free_version(X) will return:
    "/foo/bah" => (5, 3, "/foo/bah");
    "/foo/bah/v001" => (5, 3, "/foo/bah/v005");
    "/foo/bah/v001/render" => (5, 3, "/foo/bah/v005/render");
    "/foo/bah/v004/exr/foo.v004.exr" => (5, 3, "/foo/bah/v005/exr/foo.v005.exr")

    @returns (version, padding, new-path), or None if no version is found.
    """
    vers = get_versions(path)
    if vers:
        new_ver = vers[-1][0] + 1
        new_path = replace_version(path, new_ver, version_regex=version_regex)
        return (new_ver, vers[-1][1], new_path)


def get_next_used_version(path, version_regex=None):
    """
    Finds the next existing path that matches the given path, but has a newer version number.
    @returns (version, padding, next-path), or None if no path is found.
    """
    this_ver = get_version(path, version_regex=version_regex)
    if this_ver:
        vers = get_versions(path, version_regex=version_regex)
        for ver in (vers or []):
            if ver[0] > this_ver[0]:
                next_path = replace_version(path, ver[0], ver[1], version_regex=version_regex)
                if os.path.exists(next_path):
                    return (ver[0], ver[1], next_path)


def get_previous_used_version(path, version_regex=None):
    """
    Finds the previous existing path that matches the given path, but has an earlier version number.
    @returns (version, padding, next-path), or None if no path is found.
    """
    this_ver = get_version(path, version_regex=version_regex)
    if this_ver:
        vers = get_versions(path, version_regex=version_regex)
        for ver in reversed(vers or []):
            if ver[0] < this_ver[0]:
                next_path = replace_version(path, ver[0], ver[1], version_regex=version_regex)
                if os.path.exists(next_path):
                    return (ver[0], ver[1], next_path)


_g_ver_regex = "v[0-9]+"
