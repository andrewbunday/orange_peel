"""
@namespace helpy.recall A simple API for storing (k,v) pairs, for the purposes of persistent 
remembering of settings etc between app invocations. Settings are saved to localhost, and it is 
assumed that they apply to the current user only. Simple python types can be remembered - ie int,
string, list and dicts etc. Use functions set_value(), get_value() and update().

By default, configuration files are stored at:
$HOME/.config/method/<APP_NAME>/<NAMESPACE or main>.config
"""

import atexit
import ast
import string
import pprint
import os
import os.path
import copy



_g_debug = bool(os.getenv("HELPY_RECALL_DEBUG"))


def set_value(app_name, key, value, namespace=None):
    """
    Set a value to remember in future.
    @param key Key to store.
    @param value Value to store for the given key.
    """
    d = Recall._get_kv_pairs(app_name, namespace)
    if d.get(key) != value:
        d[key] = copy.deepcopy(value)
        Recall.changed_namespaces.add((app_name, namespace))
        if _g_debug:
            scope = "%s.%s" % (app_name, namespace or "main")
            print "recall - SET %s.%s = %s" % (scope, key, str(value))


def get_value(app_name, key, namespace=None):
    """
    @returns The value associated with the key, or None.
    """
    v = Recall._get_kv_pairs(app_name, namespace).get(key)
    if v is not None:
        if _g_debug:
            scope = "%s.%s" % (app_name, namespace or "main")
            print "recall - GET %s.%s = %s" % (scope, key, str(v))
        return copy.deepcopy(v)


def update(app_name, dict_, add_new_keys=True, namespace=None):
    """
    Update the given dict with recalled values from the given namespace.
    @param add_new_keys If False, no new keys will be added to dict_.
    """
    d = Recall._get_kv_pairs(app_name, namespace)
    for k,v in d.iteritems():
        if add_new_keys or (k in dict_):
            dict_[k] = copy.deepcopy(v)



def _recall_atexit():
    if Recall.settings_dir:
        for namespace in Recall.changed_namespaces:
            d = Recall.namespaces.get(namespace)
            s = pprint.pformat(d)
            filepath = Recall._settings_filepath(*namespace)
            try:
                path = os.path.dirname(filepath)
                if not os.path.exists(path):
                    os.makedirs(path)
                with open(filepath,'w') as f:
                    f.write(s)
                if _g_debug:
                    print "recall - saved settings to " + filepath
            except:
                pass


class Recall():
    namespaces = {}
    changed_namespaces = set()
    settings_dir = os.getenv("METHOD_SETTINGS_DIR", ("%s/.config/method" % os.getenv("HOME")))

    if not os.path.isdir(settings_dir):
        try:
            os.makedirs(settings_dir)
        except:
            settings_dir = None

    if settings_dir:
        atexit.register(_recall_atexit)

    @staticmethod
    def _settings_filepath(app_name, namespace):
        namespace = namespace or "main"
        if (set(app_name + namespace) - set(string.ascii_letters + string.digits + "_")):
            raise ValueError("Namespaces/app names can only contain letters, numbers or underscores")

        filename = "%s.config" % namespace
        return os.path.join(Recall.settings_dir, app_name, filename)

    @staticmethod
    def _get_kv_pairs(app_name, namespace):
        d = Recall.namespaces.get((app_name, namespace))
        if d is None:
            d = {}
            if Recall.settings_dir:
                filepath = Recall._settings_filepath(app_name, namespace)
                if os.path.isfile(filepath):
                    try:
                        with open(filepath) as f:
                            s = f.read()
                        d = ast.literal_eval(s)
                        if _g_debug:
                            print "recall - loaded settings from " + filepath
                    except:
                        pass
            Recall.namespaces[(app_name, namespace)] = d
        return d
