"""
@namespace helpy.sequence Functions for manipulating and interpreting lists of related filepaths,
for example an image sequence.
"""
import string_
import osys
import glob
import iterable_utils
import math
import re
import os
import os.path



_g_frame_regex = os.getenv("HELPY_SEQUENCE_FRAME_REGEX")


class FilepathSequence(object):
    """
    An object representing a sequence of related paths, for example an image sequence. The last
    number in each path is assumed to be the frame number. Do not construct a FilepathSequence
    object directly - instead use the free functions, such as create_sequences(). A filepath 
    sequence can represent a single path with no frame number - in this case, get_num_frames() 
    will return zero, and is_sequence() will return False.

    An example sequence:
    - /foo/bah.002.exr
    - /foo/bah.003.exr
    - /foo/bah.005.exr
    """
    FORMAT_STANDARD     = 0     # eg foo.[001-099].exr
    FORMAT_EXTENDED     = 1     # eg foo.[001-050,052-99].exr
    FORMAT_GLOB         = 2     # eg foo.*.exr
    FORMAT_HOUDINI      = 3     # eg foo.$F4.exr
    FORMAT_NUKE         = 4     # eg foo.%04d.exr
    FORMAT_KATANA       = 5     # eg foo.####.exr
    FORMAT_RV           = 6     # eg foo.1-99@@@@.exr

    FormatRegexes = {
        FORMAT_STANDARD:   re.compile("(.*?)\\[([0-9]+(?:-[0-9]+)?)\\](.*?)$"),
        FORMAT_EXTENDED:   re.compile("(.*?)\\[([0-9-,]+)\\](.*?)$"),
        FORMAT_RV:         re.compile("(.*?)([0-9]+-[0-9]+@+)(.*?)$"),
        FORMAT_HOUDINI:    re.compile("(.*?)(\\$F[0-9]+)(.*?)$"),
        FORMAT_NUKE:       re.compile("(.*?)(%[0-9]+d)(.*?)$"),
        FORMAT_GLOB:       re.compile("([^\\*]*)(\\*)([^\\*]*)$"),
        FORMAT_KATANA:     re.compile("([^#]*)(#+)([^#]*)$")
    }

    FormatDecodeOrder = [
        FORMAT_STANDARD,
        FORMAT_EXTENDED,
        FORMAT_RV,
        FORMAT_HOUDINI,
        FORMAT_NUKE,
        FORMAT_GLOB,
        FORMAT_KATANA
    ]

    Formats = {
        "standard":     FORMAT_STANDARD,
        "extended":     FORMAT_EXTENDED,
        "glob":         FORMAT_GLOB,
        "houdini":      FORMAT_HOUDINI,
        "nuke":         FORMAT_NUKE,
        "katana":       FORMAT_KATANA,
        "rv":           FORMAT_RV
    }

    FormatsInv = dict((v,k) for k,v in Formats.iteritems())


    def __init__(self, frames=None, padding=0, pathsplit=None, single_path=None):
        """ Do not call this constructor directly """
        self.frames = set(frames or [])
        self.single_path = single_path
        self.pathsplit = pathsplit
        self.padding = padding

        self.start_frame, self.end_frame, self.num_missing_frames, \
            self.frame_ranges, self.missing_frame_ranges = get_sequence_info(self.frames)

    def copy(self):
        return FilepathSequence(self.frames, self.padding, self.pathsplit, self.single_path)

    def __str__(self):
        return self.get_string()

    def __len__(self):
        return len(self.frames) or 1

    def __getitem__(self, frame):
        return self.get_path(frame)

    def __iter__(self):
        return self.iterframes()

    def iterframes(self):
        return iter(self.get_frames())

    def iterpaths(self):
        return iter(self.get_paths())

    def iteritems(self):
        if self.is_sequence():
            items = [(f,self.get_path(f)) for f in self.get_frames()]
        else:
            items = [(None, self.single_path)]
        return iter(items)

    def get_num_frames(self):               return len(self.frames)
    def get_padding(self):                  return self.padding
    def get_start_frame(self):              return self.start_frame
    def get_end_frame(self):                return self.end_frame
    def get_num_runs(self):                 return max(len(self.frame_ranges), 1)
    def get_num_missing_frames(self):       return self.num_missing_frames
    def get_frame_ranges(self):             return self.frame_ranges[:]
    def get_missing_frame_ranges(self):     return self.missing_frame_ranges[:]
    def is_sequence(self):                  return (not bool(self.single_path))

    def set_padding(self, padding):
        """
        Change the padding of this sequence.
        """
        if padding <= 0:
            raise ValueError("Padding must be 1 or higher")
        self.padding = padding

    def get_num_paths(self):
        return 1 if self.single_path else len(self.frames)

    def get_common_prefix(self):
        return self.pathsplit[0] if self.is_sequence() else self.single_path

    def get_common_suffix(self):
        return self.pathsplit[1] if self.pathsplit else ''

    def get_string(self, format=None, single_as_path=False):
        """
        Return a string representing the sequence, eg "/foo/bah.[001-099].exr".
        @param format One of various formats such as Rv, Nuke etc. See FORMAT_XXX enums.
        @param single_as_path If True, and the sequence contains a single frame, then the exact
            path will be returned (rather than for eg 'foo.[1].exr').
        """
        if (not self.is_sequence()) or \
            ((self.get_num_frames() == 1) and \
            (single_as_path or (format==FilepathSequence.FORMAT_RV))):
            return self.get_path()

        if not format:
            a = str(self.start_frame).zfill(self.padding)
            if self.get_num_frames() == 1:
                tok = "[%s]" % a
            else:
                b = str(self.end_frame).zfill(self.padding)
                tok = "[%s-%s]" % (a,b)
        elif format == FilepathSequence.FORMAT_EXTENDED:
            toks = []
            fn = lambda x: str(x).zfill(self.padding)
            for r in self.frame_ranges:
                tok = ("%s-%s" % (fn(r[0]), fn(r[1]-1))) if (r[1]>r[0]+1) else fn(r[0])
                toks.append(tok)
            tok = "[%s]" % str(',').join(toks)
        elif format == FilepathSequence.FORMAT_GLOB:
            tok = '*'
        elif format == FilepathSequence.FORMAT_HOUDINI:
            tok = "$F%d" % self.padding
        elif format == FilepathSequence.FORMAT_NUKE:
            tok = "%0" + str(self.padding) + "d"
        elif format == FilepathSequence.FORMAT_KATANA:
            tok = '#' * self.padding
        elif format == FilepathSequence.FORMAT_RV:
            tok = "%d-%d%s" % (self.start_frame, self.end_frame, '@'*self.padding)
        else:
            raise ValueError("Unknown format '%s'" % format)

        return "%s%s%s" % (self.pathsplit[0], tok, self.pathsplit[1])

    def get_path(self, frame=None, force=False):
        """
        @returns The filepath for the given frame.
        @param frame Frame to retrieve path for. If None, the first frame's path is returned.
        @oaram force Return a path, even if this path does not exist in the sequence.
        @returns A path, or None if the frame was not in the sequence.
        """
        if not self.is_sequence():
            return self.single_path

        if frame is None:
            frame = self.start_frame
        elif not force and frame not in self.frames:
            return None

        tok = str(frame).zfill(self.padding)
        return "%s%s%s" % (self.pathsplit[0], tok, self.pathsplit[1])

    def get_paths(self, existing=False):
        """
        @param existing If True, only paths that exist on disk are returned.
        @returns A frame-ordered list of all paths in the sequence.
        """
        paths = [self.get_path(x) for x in sorted(self.frames)] \
            if self.is_sequence() else [self.single_path]
        if existing:
            paths = [x for x in paths if os.path.exists(x)]
        return paths

    def has_path(self, path, force=False):
        """
        @return True if the path is in the sequence.
        @param force If True, this function will return True even if the path is not in the 
            sequence, as long as it is in a format that matches the sequence.
        """
        if self.is_sequence():
            frame = self._extract_frame(path)
            return (frame is not None) and (force or (frame in self.frames))
        else:
            return (path == self.single_path)

    def get_frame(self, path, force=None):
        """
        Get the frame associated with the given path.
        @param force If True, this fuction will return a frame number even if the path is not in 
            the sequence, as long as it is in a format that matches the sequence.
        @return The frame corresponding to the given path, or None if there is no associated frame,
            or the path is not in the sequence.
        """
        if self.is_sequence():
            frame = self._extract_frame(path)
            return frame if (force or (frame in self.frames)) else None
        return None

    def get_frames(self):
        """
        @returns An ordered list of all frames in the sequence.
        """
        return sorted(self.frames) if self.is_sequence() else []

    def get_missing_frames(self):
        """
        @returns An ordered list of all frames missing from the sequence.
        """
        return math.add([range(*x) for x in self.missing_frame_ranges])

    def get_frame_ranges_as_string(self, sep=','):
        """
        @returns A user-friendly string representation of present frames. Eg "1-10,15"
        @param sep Seperator string.
        """
        return FilepathSequence.get_frame_ranges_str(sep, self.frame_ranges)

    def get_missing_frame_ranges_as_string(self, sep=','):
        """
        @returns A user-friendly string representation of missing frames. Eg "1-10,15"
        @param sep Seperator string.
        """
        return FilepathSequence.get_frame_ranges_str(sep, self.missing_frame_ranges)

    def get_run_paths(self, run_index=0):
        """
        @returns The filepaths, in frame order, for the given run.
        """
        if self.is_sequence():
            r = self.frame_ranges[run_index]
            return [self.get_path(i) for i in range(*r)]

        if run_index:
            raise IndexError("Run index %d out of range" % run_index)
        return self.get_paths()

    def get_run_frames(self, run_index=0):
        """
        @returns An ordered list of frames for the given run.
        """
        return range(*(self.frame_ranges[run_index]))

    def get_run_as_sequence(self, run_index=0):
        """
        @returns A run in this sequence as a new FilepathSequence object.
        """
        return self.reframed(self.get_run_frames(run_index))

    def get_ext(self):
        """
        @returns The common filepath extension, if any ( eg 'exr').
        """
        ext = os.path.splitext(self.get_path())[1]
        return ext[1:] if ext else ''

    def inverse(self):
        """
        @returns A copy of the sequence containing only frames that are missing from this
            sequence, or None if there are no missing frames.
        """
        invframes = self.get_missing_frames()
        if invframes:
            return FilepathSequence(invframes, self.padding, self.pathsplit)
        else:
            return None

    def intersection(self, frames):
        """
        @returns A copy of the sequence, restricted to the given frames, or None if no frames
            overlap with any current frames.
        """
        if not self.is_sequence() or not frames:
            return None
        intframes = set(frames) & self.frames
        if not intframes:
            return None
        return FilepathSequence(intframes, self.padding, self.pathsplit)

    def subsequence(self, min_frame=None, max_frame=None):
        """
        Returns a subsection of the sequence. The new sequence only contains frames that exist
        in the current sequence.
        @param min_frame The minumum frame in the subsequence.
        @param end_frame The maximum frame in the subsequence.
        @returns The new sequence, or None if there were no frames in the subsequence.
        """
        if min_frame is None:
            min_frame = self.start_frame
        if max_frame is None:
            max_frame = self.end_frame
        assert(max_frame >= min_frame)
        return self.intersection(range(min_frame, max_frame+1))

    def refreshed(self, grow=True):
        """
        Return a copy of the sequence, updated with respect to the file system.
        @param grow If True, new files on disk that are part of the sequence will be added. If
            False, then only files deleted from disk will be removed from the sequence.
        @returns The new sequence, or None if all files in the sequence have been deleted.
        """
        if not self.is_sequence():
            return self.copy() if os.path.exists(self.single_path) else None

        globpath = self.get_string(FilepathSequence.FORMAT_GLOB)
        paths = glob.glob(globpath)
        if not paths:
            return None

        frames = set()
        for path in paths:
            frame = self._extract_frame(path)
            if frame is not None:
                frames.add(frame)
        if not grow:
            frames &= self.frames
        if not frames:
            return None

        return FilepathSequence(frames, self.padding, self.pathsplit)

    def reframed(self, frames):
        """
        Return a copy of the sequence with different frames present.
        @param frames List of new frames.
        @returns The reframed sequence.
        """
        if not frames or not self.is_sequence():
            return None
        return FilepathSequence(frames, self.padding, self.pathsplit)

    def offset(self, n):
        """
        Return a copy of the sequence, with frame numbers offset by n.
        @param n Integer (can be negative) to offset frame numbers by.
        @returns The offset sequence.
        """
        new_frames = [x+n for x in self.frames]
        if min(new_frames) < 0:
            raise ValueError("Offset resulted in negative frame numbers")
        return self.reframed(new_frames)

    def renamed(self, path_prefix=None, path_suffix=None):
        """
        Return a copy of this sequence with a new path string.
        @param path_prefix, path_suffix The parts of the new paths preceding and following the
            frame number. If None, the existing prefix/suffix is used.
        @returns The renamed sequence, or None if this is a non-sequence.
        """
        if not self.is_sequence():
            return None
        path_prefix = self.pathsplit[0] if path_prefix is None else path_prefix
        path_suffix = self.pathsplit[1] if path_suffix is None else path_suffix
        return FilepathSequence(self.frames, self.padding, (path_prefix,path_suffix))

    def _extract_frame(self, path):
        if not (path.startswith(self.pathsplit[0]) and path.endswith(self.pathsplit[1])):
            return None
        frame_str = path[len(self.pathsplit[0]):len(path)-len(self.pathsplit[1])]
        if frame_str.isdigit():
            frame = int(frame_str)
            if str(frame).zfill(self.padding) == frame_str:
                return frame
        return None

    @staticmethod
    def get_frame_ranges_str(sep, v):
        def _fn(r):
            return str(r[0]) if (r[1]-r[0] == 1) else ("%d-%d" % (r[0],r[1]-1))
        toks = [_fn(r) for r in v]
        return sep.join(toks)


def create_sequences(paths, skip_non_frames=False, include_dirname=False, frame_regex=None):
    """
    Given a list of filepaths, group them into sequences.
    @param skip_non_frames If True, files that are not part of a sequence (ie have no numeric part 
        in their filename) will be ignored.
    @param include_dirname If True, the directory part of each path is included. This means that
        sequences can occur based on directory names as well as file names.
    @param frame_regex Optionally specify your own regex for matching frame numbers. Defaults to
        $HELPY_SEQUENCE_FRAME_REGEX if set, or any number if not set, or frame_regex is ''.
    """
    seqs = []
    if frame_regex is None:
        frame_regex = _g_frame_regex

    # convert paths into lists of tokens
    tok_paths = []
    for path in paths:
        tok_path = string_.alphanumeric_split(path, basename_only=(not include_dirname), \
            number_regex=frame_regex)
        if len(tok_path)==1 and isinstance(tok_path[0], basestring):
            if skip_non_frames:
                continue
            else:
                # no number part, this is a non-sequence
                seq = create_non_sequence(path)
                seqs.append(seq)
        else:
            tok_paths.append(tok_path)

    # group by non-numeric part
    fn = lambda x: string_.alphanumeric_merge(x, replace_numbers=0)
    groups1 = iterable_utils.group_by(tok_paths, fn)

    # in each group, find the number part that, if interpreted as the frame number, would 
    # result in the least number of sequences being generated. This is the one we choose.
    for group1 in groups1.itervalues():
        number_indexes = [x[0] for x in enumerate(group1[0]) if isinstance(x[1],tuple)]
        idx = number_indexes[0]
        if len(number_indexes) > 1:
            seq_lens = []
            for i in number_indexes:
                fn = lambda x: tuple(x[:i]+x[i+1:])
                groups = iterable_utils.group_by(group1, fn)
                seq_lens.append((i, len(groups)))

            idx = min(reversed(seq_lens), key=lambda x:x[1])[0]
            fn = lambda x: tuple(x[:idx]+x[idx+1:])
            groups = iterable_utils.group_by(group1, fn).itervalues()
        else:
            groups = [group1]

        for group in groups:
            # these files all form the same sequence, if we don't take padding into account. Here
            # we check padding, and split into larger groups first, down to the smallest. This
            # ensures that files with unusual padding end up in their own groups.
            str_a = string_.alphanumeric_merge(group[0][:idx])
            str_b = string_.alphanumeric_merge(group[0][idx+1:])
            pathsplit = (str_a, str_b)

            numbers = [x[idx] for x in group]
            min_padding = min([x[1] for x in numbers])
            max_padding = max([x[1] for x in numbers])
            
            padgroups = []
            for pad in range(min_padding, max_padding+1):
                padnums = [x for x in numbers if str(x[0]).zfill(x[1]) == str(x[0]).zfill(pad)]
                padgroups.append((pad, set(padnums)))

            padgroups = sorted(padgroups, reverse=True, key=lambda x:len(x[1]))
            visited_nums = set()
            for padding,nums in padgroups:
                nums2 = nums - visited_nums
                if nums2:
                    frames = [x[0] for x in nums2]
                    seq = FilepathSequence(frames, padding, pathsplit)
                    seqs.append(seq)
                    visited_nums |= nums2

    fn = lambda x: string_.alphanumeric_split(x.get_string(FilepathSequence.FORMAT_GLOB).lower())
    return sorted(seqs, key=fn)


def create_non_sequence(path):
    """
    Create a sequence object that contains a single path and no frames.
    """
    return FilepathSequence(single_path=path)


def detect_sequence_format(seq_str):
    """
    Given a sequence string such as "foo.[001-099].exr", detect the format.
    @returns Format (such as rv, nuke etc) or None if no matching format was found.
    """
    for fmt in FilepathSequence.FormatDecodeOrder:
        regex = FilepathSequence.FormatRegexes[fmt]
        m = regex.match(seq_str)
        if m:
            return fmt
    return None


def create_sequence_from_string(seq_str, frames=None, padding=None, format=None):
    """
    Given a sequence string such as "foo.[001-099].exr", create the corresponding sequence object.
    @param frames List of frames in the sequence. Only used if the frames could not be determined
        from the sequence string (eg "foo.###.exr").
    @param padding Frame number padding. Only used if the padding could not be determined from the
        sequence string (eg "foo.*.exr").
    @param format The string format (rv, nuke etc). If left as None, format will be detected.
    @returns The equivalent sequence object, or None if not enough information was given.
    """
    r = decode_sequence_string(seq_str, format)
    if not r:
        return create_non_sequence(seq_str)
    fmt,path_prefix,path_suffix,frame_ranges,pad = r

    if frame_ranges is None and frames is None:
        return None
    if pad is None and padding is None:
        return None
    if frame_ranges:
        frames = math.add([range(x[0],x[1]) for x in frame_ranges])

    pathsplit = (path_prefix,path_suffix)
    return FilepathSequence(frames, pad or padding, pathsplit)


def create_sequence_containing_file(filepath):
    """
    @returns A sequence containing this file and other matching files on the filesystem, or None
        if this file does not exist.
    """
    if not os.path.exists(filepath):
        return None

    dirname,filename = os.path.split(filepath)
    m = re.compile("^\D+").match(filename)
    prefix = m.group() if m else ''
    globpath = os.path.join(dirname, "%s*" % prefix)

    paths = glob.glob(globpath)
    seqs = create_sequences(paths)
    seqs = [x for x in seqs if x.has_path(filepath)]
    return seqs[0]


def find_files_from_string(seq_str, format=None, grow=False, padding=None):
    """
    Given a sequence string such as "foo.[001-099].exr", find the files that belong to the sequence.
    @param format The string format (rv, nuke etc). If left as None, format will be detected.
    @param grow If True, then files that match the sequence, but have a frame number outside of
        the sequence, will also be included.
    @param padding Frame number padding. Only used if the padding could not be determined from the
        sequence string (eg "foo.*.exr").
    @returns The sequence containing the matching files, or None if no matching files found.
    """
    r = decode_sequence_string(seq_str, format)
    if not r:
        return create_non_sequence(seq_str) if os.path.exists(seq_str) else None
    fmt,path_prefix,path_suffix,frame_ranges,pad = r

    if fmt == FilepathSequence.FORMAT_GLOB:
        files = glob.glob(seq_str)
        seqs = create_sequences(files)
        if padding:
            seqs = [x for x in seqs if x.get_padding()==padding]
            if not seqs:
                return None
        seq = max(seqs, key=lambda x:x.get_num_paths())
        return seq

    assert(pad is not None)
    frames = math.add([range(x[0],x[1]) for x in frame_ranges]) \
        if frame_ranges else [0]
    pathsplit = (path_prefix,path_suffix)
    
    seq = FilepathSequence(frames, pad, pathsplit)
    return seq.refreshed(grow=(grow or (frame_ranges is None)))


def decode_sequence_string(seq_str, format=None):
    """
    Given a sequence string such as "foo.[001-099].exr", return decoded information.
    @param format The string format (rv, nuke etc).
    @returns (format, path_prefix, path_suffix, frame_ranges, padding), where:
        - format is one of FORMAT_NUKE, FORMAT_RV etc;
        - path_prefix is the part of the path before the frame number, or the whole string if
          there is no frame number;
        - path_suffix is the part of the path after the frame number, or '' if there is no
          frame number;
        - frame_ranges is a list of (start_frame,end_frame+1) tuples describing frame numbers
          present in the sequence, or None if frame ranges could not be determined (some formats
          don't describe this info, such as Katana).
        - padding is the frame number padding. None if padding cannot be determined.
        Or, returns None if the string is not recognised as a sequence.
    """
    fmt = None
    path_prefix = None
    path_suffix = None
    frame_ranges = None
    padding = None
    s = None

    formats = FilepathSequence.FormatDecodeOrder if format is None else [format]
    for fmt_ in formats:
        regex = FilepathSequence.FormatRegexes[fmt_]
        m = regex.match(seq_str)
        if m:
            fmt = fmt_
            path_prefix,s,path_suffix = m.groups()
            break
    if fmt is None:
        return None

    def _get_range(rstr):
        nums = [(int(x),len(x)) for x in rstr.split('-')]
        if len(nums) == 1: nums.append(nums[0])
        return ((nums[0][0], nums[1][0]+1), min(nums[0][1], nums[1][1]))

    if fmt == FilepathSequence.FORMAT_STANDARD:
        r,padding = _get_range(s)
        frame_ranges = [r]
    elif fmt == FilepathSequence.FORMAT_EXTENDED:
        frame_ranges = []
        rstrs = s.split(',')
        padding = None
        for rstr in rstrs:
            r,pad = _get_range(rstr)
            padding = min(padding or pad,pad)
            frame_ranges.append(r)
    elif fmt == FilepathSequence.FORMAT_RV:
        padding = s.count('@')
        rstr = s.replace('@','')
        r = _get_range(rstr)[0]
        frame_ranges = [r]
    elif fmt == FilepathSequence.FORMAT_KATANA:
        padding = s.count('#')
    elif fmt == FilepathSequence.FORMAT_HOUDINI:
        padding = int(s.replace('F','').replace('$',''))
    elif fmt == FilepathSequence.FORMAT_NUKE:
        padding = int(s.replace('%','').replace('d',''))

    return (fmt, path_prefix, path_suffix, frame_ranges, padding)


def get_sequences_summary_string(sequences):
    """
    @param sequences A list of FilepathSequence objects.
    @returns A string summarising the total number of sequences, runs and missing frames in the 
        given sequences.
    """
    toks = []
    nfiles = sum([x.get_num_paths() for x in sequences])
    if nfiles == 1:
        return "1 file"
    toks.append("%d files" % nfiles)

    nseqs = len(sequences)
    if nseqs > 1:
        s = "%d sequence%s" % (nseqs, ('s' if nseqs>1 else ''))
        toks.append(s)

    nruns = sum([x.get_num_runs() for x in sequences])
    if nruns > 1:
        toks.append("%d runs" % nruns)

    nmissing = sum([x.get_num_missing_frames() for x in sequences])
    if nmissing:
        toks.append("%d missing frame%s" % (nmissing, ('s' if nmissing>1 else '')))

    return str(", ").join(toks)


def get_sequence_info(nums):
    """
    Given a list of numbers, return a list containing:
    - the minimum number
    - the maximum number
    - the total number of missing numbers
    - a list of (start,end) tuples, where each represents a range of present numbers.
    - a list of (start,end) tuples, where each represents a range of missing numbers.

    For example, get_sequence_info([1,2,3,5,6,7,100,101,102]) would give:
    - 1
    - 102
    - 93
    - [(1,4), (5,8), (100,103)]
    - [(4,5), (8,100)]
    """
    if not nums:
        return (0,0,0,[],[])

    nums2 = sorted(list(set(nums)))
    nfirst = nums2[0]
    nlast = nums2[-1]
    nmissing = 0
    ranges = []
    gaps = []

    nums2.append(nlast+2)
    while len(nums2) > 1:
        start = nums2[0]
        while (nums2[1]-nums2[0]) < 2:
            nums2 = nums2[1:]
        ranges.append((start, nums2[0]+1))
        nmissing += nums2[1] - (nums2[0]+1)
        gaps.append((nums2[0]+1, nums2[1]))
        nums2 = nums2[1:]

    return (nfirst, nlast, nmissing-1, ranges, gaps[:-1])


def decode_frames_string(frames_str):
    """
    Returns the frames present in an extended-style string. Examples:
    - 5         frames: 5
    - 1-3       frames: 1,2,3
    - 4,5,8     frames: 4,5,8
    - 1,5-7     frames: 1,5,6,7
    @returns The frames described by the string.
    """
    def _err():
        raise ValueError("'%s' does not describe a range of numbers" % frames_str)

    def _frames(s):
        if s.isdigit():
            return set([int(s)])
        toks = s.split('-',1)
        if len(toks) != 2:
            _err()
        try:
            a,b = (int(toks[0]), int(toks[1]))
            return set(range(a,b+1))
        except ValueError:
            _err()

    frames = set()
    rstrs = frames_str.replace(',',' ').strip().split()
    for rstr in rstrs:
        frames |= _frames(rstr)

    return frames


def _get_format(fmt):
    def _err():
        raise ValueError("Unknown format %s" % str(fmt))

    if isinstance(fmt, basestring):
        fmt = FilepathSequence.Formats.get(fmt)
        if fmt is None:
            _err()
    elif fmt not in FilepathSequence.FormatDecodeOrder:
        _err()
    return fmt
