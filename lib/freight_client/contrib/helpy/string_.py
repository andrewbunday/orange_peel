"""
@namespace helpy.string Functions for manipulating strings.
"""
import re
import os.path



def regex_split(s, regex, max_matches=0):
    """
    Takes a string and splits it into parts that match the given regex and parts that do not.
    @param max_matches The maximum number of substrings to match to the regex; ignored if zero.
    @returns (tokens, indexes) where 'tokens' is the list of substrings, and 'indexes' is a list
        of ints that indicate the regex-matching tokens in the returned token list.
    """
    toks = []
    retoks = re.findall(regex, s)
    indexes = []

    for j,tok in enumerate(retoks):
        i = s.find(tok)
        if i:
            toks.append(s[:i])
        indexes.append(len(toks))
        n = len(tok)
        toks.append(s[i:i+n])
        s = s[i+n:]
        if j == max_matches-1:
            break

    if s:
        toks.append(s)
    return (toks, indexes)


def alphanumeric_split(s, basename_only=False, number_regex=None, strip_alpha=False, \
    strip_numeric=False):
    """
    Takes a string and splits it into alpha and numeric parts. Numeric parts are represented as a
    tuple, where the second number is the padding. Eg, "foo_v032/imgs/robot1002.exr" becomes
    ["foo_v", (32,3), "/imgs/robot", (1002,4), ".exr")].
    @param basename_only If 's' is a path, then numeric parts in the directory part of the path 
        will be ignored - ie splitting will only occur in the basename part of the string.
    @param number_regex Optionally specify your own regex to determine what is a number part. For
        example, you might want to only split out numbers prefixed with an 'f', so your regex 
        would be "f[0-9]+". Non-numeric parts of the regex are able to overlap.
    @param strip_alpha If True, non-numeric tokens will be removed.
    @param strip_numeric If True, numeric tokens will be removed.
    """
    if not s or (strip_alpha and strip_numeric):
        return []

    if basename_only:
        dirpart,basepart = os.path.split(s)
        if basepart != s:
            basetoks = alphanumeric_split(basepart, False, number_regex, strip_alpha, strip_numeric)
            first_tok = dirpart + os.path.sep
            if basetoks and isinstance(basetoks[0], basestring):
                toks = basetoks
                toks[0] = first_tok + toks[0]
            else:
                toks = [first_tok] + basetoks
            return toks

    if number_regex:
        s_ = s
        toks = []
        while s_:
            m = re.search(number_regex, s_)
            if m:
                m2 = re.search("[0-9]+", m.group())
                if not m2:
                    raise ValueError("Your provided number regex matched non-numeric string '%s'" \
                        % m.group())
                i = m.start() + m2.start()
                if i:
                    toks.append(s_[:i])
                toks.append(m2.group())
                s_ = s_[i+len(m2.group()):]
            else:
                toks.append(s_)
                break
    else:
        toks = regex_split(s, "[0-9]+")[0]

    fn = lambda x:(int(x), len(x)) if x.isdigit() else x
    toks = [fn(x) for x in toks]

    if strip_alpha:
        toks = [x for x in toks if isinstance(x, tuple)]
    elif strip_numeric:
        toks = [x for x in toks if not isinstance(x, tuple)]
    return toks


def alphanumeric_merge(tokens, replace_numbers=None):
    """
    Reconstruct the original string from the result of alphanumeric_split()
    @param replace_numbers If not None, replace all instances of numbers with this value instead.
    """
    s = ''
    for tok in tokens:
        if type(tok) == tuple:
            if replace_numbers is None:
                s += str(tok[0]).zfill(tok[1])
            else:
                s += str(replace_numbers)
        else:
            s += tok
    return s


def alphanumeric_sorted(iterable, basename_only=False, number_regex=None):
    """
    @returns A new alphanumerically-sorted list from the items in iterable.
    """
    return sorted(iterable, key=lambda x: alphanumeric_split(x, basename_only, number_regex))


def expand_strings(string_dict, string_ref_regex, extract_key=None):
    """
    Given a dictionary of strings containing references to other strings, return another dict with
    all the strings expanded out. For example, the givent dict:

    { "foo": "dude",
      "bah": "hello !foo!" }

    might result in the following returned dict:

    { "foo": "dude",
      "bah": "hello dude" }

    @param string_dict Dict of unexpanded strings.
    @param string_ref_regex A regex string that identifies string references, for example 
        "\\![a-z]+\\!".
    @param extract_key A callable with signature (str), which takes a regex match and returns the
        matching key in the string dict. If None, the exact regex match is used as the key.
    """
    key_fn = extract_key if extract_key else lambda x:x
    ref_re = re.compile(string_ref_regex)
    d_in = string_dict.copy()
    d_out = {}

    while d_in:
        b = False
        for k,v in d_in.items():
            refs = ref_re.findall(v)
            unknown_refs = [x for x in refs if key_fn(x) not in d_out]
            if not unknown_refs:
                for ref in refs:
                    ref_key = key_fn(ref)
                    expanded_ref = d_out[ref_key]
                    v = v.replace(ref, expanded_ref)
                del d_in[k]
                d_out[k] = v
                b = True

        if not b:
            raise ValueError("String dict contains unresolved/circular reference(s)")
    return d_out
