"""
@namespace helpy.progress Class for printing progress indication.
"""
import inspect
import threading
import time



class Progress(object):
    """
    A Progress instance will print progress indication to stdout, with a time increment so as not
    to spam the console. A typical printout might be "34%..." for example. Progress instances can
    see other instances higher in the callstack, and when this happens the output is changed to
    show one process happening as a subset of another - for example "34%|22%...".
    """
    def __init__(self, count, label=None, wait=1.0, first_wait=1.0):
        """
        Creates a Progress instance.
        @param count Total count for this progress indicator.
        @param wait Time, in seconds, to wait between printing.
        @param first_wait Like wait, but applied before the first printout.
        """
        self.count = count
        self.label = label
        self.i = 0
        self.overflow = False
        self.printed = False
        self.first = True
        self.wait = wait
        self.first_wait = max(wait, first_wait)
        self.parent = Progress._find_prog(self)
        self.t = time.time()

    def inc(self, n=1):
        """
        Increment the progress indicator.
        @param n Number to increment the count by.
        """
        if (self.count <= 0) or self.overflow:
            return

        self.i += n
        if self.i >= self.count:
            self.overflow = True

        t = time.time()
        if self.first and (t-self.t) > self.first_wait:
            self.first = False

        if (self.overflow and self.printed) or ((not self.first) and ((t-self.t) > self.wait)):
            s = self.progress_str()
            Progress._print_lock.acquire()
            print s + "..."
            Progress._print_lock.release()
            self.printed = True
            self.t = t

    def set(self, n):
        self.inc(n - self.i)

    def progress_str(self):
        s = ''
        if self.parent:
            s += self.parent.progress_str()
        if self.count <= 0:
            return s

        if self.parent:
            s += '|'
        if self.label:
            s += "(%s)" % self.label

        percent = int(100.0 * float(self.i+1) / self.count)
        percent = min(percent, 100)
        s += "%d%%" % percent
        return s

    @staticmethod
    def _find_prog(_self=None):
        for frame in inspect.stack():
            locvars = frame[0].f_locals
            for name,obj in locvars.iteritems():
                if isinstance(obj, Progress) and (obj != _self):
                    return obj

    _print_lock = threading.Lock()
