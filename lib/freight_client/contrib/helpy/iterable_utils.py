"""
@namespace helpy.iterable_utils Utility functions for general iterable sequences.
"""



def replace(values, value, new_value):
    """
    @returns New list with each instance of value replaced with new_value.
    """
    return [new_value if x==value else x for x in values]


def remove_items(values, indexes):
    """
    In-place removes the given index items from the given random-access iterable.
    """
    idxs = sorted(list(indexes), reverse=True)
    for i in idxs:
        del values[i]


def remove_duplicates(values, key=None):
    """
    @params key Comparator function.
    @returns List with all repeated values removed, leaving only the values that first appear.
    """
    if key is None:
        def key(x): return x
    seen = set()
    result = []
    for item in values:
        marker = key(item)
        if marker in seen: continue
        seen.add(marker)
        result.append(item)
    return result


def get_common_prefix_count(a, b):
    """
    @returns The number of leading values of iterable a and b that are the same.
    """
    for i,e in enumerate(zip(a,b)):
        if e[0] != e[1]:
            return i
    return i+1


def group_by(values, fn):
    """
    Given a list of values, returns the dict {K: [values]}, where K is the result of evaluating 
    fn(value).
    """
    d = {}
    for val in values:
        k = fn(val)
        if k in d:
            d[k].append(val)
        else:
            d[k] = [val]
    return d


def group_by_longest_common_prefix(values):
    """
    Given a list of iterable objects, group them by the longest common prefix.

    For example, consider the following strings:
    - /usr/foo/bah.txt      /usr/foo/
    - /usr/foo/meh/eek.txt  /usr/foo/
    - /bin                  /bin
    - /bin/misc             /bin
    - /usr/test.a           /usr/test.a

    The return value would be: {
        "/usr/foo/":    ["bah.txt","meh/eek.txt"],
        "/bin":         ["","/misc"],
        "":             ["/usr/test.a"]
    }

    @returns list of (common_prefix, [suffix]) tuples.
    """
    groups = []
    vals = sorted(values)

    while vals:
        longest_prefix = []
        for i in range(len(vals)-1):
            j = get_common_prefix_count(vals[i], vals[i+1])
            if j > len(longest_prefix):
                longest_prefix = vals[i][:j]
        if not longest_prefix:
            break

        n = len(longest_prefix)
        group = [x for x in vals if (x[:n] == longest_prefix)]
        vals = [x for x in vals if x not in group]
        groups.append((longest_prefix, [x[n:] for x in group]))

    if vals:
        empty_prefix = type(vals[0])()
        groups.append((empty_prefix, list(vals)))
    return groups


def group_by_longest_common_suffix(values):
    """
    Opposite of group_by_longest_common_prefix.
    @returns list of (common_suffix, [prefix]) tuples.
    """
    rev_values = [x[::-1] for x in values]
    rev_groups = group_by_longest_common_prefix(rev_values)
    groups = []
    for rev_group in rev_groups:
        prefix,rev_vals = rev_group
        suffix = prefix[::-1]
        vals = [x[::-1] for x in rev_vals]
        groups.append((suffix, vals))
    return groups


def iterables_to_tree(values):
    """
    Given a list of iterable objects, convert them to a tree structure, which is a dict of dicts
    where leaf nodes are a (value,None) tuple. (None,None) tuples signify a shorter path that
    shadows a longer one (as in the example below).

    For example, the values:
    [[1,2], [1,2,5], [1,3], [7]]
    would be converted to:
    {1:{2:{None:None, 5:None}, 3:None}, 7:None}
    """
    tree = {}
    d = {}

    for value in values:
        if value is None:
            continue
        d = tree
        n = len(value)
        for i,tok in enumerate(value):
            d2 = d.get(tok)
            if i==n-1:
                if d2 is None:
                    d[tok] = None
                else:
                    d2[None] = None
            else:
                if d2 is None:
                    if tok in d:
                        d2 = {None:None}
                    else:
                        d2 = {}
                    d[tok] = d2
                d = d2

    return tree or d
