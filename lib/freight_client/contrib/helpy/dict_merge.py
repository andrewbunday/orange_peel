import os.path
import copy


class DictMerger():
    """
    This class merges together multiple dicts. The user can specify different merging rules for
    specific keys.
    """
    
    """
    These are a set of standard 'rules' that determine what happens when merging two dicts
    together that have values stored under identical keys.
    """
    # The user is supplying their own custom rule.
    RULE_CUSTOM         = 0
    # The value is overwritten.
    RULE_OVERWRITE      = 1
    # Identical keys will raise an exception.
    RULE_ERROR          = 2
    # Values are appended in a list.
    RULE_APPEND         = 3
    # Values are added. In the case of lists, this creates a master list with all elements appended.
    # Note that if RULE_APPEND is used with lists, the result is a list of lists, not a single list.
    RULE_ADD            = 4
    # Values are expected to be documents (ie dicts). In this case, these docs are further merged 
    # by another DictMerger object.
    RULE_NESTED         = 5

    def __init__(self, default_rule=RULE_OVERWRITE, key_filter=None):
        """
        Create a document merger.
        @param default_rule The rule that will be applied to all keys that have not explicitly
            been assigned a role via add_rule(). If you want to assign a custom or nested rule as
            the default rule, use add_rule().
        @param key_filter The keys to merge, all other keys will be discarded.
        """
        self._rules = {}
        self._key_filter = copy.copy(key_filter)
        self._default_rule = DictMerger._get_rule_functor(default_rule)

    def add_rule(self, rule, key=None, rule_functor=None, doc_merger=None):
        """
        Associate a rule with a key. If a rule is already associated with the key, it is overwritten.
        @param rule The rule to apply.
        @param key The key to associate the rule with. If None, this rule becomes the default rule.
        @param rule_functor Ignored unless rule=RULE_CUSTOM. Defines a custom rule for the key. The
            given value must be a callable object with signature "(key, prev_value, value, 
            _merging_dict)", which returns the combined resulting value.
        @param doc_merger Ignored unless rule=RULE_NESTED. Defines a nested rule for the key. The
            given DictMerger object will be used to merge the values for this key.
        """
        rule_functor2 = DictMerger._get_rule_functor(rule, rule_functor, doc_merger)
        if key is None:
            self._default_rule = rule_functor2
        else:
            self._rules[key] = rule_functor2

    def add_nested_rule(self, rule, key=None):
        """
        Convenience function for creating a nested rule. If you need to do more than this function
        is able to, then construct your own DictMerger object and use add_rule() directly.
        @param rule The nested rule to apply.
        """
        merger = DictMerger(rule)
        self.add_rule(DictMerger.RULE_NESTED, key, doc_merger=merger)

    def merge(self, dicts, _merging_dict=None):
        """
        Merge a list of dicts.
        @param dicts List of documents, typically loaded from yaml files.
        @return The merged document; and a set of tuples, where each tuple is the scoped name of a
            key which has been merged.
        """
        merged_d = {}
        _merging_d = _merging_dict or {"scope":[], "merged_keys":set()}
        scope = _merging_d["scope"]
        merged_keys = _merging_d["merged_keys"]
        
        if dicts:
            merged_d = dicts[0].copy()
            if self._key_filter:
                for k in merged_d.keys():
                    if k not in self._key_filter:
                        del merged_d[k]

            for d in dicts[1:]:
                for k,v in d.iteritems():
                    if self._key_filter and k not in self._key_filter:
                        continue
                    if k in merged_d:
                        scope.append(k)
                        rule_functor = self._rules.get(k) or self._default_rule
                        merged_d[k] = rule_functor(k, merged_d[k], v, _merging_d)
                        merged_keys.add(tuple(scope))
                        scope.pop()
                    else:
                        merged_d[k] = v

        return copy.deepcopy(merged_d), merged_keys

    # Allows a DictMerger itself to act as a merge rule
    def _rule_merge(self, key, prev_value, value, _merging_dict):
        dicts = [prev_value, value]
        result = self.merge(dicts, _merging_dict)
        return result[0]

    @staticmethod
    def _get_rule_functor(rule, rule_functor=None, doc_merger=None):
        if rule == DictMerger.RULE_CUSTOM:
            if not rule_functor:
                raise Exception("Missing custom rule functor with rule=RULE_CUSTOM")
            return rule_functor
        elif rule == DictMerger.RULE_NESTED:
            if not doc_merger:
                raise Exception("Missing DictMerger object with rule=RULE_NESTED")
            return doc_merger._rule_merge
        elif rule == DictMerger.RULE_OVERWRITE:
            return DictMerger._rule_overwrite
        elif rule == DictMerger.RULE_ERROR:
            return DictMerger._rule_error
        elif rule == DictMerger.RULE_APPEND:
            return DictMerger._rule_append
        elif rule == DictMerger.RULE_ADD:
            return DictMerger._rule_add
        else:
            raise Exception("Invalid rule code supplied.")

    @staticmethod
    def _get_scoped_key(key, _merging_dict):
        return str('/').join(_merging_dict["scope"] + [key])

    @staticmethod
    def _rule_overwrite(key, prev_value, value, _merging_dict):
        return value
    
    @staticmethod
    def _rule_error(key, prev_value, value, _merging_dict):
        raise Exception("Key '%s' cannot be set more than once." \
            % DictMerger._get_scoped_key(key, _merging_dict))

    @staticmethod
    def _rule_append(key, prev_value, value, _merging_dict):
        t = tuple(_merging_dict["scope"])
        if t in _merging_dict:
            return prev_value + [value]
        else:
            _merging_dict[t] = True
            return [prev_value] + [value]

    @staticmethod
    def _rule_add(key, prev_value, value, _merging_dict):
        return prev_value + value
