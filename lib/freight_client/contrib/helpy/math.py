"""
@namespace helpy.math Math-related functions.
"""
import copy



def clamp(n, minn=0, maxn=1):
    """
    @returns n, clamped within the given values.
    """
    return max(min(maxn, n), minn)


# todo move to iterable_utils
def add(values):
    """
    @param values List of values to add together.
    @returns The sum of all values. This differs from python's built-in sum(), because it works
        for non-number types, such as lists.
    """
    if values:
        r = values[0]
        for v in values[1:]:
            r += v
        return r


def lerp(a, b, f):
    """
    Give a value linearly interpolated between two given values.
    @param a Start value, returned if f<= 0.
    @param End value, returned if f >= 0.
    @param f Interpolation fraction.
    @returns An interpolated value between a and b. What that actually means depends on the types
    involved.
    """
    f = clamp(float(f))
    t = type(a)
    assert(type(b)==t)

    def _lrp_list(x,y):
        n = min(len(x),len(y))
        return [lerp(x[i], y[i], f) for i in range(n)]

    if t==list:
        return _lrp_list(a,b)
    elif t==tuple:
        return tuple(_lrp_list(a,b))
    else:
        return t((float(a)*(1-f)) + (float(b)*f))


def lramp(ramp_entries, f):
    """
    Give a value linearly interpolated between a sequence of values.
    @param ramp_entries A list of (number, value) entries to be interpolated between.
    @param f Interpolation value.
    @returns The interpolated value of the given ramp at f.
    """
    assert(ramp_entries)
    def _r(i): return copy.deepcopy(ramp_entries[i][1])

    n = len(ramp_entries)
    if n == 1:
        return _r(0)
    elif f <= ramp_entries[0][0]:
        return _r(0)
    elif f >= ramp_entries[-1][0]:
        return _r(-1)
    else:
        i = n-1
        while f < ramp_entries[i][0]: i-=1
        v1 = ramp_entries[i]
        v2 = ramp_entries[i+1]
        j = float(f-v1[0]) / float(v2[0]-v1[0])
        return lerp(v1[1], v2[1], j)
