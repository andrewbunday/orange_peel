import multiprocessing.pool as mpool



class DummyThreadTask(object):
    def __init__(self, result):
        self.result = result
    def get(self):
        return self.result


class ThreadPool(mpool.ThreadPool):
    """
    Wrapper for multiprocessing threadpool, which doesn't use threads if the thread count
    is zero (this is useful for debugging).
    """
    def __init__(self, num_threads=0, initializer=None, initargs=()):
        super(ThreadPool,self).__init__(num_threads, initializer, initargs)

    def apply(self, func, args=(), kwds={}):
        if len(self._pool):
            return super(ThreadPool,self).apply(func, args, kwds)
        else:
            return func(*args, **kwds)

    def apply_async(self, func, args=(), kwds={}, callback=None):
        if len(self._pool):
            return super(ThreadPool,self).apply_async(func, args, kwds, callback)
        else:
            r = func(*args, **kwds)
            if callback:
                callback(r)
            return DummyThreadTask(r)
