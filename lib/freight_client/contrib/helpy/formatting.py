"""
@namespace helpy.formatting Functions for formatting text, or for interpreting formatted text.
"""
import datetime
import time



def columnise(rows, padding=4):
    """
    Given a list of tuples/lists, return a list of strings, where each string contains the tuple 
    items spaced out into even columns, separated by 'padding' number of spaces.
    """
    strs = []
    maxwidths = {}

    for row in rows:
        for i,e in enumerate(row):
            se = str(e)
            nse = len(se)
            w = maxwidths.get(i,-1)
            if nse > w:
                maxwidths[i] = nse

    for row in rows:
        s = ''
        for i,e in enumerate(row):
            se = str(e)
            if i < len(row)-1:
                n = maxwidths[i] + padding - len(se)
                se += ' '*n
            s += se
        strs.append(s)
    return strs


def pretty_byte_count(bytes):
    """
    Convert a number of bytes into readable form.
    Eg: '12Kb', '3.4Gb'.
    """
    divs = ((1024*1024*1024*1024, "Tb"), (1024*1024*1024, "Gb"), \
        (1024*1024, "Mb"), (1024, "Kb"), (1, "b"))
    s = _quantity_to_string(bytes, divs, False, False)
    return s


def pretty_time_duration(secs):
    """
    Convert a number of seconds into readable form.
    Eg: '12 seconds', '3.4 days'.
    """
    divs = ((365*24*60*60, "year"), (30*24*60*60, "month"), (24*60*60, "day"), \
        (60*60, "hour"), (60, "minute"), (1, "second"))
    s = _quantity_to_string(secs, divs, True, True)
    return s


def get_epoch_time(s):
    """
    Given a string, return the equivalent epoch time. The following formats are supported:
    * A direct epoch value, eg '1347759813';
    * An exact date in form 'Sep-16-11:43:33-2012';
    * A relative time in the past, eg '-3d', '-6h', '-4.5m', '-10s'.
    @returns The epoch time equivalent to s, or None if s does not represent a time.
    """
    try:
        return int(s)
    except:
        pass

    try:
        s2 = s.replace('-',' ')
        dt = datetime.datetime.strptime(s2, "%b %d %H:%M:%S %Y")
        return int(time.mktime(dt.timetuple()))
    except:
        pass

    try:
        if s.startswith('-'):
            chars = {'d':24*60*60, 'h':60*60, 'm':60, 's':1}
            m = chars.get(s[-1])
            if m:
                n = float(s[1:-1])
                secs = int(n * m)
                now = int(time.time())
                return max((now - secs), 0)
    except:
        pass

    return None


###############################################################################
# Internals
###############################################################################

def _split_quantity(num, divs):
    toks = {}
    for d in divs:
        if num >= d[0]:
            toks[d[1]] = num/d[0]
            num %= d[0]
        else:
            toks[d[1]] = 0
    return toks


def _quantity_to_string(num, divs, pluralise_label, insert_space):
    neg = (num < 0)
    num = abs(num)
    sep = ' ' if insert_space else ''
    toks = _split_quantity(num, divs)
    s = ''

    for i,d in enumerate(divs):
        n = toks[d[1]]
        if n or (i==len(divs)-1):
            label = d[1]

            if i<len(divs)-1:
                d2 = divs[i+1]
                f = toks[d2[1]] * d2[0] * 10 / d[0]
                if f:
                    if pluralise_label:
                        label += 's'
                    s = "%d.%d%s%s" % (n, f, sep, label)
                    break

            if n != 1 and pluralise_label:
                label += 's'
            s = "%d%s%s" % (n, sep, label)
            break

    return '-'+s if neg else s
