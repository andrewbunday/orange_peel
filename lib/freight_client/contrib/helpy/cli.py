"""
@namespace helpy.cli Misc command-line-related functions.
"""
from optparse import OptionParser
import formatting
import sys



def parse_command_options(commands, args=None):
    """
    Parse options for a program that provides multiple different sub-actions, much like git or svn,
    for example "git status", "git clone".
    @param commands A list of (name, description) tuples that describe the available actions.
    @param args Arguments to parse, defaults to sys.argv.
    @returns (command,args), where args are those that appear after the action, or None if no
        action was selected.
    """
    usage = "usage: %prog COMMAND [--help] [ARGS]\n" + \
        "Available commands are:\n  "

    non_hidden_cmds = [x for x in commands if not x[0].startswith('.')]
    cmd_strs = formatting.columnise(non_hidden_cmds)
    usage += str("\n  ").join(cmd_strs)

    p = OptionParser(usage=usage)
    args_ = sys.argv[1:] if args is None else args
    if args_:
        action = args_[0]
        if action.lower() not in ["--help","-help","--?","-?"]:
            if action in [name for name,desc in commands]:
                return action, args_[1:]
            elif ('.'+action) in [name for name,desc in commands]:
                return action, args_[1:]
            else:
                p.error("'%s' is not a recognised command" % action)
            return

    p.print_help()
    return None
