
def brief_dict_diff(d1_, d2_, max_lines=8, max_line_length=100):
    """
    @return A list of tuples summarising the differences between d1 and d2. The description is
    meant to be brief - for example, values of changed keys might only be shown if the value is
    short enough to be displayed succinctly. Each tuple is of the form (key_tuple, summary), where
    key_tuple is the scoped key name for each change.
    """
    def _scoped_key(scope, k):
        t = tuple(list(scope) + [str(k)])
        return t, str('/').join(t)

    def _list_diff(l1, l2):
        n1 = len(l1)
        n2 = len(l2)

        if n2 > n1:
            if l2[:n1] == l1:
                return "'%s' list was appended with " + str(l2[n1:])
            if l2[n2-n1:] == l1:
                return "'%s' list was prefixed with " + str(l2[:n2-n1])               

        try:
            s1 = set(l1)
            s2 = set(l2)
        except:
            return None
        common_elems = s1 & s2
        if not common_elems:
            return None

        removed_elems = s1 - s2
        added_elems = s2 - s1
        if not removed_elems and not added_elems:
            return "'%s' list changed order"

        if n1 == n2:
            elems = []
            for i in range(n1):
                if l1[i] != l2[i]:
                    elems.append((i, l1[i], l2[i]))
            if len(elems) <= min(1+n1/4, 4):
                msgs = ["item %d changed from '%s' to '%s'" % \
                    (x[0],str(x[1]),str(x[2])) for x in elems]
                msg = "'%s' list had " + str(", and ").join(msgs)
                return msg

        msg = "'%s' list had "
        if removed_elems:
            msg += "removed: %s" % str(removed_elems)
        if added_elems:
            if removed_elems:
                msg += ", and "
            msg += "added: %s" % str(added_elems)
        return msg

    def _dd(d1, d2, depth, maxdepth, scope):
        s = {}
        sk1 = set(d1.keys())
        sk2 = set(d2.keys())
        removed_keys = sk1 - sk2
        added_keys = sk2 - sk1

        common_keys = sk1 & sk2
        for k in common_keys:
            scope_k,sk = _scoped_key(scope,k)
            v1 = d1[k]
            v2 = d2[k]

            if v1 != v2:
                if type(v1) == type(v2):
                    if (type(v1) == dict) and depth < maxdepth:
                        s.update(_dd(v1, v2, depth+1, maxdepth, scope_k))
                        continue
                    if (type(v1) == list):
                        msg = _list_diff(v1, v2)
                        if msg and len(msg) < max_line_length:
                            s[scope_k] = msg % sk
                            continue

                msg = "'%s' changed from '%s' to '%s'" % (sk, str(d1[k]), str(d2[k]))
                if len(msg) > max_line_length:
                    msg = "'%s' was changed" % sk
                s[scope_k] = msg

        for k in removed_keys:
            scope_k,sk = _scoped_key(scope,k)
            msg = "'%s' was removed, value was: '%s'" % (sk, str(d1[k]))
            if len(msg) > max_line_length:
                msg = "'%s' was removed" % sk
            s[scope_k] = msg

        for k in added_keys:
            scope_k,sk = _scoped_key(scope,k)
            msg = "'%s' was added with value: '%s'" % (sk, str(d2[k]))
            if len(msg) > max_line_length:
                msg = "'%s' was added" % sk
            s[scope_k] = msg

        return s

    s = {}
    sprev = {}
    maxdepth = 0

    while len(s) <= max_lines:
        s = _dd(d1_, d2_, 0, maxdepth, [])
        if s == sprev:
            break
        maxdepth += 1
        if (len(s) <= max_lines) or not sprev:
            sprev = s

    return sprev
