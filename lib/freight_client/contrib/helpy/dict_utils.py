"""
@namespace helpy.dict_utils Functions for misc dict operations.
"""


def get_in_place(d, key, default_value):
    """
    @returns d[key] if the key exists, otherwise sets the key to default_value, and returns the
        value that is now in the dict.
    """
    if key in d:
        return d[key]
    d[key] = default_value
    return default_value


def to_ordered_list(d, l):
    """
    Convert a dict to an ordered list.
    @param d Dict to convert.
    @param l List of keys.
    @returns List of (k,v) tuples, where the key order matches the items in l, and any keys not
        in l are at the end of the list, in no particular order.
    """
    r = []
    d2 = d.copy()
    for k in l:
        if k in d2:
            r.append((k, d2[k]))
            del d2[k]
    r += d2.items()
    return r


def dict_intersection(dicts):
    """
    @param dicts List of dicts.
    @returns A dict containing entries common to all given dicts.
    """
    if not dicts:
        return {}
    
    i = iter(dicts)
    d = i.next().copy()
    for d_ in i:
        for k,v in d_.iteritems():
            if k in d and d[k] != v:
                del d[k]
    return d


def get_recurse(d, key_tuple):
    """
    Get a value in the given dict, or in dicts within the dict.
    @param d The dict to search.
    @param key_tuple The scoped name of the key to find. For eg ('foo','bah') is expecting to find
        the key 'bah' in the dict d['foo'].
    @return A 4-tuple, where:
        [0] is an error message, or None if the key was found;
        [1] is the value of the key, or None;
        [2] is the last object searched, which will be the dict containing the key on success.
        [3] is the scoped key of [2] (which is [] if [2] is 'd').
    """
    v = d
    scope = []
    parent_d = None

    for i,k in enumerate(key_tuple):
        if type(v) == dict:
            if k not in v:
                errmsg = "'%s' does not exist" % str('/').join([str(x) for x in key_tuple[:i+1]])
                return (errmsg, None, v, scope)
        else:
            errmsg = "'%s' is not a dict" % str('/').join([str(x) for x in key_tuple[:i]])
            return (errmsg, None, v, scope)
        
        parent_d = v
        scope.append(k)
        v = v[k]

    return (None, v, parent_d, scope[:-1])


def set_recurse(d, key_tuple, value):
    """
    Set a value in the given dict, or in dicts within the dict. If the necessary nested dicts do
    not exist, they will be created.
    @param d The dict to set a value within.
    @param key_tuple The scoped name of the key to set.
    @param value The value to set.
    """
    errmsg,v,obj,obj_scope = get_recurse(d, key_tuple)
    if errmsg:
        if type(obj) == dict:
            extra_dicts = key_tuple[len(obj_scope):-1]
            for ed in extra_dicts:
                obj[ed] = {}
                obj =obj[ed]
        else:
            raise Exception(errmsg)
    
    obj[key_tuple[-1]] = value
