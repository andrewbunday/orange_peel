"""
@namespace helpy.osys OS-related functions.
"""
import os
import pwd
import os.path
import glob
import fnmatch
import subprocess as sp



def get_file_owner(filepath):
    return pwd.getpwuid(os.stat(filepath).st_uid).pw_name


def list_files(path, include_hidden=False):
    """
    @returns List of paths to files in the given path.
    """
    paths = []
    for fname in os.listdir(path):
        if not include_hidden and fname.startswith('.'):
            continue
        fpath = os.path.join(path, fname)
        if os.path.isfile(fpath):
            paths.append(fpath)
    return paths


def list_dirs(path, include_hidden=False):
    """
    @returns List paths to directories in the given path.
    """
    paths = []
    for fname in os.listdir(path):
        if not include_hidden and fname.startswith('.'):
            continue
        fpath = os.path.join(path, fname)
        if os.path.isdir(fpath):
            paths.append(fpath)
    return paths


def which(filename):
    """
    Analogy to bash's "which" command.
    @returns Full path to the executable, or None if not found.
    """
    p = sp.Popen("which "+filename, shell=True, stdout=sp.PIPE, stderr=sp.PIPE)
    s_out,s_err = p.communicate()
    return None if p.returncode else s_out.strip()


def multiglob(*paths, **kwargs):
    """
    Like python's native glob.glob(), but accepts multiple paths
    @param make_abs: Force all paths to absolute (default: False)
    @returns List of files matching the glob expressions.
    """
    make_abs = kwargs.get("make_abs")
    s = set()

    for path in paths:
        if make_abs:
            path = os.path.abspath(path)
        try:
            s |= set(glob.glob(path))
        except:
            pass
    return list(s)


def find(paths=None, recurse=False, name=None, get_files=True, get_dirs=True, \
    skip_hidden_files=True, skip_hidden_dirs=True, files_callback=None, dir_callback=None, \
    dirs_callback=None):
    """
    Analogy to bash's "find" command.
    @param paths Filepath and/or paths to search, this can include wildcards, eg "*.txt". If not 
        supplied, current working directory is used.
    @param recurse If True, recurse into subdirectories.
    @param name Glob-like regex for filtering files, eg "*.txt". You can provide a list here, 
        eg ["*.txt","*.exr"] will match all txt and exr files. Analogous to find's "name" argument. 
        Note that this filter is not applied to directories for the purpose of recursion, they
        are always recursed into.
    @param get_files Match files.
    @param get_dirs Match directories.
    @param files_callback A callable with signature (list). This is called every time a set of
        files is found in a directory.
    @param dir_callback A callable with signature (str). This is called every time a directory is
        recursed into. If this callback returns False, this directory will not be recursed into. If
        it returns None, all recursion will be stopped, and the function will return.
    @param dirs_callback A callable with signature (list). This is called every time a set of
        directories is found.
    @returns A list of paths matching the search.
    """
    result = []
    if paths is None:
        paths = ['*']
    elif isinstance(paths, basestring):
        paths = [paths]
    if isinstance(name, basestring):
        name = [name]

    paths_ = multiglob(*paths)
    _find(result, paths_, recurse, name, get_files, get_dirs, skip_hidden_files, \
        skip_hidden_dirs, files_callback, dir_callback, dirs_callback)
    return result


def _find(result, paths, recurse, name, get_files, get_dirs, skip_hidden_files, \
    skip_hidden_dirs, files_callback, dir_callback, dirs_callback):
    def _name_match(_path):
        bname = os.path.basename(_path)
        return (True in [fnmatch.fnmatch(bname,x) for x in name])

    if get_files or files_callback:
        files = []
        for path in paths:
            if os.path.isfile(path):
                if skip_hidden_files and os.path.basename(path).startswith('.'):
                    continue
                if name:
                    if _name_match(path):
                        files.append(path)
                else:
                    files.append(path)
        if files:
            if files_callback:
                files_callback(files)
            elif get_files:
                result += files

    if get_dirs or recurse or dirs_callback:
        matched_dirpaths = []
        dirpaths = []

        for path in paths:
            if os.path.isdir(path):
                if skip_hidden_dirs and os.path.basename(path).startswith('.'):
                    continue
                dirpaths.append(path)
                if name:
                    path_ = path.rstrip(os.path.sep)
                    if _name_match(path_):
                        matched_dirpaths.append(path)
                else:
                    matched_dirpaths.append(path)

        if matched_dirpaths:
            if dirs_callback:
                dirs_callback(matched_dirpaths)
            if get_dirs:
                result += matched_dirpaths

        if recurse:
            for path in dirpaths:
                if dir_callback:
                    r = dir_callback(path)
                    if r is None:
                        return
                    elif not r:
                        continue
                
                fnames = os.listdir(path)
                paths2 = [os.path.join(path,x) for x in fnames]
                _find(result, paths2, recurse, name, get_files, get_dirs, \
                    skip_hidden_files, skip_hidden_dirs, files_callback, \
                    dir_callback, dirs_callback)
