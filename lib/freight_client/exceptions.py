import requests.exceptions
import errno as errno_
import util



class NeverException(Exception):
    """ An exception that is never called """
    pass

class FreightBaseException(Exception):
    """ Base class for all Freight exceptions """
    pass


#
# Server-side exceptions
#

class FreightException(FreightBaseException):
    def __init__(self, site, error_msg, **kwargs):
        super(FreightBaseException,self).__init__("site(%s): %s" % (site, error_msg))
        self.params = kwargs
        self.params["site"] = site
        if "msg" not in kwargs:
            self.params["msg"] = error_msg

    def encode_json(self):
        d = self.params.copy()
        d["exception_class"] = self.__class__.__name__
        return d

    @staticmethod
    def decode_json(doc):
        ex_class = doc.get("exception_class")
        if not ex_class:
            return None

        doc = dict((str(k),v) for k,v in doc.iteritems() if k not in ( \
            "exception_class",
            "error_message",
            "caught_exception"
        ))
        stmt = "%s(**doc)" % ex_class
        return eval(stmt)


class FreightRuntimeException(FreightException):
    """
    A non-freight exception was caught in the server.
    """
    def __init__(self, site, msg, **kwargs):
        FreightException.__init__(self, site, msg, **kwargs)


class FreightInternalError(FreightException):
    """
    An unexpected error occurred.
    """
    def __init__(self, site, msg):
        FreightException.__init__(self, site, msg)


class FreightSiteError(FreightException):
    """
    There was a problem accessing a site.
    """
    def __init__(self, site, target_site, msg):
        FreightException.__init__(self, site, msg, target_site=target_site)


class FreightBadRequestError(FreightException):
    """
    An argument to the server API was incorrect.
    """
    def __init__(self, site, msg):
        FreightException.__init__(self, site, msg)


class FreightServiceError(FreightException):
    """
    There was a problem with a service related to freight - for example, an expected database
    server was not running.
    """
    def __init__(self, site, msg):
        FreightException.__init__(self, site, msg)


class FreightResourceError(FreightException):
    """
    A resource-related error occured; for example, the server was asked for a transfer that does
    not exist.
    """
    def __init__(self, site, msg):
        FreightException.__init__(self, site, msg)


class FreightIOError(FreightException):
    """
    An IO error occurred on the server. For example, the server was asked to transfer a file that 
    does not exist at the source site.
    """
    def __init__(self, site, path, msg="No such file", errno=errno_.ENOENT):
        error_msg = "%s - %s" % (msg,path)
        FreightException.__init__(self, site, error_msg, path=path, msg=msg, errno=errno)


class FreightIntegrityError(FreightException):
    """
    The client tried to perform an action on the server, but supplied incorrect accompanying
    information. For example, a user tried to associate a hash with a remote file, but provided
    a mismatching last-modified-date or filesize.
    """
    def __init__(self, site, path, msg):
        FreightException.__init__(self, site, msg, path=path)


class FreightCopyError(FreightException):
    """
    An error occurred during a file copy.
    """
    def __init__(self, backend, site, src_site, src_path, dest_site, dest_path, src_host, \
        dest_host, msg):
        copy_msg = util.get_copy_summary_string(dict( \
            transfer_backend=backend,
            src_site=src_site,
            src_path=src_path,
            dest_site=dest_site,
            dest_path=dest_path,
            src_host=src_host,
            dest_host=dest_host,
            controller_site=site
        ))
        error_msg = "Error during copy (%s): %s" % (copy_msg, msg)
        FreightException.__init__(self, site, error_msg, \
            backend=backend,
            src_site=src_site,
            src_path=src_path,
            dest_site=dest_site,
            dest_path=dest_path,
            msg=msg)


class FreightCopyRefusalError(FreightCopyError):
    """
    A site daemon or a transfer backend refused to copy a file. Unlike a FreightCopyError, 
    this signifies that repeated attempts at the same file copy will continue to fail.
    """
    def __init__(self, backend, site, src_site, src_path, dest_site, dest_path, \
        src_host, dest_host, msg):
        copy_msg = util.get_copy_summary_string(dict( \
            transfer_backend=backend,
            src_site=src_site,
            src_path=src_path,
            dest_site=dest_site,
            dest_path=dest_path,
            src_host=src_host,
            dest_host=dest_host,
            controller_site=site
        ))
        error_msg = "Copy was refused (%s): %s" % (copy_msg, msg)
        FreightException.__init__(self, site, error_msg, \
            backend=backend,
            src_site=src_site,
            src_path=src_path,
            dest_site=dest_site,
            dest_path=dest_path,
            src_host=src_host,
            dest_host=dest_host,
            msg=msg)


#
# Client-side exceptions
#

class FreightClientException(FreightBaseException):
    pass

class ConnectionError(FreightClientException):
    pass

class HTTPError(FreightClientException):
    pass

class TimeoutError(FreightClientException):
    pass

def _raise(e):
    if isinstance(e, requests.exceptions.ConnectionError):
        raise ConnectionError(str(e))
    elif isinstance(e, requests.exceptions.HTTPError):
        raise HTTPError(str(e))
    elif isinstance(e, requests.exceptions.TimeoutError):
        raise TimeoutError(str(e))
    else:
        raise FreightClientException(str(e))
