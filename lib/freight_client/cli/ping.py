import sys
import util
from freight_client.exceptions import FreightClientException



def run(parser):
    try:
        doc = util.client.ping()
    except FreightClientException as e:
        print >> sys.stderr, str(e)
        sys.exit(1)

    util.print_columnised_dict(doc)
