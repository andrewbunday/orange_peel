import sys
import util
import freight_client.exceptions as err



def run(parser):
    try:
        docs = util.client.get_files()
    except err.FreightBaseException as e:
        print >> sys.stderr, str(e)
        sys.exit(1)

    rows = [
        ["FILE","LMD","BYTES","HASH"],
        ["----","---","-----","----"]
    ]

    for doc in docs:
        uri = "%s:%s" % (doc["site"], doc["path"])
        rows.append((uri, doc["last_modified"], doc["filesize"], doc.get("hash",'')))

    util.print_columns(rows)
