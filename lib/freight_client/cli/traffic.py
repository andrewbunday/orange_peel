import sys
import util
import freight_client.exceptions as err
import freight_client.util as futil
from datetime import datetime



def _t(p, param, s):
    if s is None:
        return s
    try:
        return float(s)
    except ValueError:
        pass

    try:
        return datetime.strptime(s, futil.datetime_format)
    except ValueError:
        p.error("Invalid value for %s: '%s'" % (param, s))


def run(parser, restrict, start, stop, step, SITE):
    if SITE is None:
        edges = None
    elif SITE == "ALL":
        edges = [(None,None)]
    else:
        edges = [(SITE,None),(None,SITE)]

    start = _t(parser, "start", start)
    stop = _t(parser, "stop", stop)

    try:
        doc = util.client.get_traffic(edges, \
            restrict=restrict,
            start=start,
            stop=stop,
            step=step
        )
    except err.FreightBaseException as e:
        print >> sys.stderr, str(e)
        sys.exit(1)

    rows = [
        ["SITE","OUTBOUND(KBps)","INBOUND(KBps)"],
        ["----","--------------","-------------"]
    ]

    def _get_kbps(src, dest):
        v = doc.get((src, dest))
        if v is None:
            vals = [v for k,v in doc.iteritems() if k[1]==dest] \
                if src is None else [v for k,v in doc.iteritems() if k[0]==src]
            if vals:
                v = [sum(x) for x in zip(*vals)]
            else:
                return ['-']

        if v:
            return [("%g"%x) for x in v]
        else:
            return [0]

    sites = set([x[0] for x in doc.iterkeys()])
    sites |= set([x[1] for x in doc.iterkeys()])
    sites -= set([None])

    for site in sites:
        out_kbps = _get_kbps(site, None)
        in_kbps = _get_kbps(None, site)

        n = max(len(out_kbps), len(in_kbps))
        for l in [out_kbps, in_kbps]:
            if len(l) < n:
                l += l[-1] * (n-len(l))

        s = site
        for out_in in zip(out_kbps, in_kbps):
            rows.append((s, out_in[0], out_in[1]))
            s = ''

    util.print_columns(rows)
