import sys
import util
import freight_client.exceptions as err
import freight_client.util as futil
import freight_client.const as fconst



def run(parser, SRC, DEST, ssite, dsite, shost, dhost, dtb, block, verbose, bwlimit, \
    error_at, throw_at):
    dest_site,dest_path,dest_host = util.split_sitepath(DEST)

    if len(SRC) == 1:
        src_site,src_path,src_host = util.split_sitepath(SRC[0])
    else:
        src_site = None
        src_host = None
        src_path = SRC

    src_site = ssite or src_site
    dest_site = dsite or dest_site
    src_host = shost or src_host
    dest_host = dhost or dest_host

    try:
        r = util.client.copy_file( \
            src_site=src_site,
            src_path=src_path,
            dest_site=dest_site,
            dest_path=dest_path,
            src_host=src_host,
            dest_host=dest_host,
            transfer_backend=dtb,
            block=block,
            verbose=verbose,
            bwlimit=bwlimit,        # rsync only
            error_at=error_at,      # rsync only
            throw_at=throw_at       # rsync only
        )
    except err.FreightBaseException as e:
        print >> sys.stderr, str(e)
        sys.exit(1)

    if isinstance(r, tuple):
        copy_handle,status_doc = r
        if verbose: print
        print copy_handle
        print futil.get_copy_summary_string(status_doc, show_all=True)

        if status_doc.get("result") != fconst.CopyResult.SUCCESS:
            sys.exit(status_doc.get("exit_code") or 1)
    else:
        copy_handle = r
        print copy_handle
