import sys
import util
import freight_client.util as fcutil
import freight_client.exceptions as err



def get_file(path):
    try:
        doc = util.client.get_file(path)
    except err.FreightBaseException as e:
        print >> sys.stderr, str(e)
        sys.exit(1)
    util.print_columnised_dict(doc)


def set_file_hash(path, lmd, filesize, hsh):
    try:
        doc = util.client.set_file_hash(path, lmd, filesize, hsh)
    except err.FreightBaseException as e:
        print >> sys.stderr, str(e)
        sys.exit(1)
    util.print_columnised_dict(doc)


def local_set_file_hash(path):
    try:
        doc = util.client.get_file(path)
    except err.FreightBaseException as e:
        print >> sys.stderr, str(e)
        sys.exit(1)

    if "hash" not in doc:
        hsh = fcutil.get_hash(path)
        set_file_hash(path, doc["last_modified"], doc["filesize"], hsh)
    else:
        util.print_columnised_dict(doc)


def run(parser, files, set_hash, local_set_hash):
    if set_hash and local_set_hash:
        parser.error("--set-hash and --local-set-hash cannot be used together")

    path = files[0]
    if set_hash:
        set_file_hash(path, *set_hash)
    elif local_set_hash:
        local_set_file_hash(path)
    else:
        get_file(path)
