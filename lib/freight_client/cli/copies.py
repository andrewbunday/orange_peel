import sys
import util
from freight_client import const
import freight_client.util as futil
import freight_client.exceptions as err



def _get_file_copies(parser, controller_site, status, order):
    try:
        status_ = futil.to_enum(status, const.CopyStatus, True) if status else None
        order_ = futil.to_enum(order, const.CopyOrder) if order else None
    except ValueError as e:
        parser.error(str(e))

    try:
        docs = util.client.get_file_copies( \
            controller_site=controller_site,
            status=status_,
            order=order_
        )
    except err.FreightBaseException as e:
        print >> sys.stderr, str(e)
        sys.exit(1)

    rows = [
        ["HANDLE","CONTROLLER","SRC","DEST","STATUS"],
        ["------","----------","---","----","------"]
    ]

    for doc in docs:
        uri = doc["copy_handle"]
        contr = "%s@%s" % (doc["transfer_backend"], doc["controller_site"])
        src = futil.location_string(doc["src_site"], doc["src_path"], doc.get("src_host"))
        dest = futil.location_string(doc["dest_site"], doc["dest_path"], doc.get("dest_host"))        
        status = futil.get_copy_status_string(doc)
        rows.append((uri, contr, src, dest, status))

    util.print_columns(rows)


def _get_file_copy(handle, all_):
    try:
        doc = util.client.get_file_copy_status(handle, all=all_)
    except err.FreightBaseException as e:
        print >> sys.stderr, str(e)
        sys.exit(1)

    if all_:
        futil.pretty_print(doc)
    else:
        print futil.get_copy_summary_string(doc)


def run(parser, handle, all_, controller_site, status, order):
    if handle:
        _get_file_copy(handle, all_)
    else:
        _get_file_copies( \
            parser,
            controller_site=controller_site,
            status=status,
            order=order
        )
