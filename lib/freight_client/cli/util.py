import re
import os
import os.path
import sys
import freight_client.client
from freight_client.exceptions import FreightClientException



default_host = "localhost"
default_port = 5000
client = None
re_sitepath = re.compile("([^/]+?):(.*)")
re_sitehost = re.compile("([^,]+?),(.*)")


def set_client(host_, port_):
    global client
    host = host_ or os.getenv("FREIGHT_HOST", default_host)
    port = port_ or int(os.getenv("FREIGHT_PORT", default_port))

    try:
        client = freight_client.client.Client(host, port)
    except FreightClientException as e:
        print >> sys.stderr, str(e)
        sys.exit(1)

def print_columns(rows, buf=sys.stdout, padding=2):
    if not rows: return
    widths = [max([len(str(y)) for y in x])+padding for x in zip(*rows)]
    widths[-1] = 0
    for row in rows:
        print >> buf, "".join((str(val).ljust(width) for val,width in zip(row, widths)))

def print_columnised_dict(d, buf=sys.stdout, padding=2):
    if not d: return
    rows = [(k,str(v)) for k,v in d.iteritems()]
    rows.sort()
    print_columns(rows, buf, padding)

def split_sitepath(s):
    site = None
    path = None
    host = None

    m = re_sitepath.match(s)
    if m and len(m.groups()) == 2:
        site,path = m.groups()
        m2 = re_sitehost.match(site)
        if m2:
            site,host = m2.groups()
    else:
        path = s

    return site,path,host
