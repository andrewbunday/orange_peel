import sys
import util
import freight_client.exceptions as err



def run(parser, HANDLE):
    try:
        docs = util.client.get_log_entries(HANDLE)
    except err.FreightBaseException as e:
        print >> sys.stderr, str(e)
        sys.exit(1)

    rows = [
        ["HOST","SITE","MESSAGE"],
        ["----","----","-------"]
    ]

    for doc in docs:
        host_str = "%s@%s" % (doc["user"], doc["host"])
        dtb = doc.get("transfer_backend")
        site_str = ("%s@%s" % (dtb, doc["site"])) if dtb else doc["site"]
        rows.append((host_str, site_str, doc["msg"]))

    util.print_columns(rows)
