import sys
import util
import freight_client.exceptions as err



def run(parser, update=False):
    try:
        docs = util.client.get_sites(update=update)
    except err.FreightBaseException as e:
        print >> sys.stderr, str(e)
        sys.exit(1)

    rows = [
        ["SITE","HOST:PORT","PING TIME"],
        ["----","---------","---------"]
    ]

    for doc in docs:
        site = doc["site"]
        ch = '*' if site == util.client.site else ' '
        url = "%s:%d" % (doc["host"], doc["port"])
        secs = "%gs" % doc["seconds"]
        rows.append([ch+site, url, secs])

    util.print_columns(rows)
