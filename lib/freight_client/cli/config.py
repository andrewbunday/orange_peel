import sys
import util
import freight_client.util as futil
import freight_client.exceptions as err



def run(parser):
    try:
        doc = util.client.get_config()
    except err.FreightBaseException as e:
        print >> sys.stderr, str(e)
        sys.exit(1)

    futil.pretty_print(doc)
