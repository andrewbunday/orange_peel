
class Enum(object):
    pass


class CopyStatus(Enum):
    """ File copy status codes """
    # the copy has been paused
    PAUSED = 0
    # The copy is awaiting execution
    PENDING = 1
    # The copy was active, but the backend appears to have ceased operating
    DORMANT = 2
    # The copy is in progress
    ACTIVE = 3
    # The copy has ended - this does not necessarily mean it succeeded
    ENDED = 4

    label = {
        PAUSED:"PAUSED",
        PENDING:"PENDING",
        DORMANT:"DORMANT",
        ACTIVE:"ACTIVE",
        ENDED:"ENDED"
    }


class CopyResult(Enum):
    """ File copy result codes """
    # The copy completed successfully
    SUCCESS = 0
    # The copy did not occur - it was refused because it is not possible. For example, the site
    # may not have the requested backend; the backend may not be able to do what is asked; the
    # backend may be lacking config data it needs in order to function.
    REFUSED = 1
    # The copy failed - an error occured during the copy
    FAILURE = 2
    # The copy was cancelled
    CANCELLED = 3

    label = {
        SUCCESS:"SUCCESS",
        REFUSED:"REFUSED",
        FAILURE:"FAILURE",
        CANCELLED:"CANCELLED"        
    }


class CopyOrder(Enum):
    """ File copy sort order settings """
    # Sort from highest priority to lowest
    PRIORITY = 0
    # Sort from most complete to least priority
    COMPLETENESS = 1

    label = {
        PRIORITY:"PRIORITY",
        COMPLETENESS:"COMPLETENESS"
    }

    sort = {
        PRIORITY:       [("priority",-1)],
        COMPLETENESS:   [("status",-1),("percent_complete",-1),("priority",-1)]
    }


class TransferStatus(Enum):
    """ Transfer copy status codes """
    # The system is determining how and where the transfer should occur - what backend should
    # be used, and what site(s) should be involved.
    ROUTING = 0
    # A file copy is occurring
    COPYING = 1
    # The transfer has ended - this does not necessarily mean it succeeded
    ENDED = 2


class TransferResult(Enum):
    """ Transfer copy result codes """
    # The transfer completed successfully
    SUCCESS = 0
    # The transfer failed - see the last child copy result as to why
    FAILURE = 1
    # The transfer was cancelled.
    CANCELLED = 2


class JobPriority(Enum):
    """ Priority of a copy, transfer etc """
    # Highest priority
    HIGHEST = 0
    # Lowest priority
    LOWEST = 1
