#!/usr/bin/env python

"""entity.py: Support for production entities (Project, Sequence, Shot, Task, Version, EventLog, etc.)
    that use a standard backing store (like Shotgun, Mongo, MySQL, Postgres, CouchDB, Hadoop, etc.);
    The entities that inherit from Entity all use the same instance of backing store;
    The entities provide their own name mapping, although if no mapping exists, the raw keys are
    available;"""

# Copyright (c) 2013 Wook

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

if True:
    import copy
    import os
    import time
    import types
    import bson

    import backing_store

class Entity(object):
    """Entity support for Shotgun Applications:
       bs		Backing Store (a FRIEND of Entity, looks at _blah;)
       _data	Dictionary fresh out of the backing store
       _map	Translations from Friendly names to raw names
       _magic	List of keys that provide transformative functions    """
    keyCache = dict() # list of original (raw) keys by etype
    dataCache = dict() # { 'Version':{id:ObjRef,...,id:ObjRef} }
    bs = None
    time_all = False
    XDICT = dict() # Map HumanUser=artist.Artist,CustomEntity...=FacilitySpecific,...
    def __init__(self, etype, _map, _magicmap, _id=None, _code=None, **kwa):
        """ Entity(etype) -- get a null entity, ready to populate
            Entity(etype, **{'id':_id}) -- load an existing entity
            Entity(etype, **{'name':nm}) -- load an existing entity
            Entity(etype, f1=v, f2=v2, f3=v3) -- create an entity initialize by friendly names
            x = entity[friendly]
            entity[friendly] = x"""
        self._data = dict()
        self._auxData = dict()
        self._changed = set()
        self.update_entries = [ ]
        self._etype = etype
        self._ename = self._etype
        self._debug = False
        self._timing = self.elapsed = False
        self.start = time.time()
        self._iterlist = [ ]
        self._eDict = dict()
        if '_auxOnly' not in self.__dict__: self._auxOnly = False
        if '_default' not in self.__dict__:  self._default = [ 'id', 'code', 'project' ]
        # These maps are initially populated with the entity level (global) magic functions
        # Tip: When adding a magic name, it's mapping needs to be in _map,
        # TODO: Fix above requirement, it's stupid;
        #      it's (semi-)reversible method pointer in _magic
        self._map = dict(parentId='parent', parentName='parent',  # TODO: Are these really needed?
                         # Default Magic Methods need a default mapping
                         projectId='project', projectName='project', projectCode='project',
                         typeLink='type', pathFP='sg_path', hexId='type', tentativeLink='type', eDict='type')
        self._magic = dict(parentId = self._parentId, parentName = self._parentName,
                           projectId = self._projectId, projectName = self._projectName,
                           projectCode = self._projectCode, pathFP=self._pathFP,
                           typeLink=self._typeLink, hexId = self._hexId, tentativeLink = self._tentativeLink,
                           eDict=self._eDictRef)
        self._map.update(_map)         # map is friendly name:rawname
        self._reverse = self._reverse_dict(self._map)
        self._magic.update(_magicmap)  # to take paths and such and xform into link fields
        # Magic functions: def _magic_fnc(self, v, invert=False)
        # invert means reverse the magic, ie: link-field to string vs string to link-field
        # alternate = kwa.get('_alternate', None)
        setup_bs = (not Entity.bs) or ('_storage_mode' in kwa)
        if setup_bs:
            bs_mode = kwa.get('_storage_mode', 'prod')
            if '_key' in kwa:
                api_keyname = kwa['_key']
                del kwa['_key']
            else:
                api_keyname = 'default'
            Entity.bs = bs = backing_store.BackingStore(bs_mode, _key = api_keyname, _aux_only=self._auxOnly)
        else:
            bs = Entity.bs
        self._bs = bs
        if (etype not in Entity.keyCache):
            if self._auxOnly:
                self._aux_schema(etype)
            else:
                self._load_schema(etype)
        else:
            self._raw_fields = Entity.keyCache[etype]
        fields = kwa.get('_fields', self._default)
        if '_fields' in kwa: del kwa['_fields']
        if '_all' in fields:
            fields = self._raw_fields
        if '_debug' in kwa:
            self._debug = kwa['_debug']
            del kwa['_debug']
        if '_timing' in kwa:
            self._timing = kwa['_timing']
            del kwa['_timing']
        if '_time_all' in kwa:
            Entity.time_all = kwa['_time_all']
            del kwa['_time_all']
        for k in [ 'id', 'type' ]:      # Always want these, but some are illegal as dict() keys
            sk = '_' + k
            self._map[sk] = k
            # if (k in fields) and (not sk in self._map): self._map[sk] = k
        self.include(**{'id':None,  'name':None}) # Null case values
        self.include(**kwa)
        fields = self._remap_fields(fields)
        self._nameSelector = 'code' # default case
        for ns in [ 'code', 'name', 'sg_code', 'sg_name' ]:
            if ns in self._raw_fields:
                self._nameSelector = ns
                break
        ns = self._nameSelector
        if _id:
            if not self._auxOnly:
                dct = bs.read(etype, _id, fields)
                if dct: self._data = dct
        elif  _code:
            filters = [[ns, 'is', _code]]
            for k, v in kwa.iteritems():
                if k.startswith('_'): continue
                if k in self._magic:
                    tgt = self._map[k]
                    v = self._magic[k](v)
                    k = tgt
                if k in self._map: k = self._map[k]
                filters.append([k, 'is', v])
            if not self._auxOnly: self._data = bs.find_one(etype, filters, fields)
            else:
                self._data = dict()
                self._auxData = self.bs.auxProd.find_one({ 'name':_code, 'type':self._etype})
                print "auxData", self.auxData() ; return
            if not self._data: self._data = dict()
        else:
            self._data = dict()
        if self._debug:
            print "%s default: %s fields: %s" % ( self._etype,  " ".join(self._default),  " ".join(fields))
        return                      # Want a null object

    def get(self,  k, default):
        if k in self: return self[k]
        # if k in self.auxFields: return self(k) # Disabled at Method because we aren't using this yet;
        return default

    def _get(self, k):
        tgt = self._map.get(k, k) # Get remap of k if remapped, else continue with source value
        if tgt == 'type' and 'type' not in self: self['type'] = self._etype # HACK: if we tentativeLink or hexId a new object, we hit this case;
#        if (tgt not in self._data) and (tgt not in self._magic):
#            print "%s not in data or magic for %s" % ( k,  self._etype )
#            return None
        data = self._data[tgt]
        if k in self._magic:
            data = self._magic[k](data, True)
        return data

    def __getitem__(self, k):
        "Dereference if k in _map, and transform data (inversely) if k in _magic"
        return self._get(k)

    def __setitem__(self,  k,  v):
        tgt = k
        if k in self._map: tgt = self._map[k]
        if k in self._magic: v = self._magic[k](v)
        self._data[tgt] = v
        if not tgt.startswith('_'):
            self._changed.add(tgt)          # Always note the underlying name

    def _set(self, k, v):
        "Like setitem, but without updating _changed"
        tgt = self._map.get(k, k)
        if k in self._magic: v = self._magic[k](v) # Self._magic always referred to by friendly name
        self._data[tgt] = v

    def _set_aux(self, k, v):
        "Like setitem, but without updating _changed"
        # print "_set_aux", k, v, id(self)
        self._auxData[k] = v
        # print "_set_aux", self._auxData

    def __delitem__(self, k):
        if k in self._magic: raise IndexError("Cannot delete magic methods (%s)" % k)
        tgt = self._map.get(k, k)
        if tgt not in self._data: raise IndexError("Cannot delete unpopulated index %s" % k)
        del self._data[tgt]

    def __contains__(self,  k):
        "Performs friendly name remapping before 'in' operation"
        if self._auxOnly:
            return k in self._auxData
        if k in self._map: k = self._map[k]
        return k in self._data

    def __len__(self):
        if not self._data: return 0
        return len(self._data)

    def __repr__(self):
        # TODO: need to support any meta fields?
        if self._auxOnly:
            return repr(self._auxData)
        return repr(self._data)

    def __str__(self):
        "Return group sorted Entity(k=v, k=v) string"
        def kvs(k):
            if k in self: return "%s=%s" % ( k, self[k] )
            return None
        if self._auxOnly:
            return self._auxStr()
        keys = [ 'id', 'name', 'project', 'parent' ]
        fkeys = self._map.keys()
        fkeys.sort()
        keys += [k for k in fkeys if k not in keys]
        rkeys = self._reverse.keys()
        rkeys.sort()
        rkeys += keys
        keys += [k for k in self._raw_fields if k not in rkeys]
        keys = filter(lambda k: k.islower() and (not k.startswith('_')), keys)
        pairs = map(kvs, keys)
        pairs = filter(None, pairs)
        pairs = ", ".join(pairs)
        if pairs: pairs = "[%s]" % pairs
        auxpairs = ""
        if self.auxFields:
            keys = [k for k in self.auxFields if not k.startswith('_')]
            keys.sort()
            auxpairs = ",".join(["%s:%s" % (k, self(k)) for k in keys])
            auxpairs = "(%s)" % auxpairs
        return "%s(%s%s)" % ( self._ename, pairs, auxpairs)

    def _auxStr(self):
        keys = self._auxData.keys()
        keys.sort()
        ostr = ", ".join(["%s=%s" % (k, self._auxData[k]) for k in keys])
        return "%s(%s)" % ( self._ename, ostr )

    def str(self,  **kwa):
        def kvs(k):
            if k in self: return "%s=%s" % ( k, self[k] )
            return None
        keys = [ 'id', 'name', 'project', 'parent' ]
        fkeys = self._map.keys()
        fkeys.sort()
        keys += [k for k in fkeys if k not in keys]
        exclude = kwa.get('exclude', [])
        for x in exclude:
            if x in keys: keys.remove(x)
        rkeys = self._reverse.keys()
        rkeys.sort()
        rkeys += keys
        keys += [k for k in self._raw_fields if k not in rkeys]
        keys = filter(lambda k: k.islower() and (not k.startswith('_')), keys)
        pairs = map(kvs, keys)
        pairs = filter(None, pairs)
        pairs = ", ".join(pairs)
        return "%s(%s)" % ( self._ename, pairs)

    def __hash__(self): return id(self)
    def __call__(self, *args):
        """entity(identifier) -- return all objs that contain identifier
           entity(identifier, key) -- return value for key in first doc matching identifier
           entity(identifier, key, value) -- set doc[key] = value in first doc matching """
        self._start_timing()
        if len(args) == 1:
            k = args[0]
            if k in self._auxData:
                return self._auxData[k]
        bs = Entity.bs
        identifier = dict()
        identifier['type'] = self._ename
        if self._auxOnly:
            if '_id' in self._auxData:  identifier['_id'] = self._auxData['_id']
            elif 'id' in self._auxData:  identifier['_id'] = self._auxData['id']

            if 'name' in self._auxData: identifier['name']  = self._auxData['name']
            elif 'code' in self._auxData: identifier['code'] = self._auxData['code']

        else:
            if 'id' in self: identifier['sg_id'] = self['id']

            if 'name' in self:   identifier['name']  = self['name']
            elif 'code' in self: identifier['code'] = self['code']

        if False:
            doc = bs.auxProd.find_one(identifier)
            if not doc:
                bs.auxProd.save(identifier)

        doc = bs(identifier, *args)
        if len(args) == 2:
            k, v = args[0], args[1]
            self._auxData[k] = v
        self._end_timing()
        return doc

    def __iter__(self):
        return self.next()

    def next(self):
        if not self._iterlist:
            self._iterlist = self.keys()
        for item in self._iterlist:
            yield item

    def delimited_view(self, key_delim, pair_delim, object_delim):
        """Provide a specialized view of the objects contents:
        <object_delim>ObjName<pair_delim>key<key_delim>value<pair_delim>...<object_delim>
        """
        def start(delim):
            if len(delim) < 2: return delim
            return delim[0]
        def end(delim):
            if len(delim) < 2: return delim
            return delim[-1]

        special_keys = [ 'id', 'name', 'project', 'parent' ]
        ostr = object_delim + self._ename
        for k in special_keys:
            if k in self:
                ostr += pair_delim + k + key_delim + str(self[k]) + pair_delim
        for k in self.keys():
            if k in special_keys: continue
            if not k.islower(): continue
            if k.startswith('_'): continue
            ostr += pair_delim + k + key_delim + str(self[k]) + pair_delim
        ostr += object_delim
        return ostr

    def auxData(self, k = None, v = None):
        if (not k) and (not v):
            return self._auxData
        if k and (not v):
            return self._auxData[k]
        self._auxData[k] = v
        return self._auxData[k]

    def ekeys(self):
        mapped = set(self._map.keys())
        native = set(Entity.keyCache[self._etype])
        l = list(mapped | native)
        l.sort()
        return l

    def keys(self):
        return self._data.keys() # FIXME: was: self._friendify(self._data.keys())

    def perma_keys(self):
        keys = self._raw_fields[:] # self.keys()
        # keys = filter(lambda s: not s.startswith('_') and s.islower(), keys)
        return keys

    def help(self):
        obuf = [ "%s entity encapsulates %s Shotgun Entity" % ( self._ename, self._etype ) ]
        obuf.append("")
        friendly = self.keys()
        friendly.sort()
        obuf.append("Keys: %s" % " ".join(friendly) )
        mm = self._magic.keys()
        mm.sort()
        obuf.append("Magic Methods: %s" % " ".join(mm) )
        obuf.append("")
        raw = self._raw_fields
        raw.sort()
        obuf.append("Schema Keys: %s" % " ".join(raw))
        obuf.append("")
        print "\n".join(obuf)

    def _load_schema(self, etype):
        schema = Entity.bs.schema(etype)
        keys = schema.keys()
        keys.sort()
        self._raw_fields = Entity.keyCache[etype] = keys
        return keys

    def _aux_schema(self, etype):
        schema = dict()
        self._raw_fields = Entity.keyCache[etype] = [ ]
        return []

    def field_name(self, column_name):
        "Translate from column name to field name"
        field_name = 'sg_' + column_name.lower()
        for c in " .+-&*\\/": field_name = field_name.replace(c, '_')
        return field_name

    def _friend_names(self,  lst):
        rl = [ ]
        for k in lst:
            if k in self._reverse:
                rl.append(self._reverse[k])
            else:
                rl.append(k)
        return rl

    def _friendify(self,  dct):
        "Take a dictionary and modify its keys to be friendly name if available"
        for k in dct:
            if k in self._reverse:
                dct[self._reverse[k]] = dct[k]
                del dct[k]

    def _update_keys(self,  dctOrLst):
        if type(dctOrLst) == types.DictType:
            self._friendify(dctOrLst)
        elif type(dctOrLst) == types.ListType:
            for dct in dctOrLst:
                self._friendify(dct)

    def _true_name(self,  k):
        if k in self._map: k = self._map[k]
        return k

    def include(self, **dct):
        """This method uses internal setitem to dereference things in map,
        or data transforms from _magic"""
        for k in dct:
            if self._auxOnly:
                self._set_aux(k, dct[k])
                return self
            self._set(k, dct[k])
            self._changed.add(k)
        return self

    def _reverse_dict(self, dct):
        odict = dict()
        for k in dct: odict[dct[k]] = k
        return odict

    def _remap_fields(self,  fields):
        rlst = [ ]
        for k in fields:
            if k in self._map:
                # print "_remap_fields: %s->%s" % ( k,  self._map[k] )
                k = self._map[k]
            rlst.append(k)
        return rlst

    def _remap_filters(self,  src_filters):
        if self._auxOnly: return src_filters # Handled deeper down elsewhere (I hope)
        filters = [ ]
        for _filter in src_filters:
            k = _filter[0]
            if k in self._magic:
                continue
            if k in self._map:
                _filter = [ self._map[k], _filter[1], _filter[2] ]
            filters.append(_filter)
        return filters

    def _clean(self, dct):
        rems = [ ]
        for k in dct:
            if k.startswith('_'): rems.append(k)
            if not k.islower(): rems.append(k)
        for k in rems:
            del dct[k]

    def parse_keylist(self, arglist):
        """Parse ['a:b', 'c:d', ...] into current values, autoconvert ints, allow for int as string (single quote)"""
        for arg in arglist:
            k, v = arg.split(':', 1)
            if v.isdigit(): v = int(v)
            elif v.startswith("'") and v.endswith("'"): v = v[1:-1]
            self[k] = v

    # Magic Methods -- single value, (getitem=True is getitem form, getitem=False is setitem form)
    def _projectId(self, _id, getitem=False):
        if getitem:
            if 'id' not in self._data['project']: return None
            return self._data['project']['id']
        rdct = { 'type':'Project',  'id':_id }
        self._data['project'] = rdct
        return rdct

    def _projectName(self, code, getitem=False):
        if getitem: return self._data['project']['name']
        self._data['project'] = rdct = { 'type':'Project', 'name':code }
        return rdct

    def _projectCode(self, code, getitem=False):
        if getitem:
            if self._nameSelector not in self._data['project']: return None
            return self._data['project'][self._nameSelector]
        self._data['project'] = rdct = { 'type':'Project', 'code':code }
        return rdct

    def _typeLink(self, dummy, getitem=False):
        if getitem:
            return { 'type':self._etype, 'id':self['id'] }
            dct = { 'type':self._etype,  'id':self['id'] }
            if 'name' in self: dct[self._true_name('name')] = self['name']
            return dct
        raise ValueError("Can't assign to typeLink")

    def _parentId(self, _id, _etype, getitem=False):
        if getitem: return self._data['parent']['id']
        rdct = { 'type':_etype,  'id':_id }
        return rdct

    def _parentName(self, nm, getitem=False):
        if getitem: return self['parent']['name']
        rdct = { 'type':self['parent']['type'], self._nameSelector:nm }
        return rdct

    def _pathFP(self, fp, getitem=False):
        if getitem: return self._data['path']['local_path']
        rdct = { 'name':os.path.basename(fp), 'link_type':'local', 'local_path':fp }
        return rdct

    def _eDictRef(self, dummy = None, getitem=False):
        if getitem:
            return self._eDict
        return False

    def _hexId(self, dummy, getitem=False):
        if getitem: return "0x%x" % id(self)
        raise ValueError("Can't assign to hexId")

    def _tentativeLink(self, dummy, getitem=False):
        if getitem:
            if 'id' in self: return self['typeLink']
            return self['hexId']
            # return { 'type':self._etype, 'code':self['hexId'] }
        raise ValueError("Can't assign to tentativeLink")

    def all(self, _fields=None):
        if not _fields: _fields = self._default
        return self.match(_fields=_fields)

    def tasklist(self, parent='parent', **kwa):
        from task import Task
        _fields = kwa.get('_fields', list(set(self._default) | set(['status_list', 'step', 'updated_by', 'parent',
                                                                    'name', 'task_assignees', 'template_task',
                                                                    'upstream_tasks', 'downstream_tasks' ])))
        # tasklist = [Task(_id) for curr_task in tasks]
        kwa = { parent:self['typeLink'], '_fields':_fields }
        tasklist = Task().match(**kwa)
        return tasklist

    def create(self, dct = None):
        self._start_timing()
        if self._auxOnly:
            # print "Creating _auxOnly", self._auxData
            dct = self._auxData.copy()
            if 'type' not in dct: dct['type'] = self._etype
            if ('name' not in dct) and ('code' not in dct):
                code = self._data.get('name', self._data.get('code', None))
                if code: dct['code'] = code
            if '_id' in dct:
                del dct['_id']
            rv = self.bs.auxProd.insert(dct)
            self._end_timing()
            self._auxData['id'] = rv
            return rv
        # self._update_raw()
        if not dct:
            dct = self._data.copy()
        self._clean(dct)
        rv = Entity.bs.create(self._etype, dct)
        if rv: self._changed = set()
        if 'id' in rv:
            self['id'] = rv['id']
        self._end_timing()
        return rv

    def field_create(self, display_name, properties_dict={}, field_type="text"):
        "field_create(Column Name, {'description':'New field desc'}, 'text')"
        self._start_timing()
        rv = Entity.bs.field_create(self._etype, field_type, display_name, properties_dict)
        if rv:
            self._load_schema(self._etype)  # Update schema and self._raw_fields
        self._end_timing()
        return rv

    def field_update(self, field_name, properties_dict):
        self._start_timing()
        rv = Entity.bs.field_update(self._etype, field_name, properties_dict)
        if rv:
            self._load_schema(self._etype)  # Update schema and self._raw_fields
        self._end_timing()
        return rv

    def field_delete(self, field_name):
        self._start_timing()
        rv = Entity.bs.field_delete(self._etype, field_name)
        if rv:
            self._load_schema(self._etype)  # Update schema and self._raw_fields
        self._end_timing()
        return rv

    def read(self, _id=None, _code=None):
        self._start_timing()
        self._raw = Entity.bs.read(self._etype, _id, _code)
        self._update_map()
        self._changed = set()
        self._end_timing()

    def _fields(self, **kwa):
        if self._auxOnly:
            fields = []
        else:
            fields = self._default
        if '_fields' in kwa:
            fields = kwa['_fields']
            if '_all' in fields: fields = self._raw_fields
            del kwa['_fields']
        return fields

    def find(self, filters, fields = None, **kwa):
        "find returns a list of dictionaries of matches for the current etype limit, retired, order, combine all in kwa"
        self._start_timing()
        limit = kwa.get('_limit',  0)
        order = kwa.get('_order',  None)
        filter_operator = kwa.get('_combine',  None)
        retired = kwa.get('_retired', None)
        page = kwa.get('_page', 0)
        # list find(string filter_operator, int limit, boolean retired_only, int page)
        if not fields: fields = self._default
        fields = self._remap_fields(fields)
        filters = self._remap_filters(filters)
        dct_list = Entity.bs.find(self._etype,  filters,  fields, order=order,
                                  filter_operator=filter_operator, limit=limit,
                                  retired=retired, page=page)
        self._end_timing()
        self._iterlist = dct_list
        return dct_list # For now, someone down the line needs to convert these to entity objects

    def find_sg(self, filters, fields, limit=0, order=None, retired=None, page=0, filter_operator=None):
        self._start_timing()
        dct_list = Entity.bs.find(self._etype, filters, fields, order=order, limit=limit, retired=retired,
                                     page=page)
        self._end_timing()
        self._iterlist = dct_list
        return dct_list

    def find_aux(self, filters, fields = None, **kwa):
        "find returns a list of dictionaries of matches for the current etype limit, retired, order, combine all in kwa"
        self._start_timing()
        limit = kwa.get('_limit',  0)
        order = kwa.get('_order',  None)
        filter_operator = kwa.get('_combine',  None)
        retired = kwa.get('_retired', None)
        page = kwa.get('_page', 0)
        # list find(string filter_operator, int limit, boolean retired_only, int page)
        if not fields: fields = []
        fields = self._remap_fields(fields)
        filters = self._remap_filters(filters)
        dct_list = Entity.bs.find_aux(self._etype,  filters,  fields, order=order,
                                      filter_operator=filter_operator, limit=limit,
                                      retired=retired, page=page)
        self._end_timing()
        self._iterlist = dct_list
        # print "find_aux rl", dct_list
        return dct_list # For now, someone down the line needs to convert these to entity objects

    def match(self, **kwa):
        "match returns a list of objects that matches the params in kwa for the current etype"
        if self._auxOnly:
            rl = self.match_aux(**kwa)
            return rl
        self._start_timing()
        comparators = { "!=":'is_not', "<":'less_than', ">":'greater_than' }
        fields = self._fields(**kwa)
        if not kwa:
            filters = [[ 'id', 'is_not', None ]]
        else:
            filters = [ ]
            for k in kwa:
                orig_k = dest_k = k
                if (k not in [ '_id', '_type' ]) and k.startswith('_'): continue
                tv = type(kwa[orig_k])
                if k in [ '_id', '_type' ]: k = k[1:]
                if k in self._map:          dest_k = self._map[k]
                if tv == types.TupleType:
                    # if k in self._magic: target = self._magic[k](kwa[k])
                    comparator, target = kwa[orig_k]
                    if comparator in comparators:
                        comparator = comparators[comparator]
                    if type(target) == types.ListType:
                        filters.append([dest_k, comparator] + target)
                    else:
                        filters.append([dest_k, comparator, target])
                    continue
                if tv in [ types.StringType, types.UnicodeType, types.DictType, types.BooleanType,
                           types.NoneType, types.IntType, types.FloatType ]:
                    target = kwa[orig_k]
                    if orig_k in self._magic:
                        target = self._magic[orig_k](kwa[orig_k])
                    if orig_k in self._map:
                        k = self._map[orig_k]
                    filters.append([k, 'is', target])
                    continue
                raise TypeError("Unsupported value type in match: %s:%s" % ( tv, kwa ))
        start = self.start
        dct_list = self.find(filters, fields, **kwa) # TODO: Add **kwa? 
        self.start = start
        if not dct_list: return [ ]
        rlst = [ ]
        for dct in dct_list:
            e = self._class()           # Get a null instance
            for k in dct: e._set(k, dct[k])
            rlst.append(e)
        self._end_timing()
        self._iterlist = rlst
        return rlst

    def match_regex(self, key, pat_or_rex, **kwa):
        result = [ ]
        _fields = self._fields(**kwa)
        if key not in _fields:
            _fields[key] = ('is_not', None)
        if type(pat_or_rex) == types.StringType:
            pat = pat_or_rex
            flags = None
            if '(?P<' in pat:
                flags |= re.VERBOSE
            rex = re.compile(pat, flags)
        elif str(type(pat_or_rex)) == "<type '_scr.SRC_Pattern'>":
            rex = pat_or_rex
        else:
            raise TypeError("Invalid type for pattern/rex: %s" % type(pat_or_rex))
        ents = []
        raise NotYetImplemented("match_regex not yet implemented")
        dct_list = self.find()

    def match_aux(self, **kwa):
        "match returns a list of objects that matches the params in kwa for the current etype"
        self._start_timing()
        comparators = { "!=":'$ne', "<":'$lt',">=":'$lte', "=<":'$lte',
                        ">":'$gt', ">=":'$gte', "=>":'$gte', "contains":'$in',
                        "exists":"$exists", "?":"$exists" }
        fields = self._fields(**kwa)
        filters = []
        if kwa:
            for k in kwa:
                orig_k = dest_k = k
                if (k not in [ '_id', '_type' ]) and k.startswith('_'): continue
                tv = type(kwa[orig_k])
                if k in [ '_type' ]: k = k[1:]
                if tv == types.TupleType:
                    comparator, target = kwa[orig_k]
                    if comparator in comparators:
                        comparator = comparators[comparator]
                    if comparator in [ '$in' ]: target = [ target ]
                    filters.append({dest_k:{comparator:target}})
                    continue
                if tv in [ types.StringType, types.UnicodeType, types.DictType, types.BooleanType,
                           types.NoneType, types.IntType, types.FloatType ]:
                    target = kwa[orig_k]
                    filters.append({k:target})
                    continue
                # backing store _id selection
                if tv in [ bson.objectid.ObjectId ]:
                    target = kwa[orig_k]
                    filters.append({k:target})
                    continue
                raise TypeError("Unsupported value type in match: %s:%s" % ( tv, kwa ))
        start = self.start
        dct_list = self.find_aux(filters, fields, **kwa) # TODO: Add **kwa?
        # print "match_aux dct_list", dct_list
        if not dct_list: return [ ]
        rlst = [ ]
        for dct in dct_list:
            e = self._class()           # Get a null instance
            for k in dct:
                e._set_aux(k, dct[k])
            rlst.append(e)
        self._iterlist = rlst[:]
        self._end_timing()
        return rlst

    def copy(self):
        return copy.copy(self)

    def min_copy(self):
        e = copy.copy(self)
        keys = self._data.keys()
        for k in keys:
            if k == 'id': continue # Manually copy the ID if you really need it;
            if k in self._read_only:
                del e._data[k]
            elif e._data[k] == None:
                del e._e[k]
            # elif e._data[k] == []: del e._data[k]
        return e

    def update(self):
        self._start_timing()
        if not self._changed:
            self._end_timing()
            return {}
        if self._auxOnly:
            if '_id' not in self._auxData:
                self.end_timing()
                return {}
            which = dict(_id=self._auxData['_id'])
            updates = dict().update(self._auxData)
            query = { '$set': updates }
            doc = self.bs.auxProd.update(which, query)
        dct = { 'id':self._data['id']}
        for k in self._changed:
            if k == 'id': continue
            tgt = k
            if k in self._map: tgt = self._map[k] # need magic support?
            if k not in self: continue # HACK: Why is 'name' in note._changed? 
            dct[tgt] = self[k] # self._data[k]
        rdct = Entity.bs.update(self._etype, dct)
        if rdct:
            self._changed = set()
        else:
            pass
        self._end_timing()
        return rdct

    def delete(self):
        self._start_timing()
        if self._auxOnly:
            rv = Entity.bs.aux_delete(self._etype, self._auxData['_id']) # FIXME: Too Mongo-Centric?
        else:
            rv = Entity.bs.delete(self._etype, self._data['id'])
        self._end_timing()
        return rv

    def set_session_uuid(self, uuid):
        self._start_timing()
        rc = self._bs.set_session_uuid(uuid)
        self._end_timing()
        return rc
    

    def batch(self, lst):
        self._start_timing()
        Entity.bs.batch(lst)
        self._end_timing()

    def append(self,  v):
        if (v in self._native) or (v in self._map): self._default.append(v)

    def remove(self, k):
        "remove a keyed item from this entity"
        if (not k.islower()):
            raise KeyError("Invalid key %s can't be removed")
        tgt = k
        if k in self._map: tgt = self._map[k]
        if tgt in self._data:
            del self._data[tgt]
            return
        raise KeyError("Invalid key %s not found" % k)

    def remove_default(self, v):
        if v in self._default:
            self._default.remove(v)

    def extend(self, *fields):
        self._start_timing()
        update = Entity.bs.read(self._etype, self._data['id'], self._remap_fields(fields))
        self._data.update(update)
        self._end_timing()

    def clear(self):  self._default = [ ]

    def _start_timing(self):
        if self._timing or Entity.time_all:
            self.start = time.time()

    def _end_timing(self):
        if self._timing or Entity.time_all:
            self.end = time.time()
            self.elapsed = self.end - self.start

    @property
    def auxFields(self):
        docs = self()
        for doc in docs:
            return doc.keys()

    @property
    def base_data(self): return self._data.copy()

    @property
    def changed(self): return len(self._changed) != 0

    if False:
        @changed.setter
        def changed(self, v):
            if v: self._changed.add('type')
            else: self._changed = set()

    @property
    def data(self):
        "Return a safe copy of the data dictionary of the entity, remapping to friendly names as keys;"
        rdict = self._data.copy()
        for k in rdict:
            if k in self._reverse:
                rdict[self._reverse[k]] = rdict[k]
                del rdict[k]
        return rdict

    @property
    def default(self): return self._default

    @property
    def fields(self): return self._friend_names(self._default)

    @property
    def io_error(self): return Entity.bs.io_error

    @property
    def link(self):
        dct = self['typeLink']
        ns = self._nameSelector
        if ns in self:
            dct[ns] = self[ns]
        return dct

    @property
    def rawData(self):
        return self._data.copy()

    @property
    def rawFields(self):
        if not self._raw_fields:
            self._load_schema(self._etype)
        return self._raw_fields[:]

    @property
    def schema(self): return self._bs.schema(self._etype)

    @property
    def valid_statuses(self):
        schema = self.schema
        valid_status_values = schema['sg_status_list']['properties']['valid_values']['values']
        statuses = self.find(code=('in', valid_status_values),
                             _fields=[ 'code', 'name', 'icon.Icon.image_map_key' ])
        return statuses

    @property
    def server_info(self):
        bs = self._bs
        db = bs._db
        dct = dict(url = db.base_url, api_ver = db.api_ver, proxy = db.http_proxy, page_size = db.records_per_page,
                   sid = db.sid, server = db.server, bstype=bs._bstype, )
        return dct

    @property
    def signature(self):
        si = self.server_info
        gmt = time.gmtime(time.time())
        fmt = "%d_%02d_%02d_%02d_%02d_%02dZ"
        when = fmt % ( gmt.tm_year, gmt.tm_mon, gmt.tm_mday, gmt.tm_hour, gmt.tm_min, gmt.tm_sec )
        _id = self.get('id', self['hexId'])
        show_id = str(self.get('project', '?'))
        s = "%s:%s:%s:%d:%s" % ( si['server'], show_id, self._etype, _id, si['sid'] )
        return s

    @property
    def now(self):
        gmt = time.gmtime(time.time())
        fmt = "%04d-%02d-%02dT%02d:%02d:%02d-%02d:00"
        z = time.altzone / 3600
        when = fmt % ( gmt.tm_year, gmt.tm_mon, gmt.tm_mday, gmt.tm_hour, gmt.tm_min, gmt.tm_sec, z )
        return when

    @property
    def quick_id(self): return "%(_type)s(%(id)d) %(name)s" % self

    @property
    def update_ready(self):
        def type_ref(dct):
            if type(dct) != types.DictType: return True
            if 'type' not in dct: return True
            if 'id' not in dct: return False
            return True
        if not self.changed: return False
        if not 'id' in self: return False
        if not 'type' in self: return False
        for k in self.keys():
            if type(self[k]) == types.DictType:
                if not type_ref(dct): return False
            elif type(self[k]) == types.ListType:
                for i in type(self[k]):
                    if type_ref(i): continue
                    else: return False
        return True

if __name__ == "__main__":
    # Inline tests
    _type = "Project"
    _map = { 'nickname':'code',  'description':'sg_description',
             'due':'sg_due', 'type':'sg_type',  'status':'sg_status', }
    _magicmap = dict()
    _id = 1058
    _code = "vfx_catbird"
    ent_by_code = Entity(_type, _map, _magicmap, None, _code)
    ent_by_id = Entity(_type, _map, _magicmap, _id, None)
    assert ent_by_code['nickname'] == _code,  "Wrong ent_by_code[name]"
    assert ent_by_id['nickname'] == _code,  "Wrong ent_by_id[name]"
    assert ent_by_code['id'] == _id, "Wrong ent_by_code[id]"
    assert ent_by_id['id'] == _id,  "Wrong ent_by_id[id]"
    print "Project %s verified" % ent_by_id['nickname']
