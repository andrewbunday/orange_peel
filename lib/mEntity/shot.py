
"Simple Shot entity"

if True:
    import m_entity

class Shot(m_entity.mEntity):
    DEFAULT = [ 'id', 'code', 'project' ]
    MAP = dict(element_tracker='sg_custom_note', external_id='sg_external_id', name='code',
	       sequence='sg_sequence',
	       final_version='sg_final_version_link', slate='sg_slate_link')
    INTERNAL = "Shot"
    READ_ONLY = [ 'created_at', 'created_by', 'updated_at', 'updated_by' ]
    def __init__(self,  _id = None,  _code = None, **kwa):
        "Setup translations for calling "
        _type = Shot.INTERNAL      # What shotgun calls it, ie: Task, CustomEntity10, etc.
        _map = Shot.MAP
        _magicmap = dict()              # map of auto-translating functions
        # No pre super
        self._default = Shot.DEFAULT[:] # default fields to load if none specified
        super(Shot,  self).__init__(_type,  _map,  _magicmap,  _id,  _code, **kwa) # Et Voila!
        # No post super
        self._class = Shot
        self._read_only = Shot.READ_ONLY

if __name__ == "__main__":
    import sys, pprint
    def test_cases():
        # shot = Shot(None, 'foo') # This should blow up, no such version name
        # id_shot = Shot(357) # This should blow up unless there is a version 357 somewhere
        shots = Shot().match(projectId=96) # Get all default shot fields for this project
        for shot in shots:
            print str(shot)
    args = sys.argv[:]
    pname = args.pop(0)
    if len(args) < 1:
        test_cases()
        sys.exit(0)
    _id = None
    if args[0] == '-m':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Shot(_id)
        entity.parse_keylist(args)
        if _id: rc = entity.update()
        else: rc = entity.create()
        pprint.pprint(rc)
    elif args[0] == '-i':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Shot(_id, _fields=['_all'])
        print str(entity)
    sys.exit(0)

