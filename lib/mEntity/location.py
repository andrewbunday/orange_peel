
"Simple Location entity"

if True:
    import m_entity

class Location(m_entity.mEntity):
    DEFAULT = [ 'id', 'code', 'nickname' ]
    MAP = dict(name='code', nickname='sg_nickname', created='created_at') # friendly name map, either can be used
    INTERNAL = "CustomNonProjectEntity04"
    READ_ONLY = [ 'created_at', 'created_by', 'updated_at', 'updated_by' ]
    def __init__(self,  _id = None,  _code = None, **kwa):
        "Setup translations for calling "
        _type = Location.INTERNAL      # What shotgun calls it, ie: Task, CustomEntity10, etc.
        _map = Location.MAP
        _magicmap = dict()              # map of auto-translating functions
        # No pre super
        self._default = Location.DEFAULT[:] # default fields to load if none specified
        super(Location,  self).__init__(_type,  _map,  _magicmap,  _id,  _code, **kwa) # Et Voila!
        self._ename = 'Location'
        self._class = Location
        self._read_only = Location.READ_ONLY

if __name__ == "__main__":
    import sys, pprint
    def test_cases():
        # customnonprojectentity04 = Location(None, 'foo') # This should blow up, no such version name
        # id_location = Location(357) # This should blow up unless there is a version 357 somewhere
        locations = Location().match(projectId=96) # Get all default location fields for this project
        for location in locations:
            print str(location)
    args = sys.argv[:]
    pname = args.pop(0)
    if len(args) < 1:
        test_cases()
        sys.exit(0)
    _id = None
    if args[0] == '-m':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Location(_id)
        entity.parse_keylist(args)
        if _id: rc = entity.update()
        else: rc = entity.create()
        pprint.pprint(rc)
    elif args[0] == '-i':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Location(_id, _fields=['_all'])
        print str(entity)
    sys.exit(0)

