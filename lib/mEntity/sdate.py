"SDate -- Shotgun Compatible Date Arithmetic"

# Copyright (c) 2013 Wook

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

if True:
    import calendar
    import datetime
    import time
    import types

class SDate(object):
    "Date/time Unifier with helpers for our Shotgun"
    S_SUFFIX = 'T00:00:00-07:00'
    E_SUFFIX = 'T23:59:59-07:00'
    FORMAT = "%Y-%m-%d"
    DTFORMAT = "%Y-%m-%d %H:%M:%S"
    ALTFORMAT = "%y-%m-%d"      # Year that doesn't start with century info
    ALTAMERICAN = "%y-%d-%m"    # How it's done in the U.S.
    def __init__(self, polytime = None):
        self._datetime = self._time = self._string = False
        self._time = time.localtime()
        self._gmt = time.gmtime()
        tp = type(polytime)
        if polytime == None:
            self.year, self.month, self.day = self._time[0:3] # extract the key fields
            self._string = time.strftime(SDate.FORMAT, self._time) # stringify
            self._datetime = datetime.date(self.year, self.month, self.day) # and use datetime.date for calcs
        elif tp in [ types.FloatType, types.IntType ]:
            self.year, self.month, self.day = self._time[0:3]
            self._string = time.strftime(SDate.FORMAT, self._time)
            self._datetime = datetime.date(self.year, self.month, self.day)
        elif isinstance(polytime, datetime.date):
            self._datetime = polytime
            self.year, self.month, self.day = polytime.year, polytime.month, polytime.day
            self._time = (self.year, self.month, self.day, 0, 0, 0, 0, 0, 0 )
            self._string = time.strftime(SDate.FORMAT, self._time)
        elif isinstance(polytime, time.struct_time):
            self._time = polytime
            # TODO: Compute alternate GMT based on this date, and repeat below
            self._gmt = self.gmtify(self._time)
            self.year, self.month, self.day = polytime[0:3]
            self._datetime = datetime.date(self.year, self.month, self.day)
            self._string = time.strftime(SDate.FORMAT, self._time)
        elif type(polytime) in [ types.TupleType, types.ListType ]:
            self.year, self.month, self.day = polytime[0:3]
            self._datetime = datetime.date(self.year, self.month, self.day)
            self._string = time.strftime(SDate.FORMAT, (self.year, self.month, self.day, 0, 0, 0, 0, 0, 0 ))
            self._time = ( self.year, self.month, self.day, 0, 0, 0, 0, 0, 0, )
            self._gmt = self.gmtify(self._time)
        elif type(polytime) == types.StringType:
            fmt = SDate.FORMAT
            if not polytime.startswith('201'):
                fmt = SDate.ALTAMERICAN
            if '/' in polytime:
                polytime = polytime.replace('/', '-', 2)
            self._time = time.strptime(polytime, fmt)
            self._gmt = self.gmtify(self._time)
            self.year, self.month, self.day = self._time[0:3]
            self._string = "%d-%02d-%02d" % ( self.year,  self.month, self.day )
            self._datetime = datetime.date(self.year, self.month, self.day)
        self.dow = calendar.weekday(self.year, self.month, self.day)
        self.monday = self._monday()
        self._fix()
        self.week_num = int(time.strftime("%U", self._time))
        return

    def __cmp__(self, other): return cmp(self._datetime, other._datetime)
    def __str__(self): return self._string

    def  __sub__(self, other):
        minuend = self
        subtrahend = other
        if self.year != other.year:      # Span across year boundary?
            end_year = other.eoyear      # Need Julian for last day of starting year
            eoyj = end_year.julian
            last_year_days = end_year.yjulian - other.yjulian
            return last_year_days + self.julian
        return self.yjulian - other.yjulian

    def _monday(self):
        if self.dow == 0: return self
        secs = time.mktime(self._time)
        secs -= 86400 * self.dow
        st_monday = time.ctime(secs)
        t = time.strptime(st_monday)
        return SDate(t)

    def _fix(self):
        "This is actually necessary so we always have julian date available"
        if self._time[3:8] != ( 0, 0, 0, 0, 0 ): return
        fmt = SDate.FORMAT
        now = time.localtime()
        _time = list(self._time)
        _time[-1] = now[-1]
        _time = tuple(_time)
        self._time = time.strptime(time.strftime(fmt, _time), fmt)
        return

    def gmtify(self, lt_rep):
        now = time.gmtime()
        gmt = time.struct_time((lt_rep.tm_year, lt_rep.tm_mon, lt_rep.tm_mday,
                                now.tm_hour, now.tm_min, now.tm_sec,
                                lt_rep.tm_wday, lt_rep.tm_yday, lt_rep.tm_isdst))
        return gmt

    @property
    def now(self):
        s = time.strftime(SDate.DTFORMAT, self._time) # stringify
        return s

    @property
    def gmt(self):
        return self._gmt

    @property
    def start_date_time(self):
        return self._string + SDate.S_SUFFIX

    @property
    def end_date_time(self):
        return self._string + SDate.E_SUFFIX

    @property
    def time(self):
        return self._time

    @property
    def datetime(self):
        return self._datetime

    @property
    def yjulian(self):
        return (self.year  * 10000) + self.julian

    @property
    def julian(self):
        return self._time[-2]

    @property
    def eoyear(self):
        return SDate((self.year, 12, 31, ))

    @property
    def seconds_since_epoch(self):
        return time.mktime(self._time)

    @property
    def md(self):
        return("%d/%d" % ( self.month, self.day ))

    @property
    def ymd_terse(self):
        return("%04d%02d%02d" % ( self.year, self.month, self.day ))

    @property
    def sg_date_time(self):
        return("%04d-%02d-%02dT00:00:00-07:00" % ( self.year, self.month, self.day ))

if __name__ == "__main__":
    def test_SDate():
        last_year = ( 2012, 1, 1 )
        start = rt0 = SDate(last_year)
        today = SDate()
        end = rt1 = SDate(( 2013, 1, 10, ))
        print "Start", str(rt0)
        print "End", str(rt1)
        print "Today", str(today)
        print "Today - End", today - end
        print "End - Start", end - start
        print "Today - Start", today - start
        print "Monday was", str(today.monday)
        print "Remember", str(SDate('12-05-05'))
        print "Remember", str(SDate('12/06/06'))
        print "Remember", str(SDate('11/11/11'))
        print "Remember", str(SDate('10/9/8'))
    test_SDate()
