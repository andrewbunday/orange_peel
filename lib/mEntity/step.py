
"Simple Step entity"

if True:
    import m_entity

class Step(m_entity.mEntity):
    DEFAULT = [ 'id', 'code', 'project' ]
    MAP = dict(name='code', created='created_at') # friendly name map, either can be used
    INTERNAL = "Step"
    READ_ONLY = [ 'created_at', 'created_by', 'updated_at', 'updated_by' ]
    def __init__(self,  _id = None,  _code = None, **kwa):
        "Setup translations for calling "
        _type = Step.INTERNAL      # What shotgun calls it, ie: Task, CustomEntity10, etc.
        _map = Step.MAP
        _magicmap = dict()              # map of auto-translating functions
        # No pre super
        self._default = Step.DEFAULT[:] # default fields to load if none specified
        super(Step,  self).__init__(_type,  _map,  _magicmap,  _id,  _code, **kwa) # Et Voila!
        # No post super
        self._class = Step
        self._read_only = Step.READ_ONLY

if __name__ == "__main__":
    import sys, pprint
    def test_cases():
        # step = Step(None, 'foo') # This should blow up, no such version name
        # id_step = Step(None, 357) # This should blow up unless there is a version 357 somewhere
        steps = Step().match(projectId=96) # Get all default step fields for this project
        for step in steps:
            print str(step)
    args = sys.argv[:]
    pname = args.pop(0)
    if len(args) < 1:
        test_cases()
        sys.exit(0)
    _id = None
    if args[0] == '-m':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Step(_id)
        entity.parse_keylist(args)
        if _id: rc = entity.update()
        else: rc = entity.create()
        pprint.pprint(rc)
    elif args[0] == '-i':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Step(_id, _fields=['_all'])
        print str(entity)
    sys.exit(0)

