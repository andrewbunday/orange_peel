
"Simple Host entity"

if True:
    import m_entity

class Host(m_entity.mEntity):
    DEFAULT = [ 'id', 'code', 'project' ]
    MAP = dict(name='code', created='created_at') # friendly name map, either can be used
    INTERNAL = "Host"
    READ_ONLY = [ 'created_at', 'created_by', 'updated_at', 'updated_by' ]
    def __init__(self,  _id = None,  _code = None, **kwa):
        "Setup translations for calling "
        _type = Host.INTERNAL      # What shotgun calls it, ie: Task, CustomEntity10, etc.
        _map = Host.MAP
        _magicmap = dict()              # map of auto-translating functions
        self._auxOnly = True
        self._default = Host.DEFAULT[:] # default fields to load if none specified
        super(Host,  self).__init__(_type,  _map,  _magicmap,  _id,  _code, **kwa) # Et Voila!
        # No post super
        self._class = Host
        self._read_only = Host.READ_ONLY

if __name__ == "__main__":
    import sys, pprint
    def test_cases():
        hosts = Host().match(projectId=96) # Get all default host fields for this project
        for host in hosts:
            print str(host)
    args = sys.argv[:]
    pname = args.pop(0)
    if len(args) < 1:
        test_cases()
        sys.exit(0)
    _id = None
    if args[0] == '-m':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Host(_id)
        entity.parse_keylist(args)
        if _id: rc = entity.update()
        else: rc = entity.create()
        pprint.pprint(rc)
    elif args[0] == '-i':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Host(_id, _fields=['_all'])
        print str(entity)
    elif args[0] == '-t':
        h = Host()              # Grab a dummy
        hosts = h.match()       # Get em all
        for h in hosts:
            print
            print h('_id')
            # pprint.pprint(h._auxData)
            # pprint.pprint(h.auxData())
            keys = h.auxData().keys()
            keys.sort()
            for k in keys:
                print "%-20s %s" % ( k, h(k) )
    sys.exit(0)

"""
Host
Field Name	Data Type	Example					Notes*

hostname	string		gfxuk002				easy identifier, used by UI elements
ip_address	string		10.10.160.23				unique key
fqdn		string		gfxuk002.london.methodstudios.com	until dns is sorted we rely on this for anything
location	string		london					Whatever we use here should match the definition in freight
sessions	Array of Sessions [Session(), Session()...]		these are the sessions a host is subscribed to
synced_media_type string	all					A string identifier that specifies the type of media to copy to the host during synchronisation.

									Current valid options are: all,frames,movie
									More options may be added a later time.
"""
