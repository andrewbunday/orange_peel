
"Simple Page entity"

if True:
    import m_entity

class Page(m_entity.mEntity):
    DEFAULT = [ 'id', 'code', 'project' ]
    MAP = dict(name='code', created='created_at') # friendly name map, either can be used
    INTERNAL = "PageSetting"
    READ_ONLY = [ 'created_at', 'created_by', 'updated_at', 'updated_by' ]
    def __init__(self,  _id = None,  _code = None, **kwa):
        "Setup translations for calling "
        _type = Page.INTERNAL      # What shotgun calls it, ie: Task, CustomEntity10, etc.
        _map = Page.MAP
        _magicmap = dict()              # map of auto-translating functions
        # No pre super
        self._default = Page.DEFAULT[:] # default fields to load if none specified
        super(Page,  self).__init__(_type,  _map,  _magicmap,  _id,  _code, **kwa) # Et Voila!
        self._ename = 'Page'
        self._class = Page
        self._read_only = Page.READ_ONLY

if __name__ == "__main__":
    import sys, pprint
    def test_cases():
        # pagesetting = Page(None, 'foo') # This should blow up, no such version name
        # id_page = Page(357) # This should blow up unless there is a 357 on this system
        pages = Page().match(projectId=96) # Get all default page fields for this project
        for page in pages:
            print str(page)
    args = sys.argv[:]
    pname = args.pop(0)
    if len(args) < 1:
        test_cases()
        sys.exit(0)
    _id = None
    if args[0] == '-m':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Page(_id)
        entity.parse_keylist(args)
        if _id: rc = entity.update()
        else: rc = entity.create()
        pprint.pprint(rc)
    elif args[0] == '-i':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Page(_id, _fields=['_all'])
        print str(entity)
    sys.exit(0)

