#!/bin/env python

import sys
import time

from sdate import SDate

from project import Project
from version import Version

PROJ_NM = 'zzCatbird'
PROJ_LA = '%s_la' % PROJ_NM
PROJ_VN = '%s_vancouver' % PROJ_NM

la = Project(None, PROJ_LA)
vn = Project(None, PROJ_VN) # ; print vn.server_info ; sys.exit(0)

fields = [ 'name', 'frompath', 'sync_needed' ]
st = time.time()
try:
    vn_versions = Version().match(project=vn.link,
                                  sync_needed=False,
                                  from_path=('is_not', None),
                                  _fields=fields)
except RuntimeError, e:
    print "Oh crap, too much data! Try paging?", str(e)
    sys.exit(1)
elapsed = time.time() - st
print "[%d versions loaded in %.3f seconds]" % ( len(vn_versions), elapsed )

st = time.time()
updates = 0
for v in vn_versions:
    if '%04d' in v['from_path']:
        v['sync_needed'] = True
        rc = v.update()
        if not rc:
            print "Didn't update %s" % v
        else:
            updates += 1
            
elapsed = time.time() - st
print "[%d versions created in %.3f seconds]" % ( updates, elapsed )
sys.exit(0)

