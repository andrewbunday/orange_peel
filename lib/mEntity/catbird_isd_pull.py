#!/bin/env python

# from ConfigParser import SafeConfigParser as SCP

import exceptions
import os
import sys
import time
import types

from sdate import SDate

from project import Project
from version import Version

class RangeError(exceptions.Exception):
    def __init__(self, e):
        super(RangeError, self).__init__(e)

class App(object):
    PROJ_NM = 'zzCatbird'
    PROJ_LA = '%s_la' % PROJ_NM
    PROJ_VN = '%s_vancouver' % PROJ_NM
    LOGFILE = '/tmp/catbird_isd_pull.log'
    def __init__(self, src = None, dest = None):
        self._earliest = SDate('2013-08-01')
        # self._config = SCP(os.path.join(os.path.expanduser("~"), "catbird_isd.ini"))
        pfields = Project.DEFAULT + [ 'name' ]
        nm = App.PROJ_VN
        if src: nm = src
        self._source = Project(None, nm, _fields = pfields)
        nm = App.PROJ_LA
        if dest: nm = dest
        self._dest   = Project(None, nm, _fields = pfields)
        self._fields = [ 'name', 'from_path', 'link', 'status', 'user', 'updated', 'vtype', 'frame_range' ]
        self._dict = dict(prefix = App.PROJ_NM, earliest = self._earliest,
                          fields=self._fields, source_project=self._source, dest_project=self._dest,
                          source_name=self._source['name'], dest_name=self._dest['name'])
        self.log("Initialized")

    def log(self, msg):
        ostr = "%s\t%s\n" % ( SDate().now, msg )
        ofd = open(App.LOGFILE, 'a')
        ofd.write(ostr)
        ofd.close()

    def __getitem__(self, k): return self._dict[k]

    def __str__(self): "%(source_name)s -> %(dest_name)s" % self._dict

    def remap_frompath(self, src, dest, k):
        "Doesn't do remap, just does data move"
        if not src[k]: return
        # dest_path = src[k].replace('/jobs/vfx_catbird/', '/jobs/vfx_catbird_la/')
        # dest[k] = dest_path
        self._spawn_transfer(key=k, start=None, end=None)
        # print "Remapped %s --> %s" % ( src[k], dest[k] )
        # Repackaging goes here

    def _cp(self, src_fp, dest_fp):
        print "Overlapped file systems: I ain't copying shit: %s" % src_fp

    def _spawn_transfer(self, **kwa):
        rtn = "App._spawn_transfer"
        start = kwa.get('start', None)
        end   = kwa.get('end', None)
        key   = kwa.get('key', None)
        signature = self._source.signature
        if (start == None) or (end == None): raise RangeError("%s: range incomplete" % rtn)
        if not key:			     raise RuntimeError("%s: No field key" % rtn)
        pid = os.fork()
        if pid: return
        for framenum in xrange(start, end):
            src_fp = self._source[k] % i
            dest_fp = self._dest[k] % i
            # TODO: remap dest_fp here if mapping known...
            self._cp(src_fp, dest_fp)
        self.log("Transfer Complete: %s --> %s" % ( src_fp, dest_fp ))
        self._dest['sync_signature'] = signature
        self._dest['synced_on'] = SDate().gmt
        self._source['sync_needed'] = False
        self._source.update()
        self._dest.update()
        self.log("Updates Complete: %s %s" % ( self._source, self._dest ))
        sys.exit(0)

    def run(self):
        done = False
        self.log("Starting")
        v_fields = [ 'name', 'from_path', 'sync_needed', 'frame_range', 'status', 'user', 'vtype', ]
        remappings = dict([(field, None) for field in v_fields])
        remappings['from_path'] = self.remap_frompath
        while not done:
            st = time.time()
            try:
                self.source_versions = Version().match(project=self._source.link,
                                                       sync_needed=True, from_path=('is_not', None),
                                                       _limit = 100,
                                                       _fields = v_fields)
            except RuntimeError, e:
                self.log("Initial Version load failed -- punt: %s" % str(e) )
                sys.exit(1)
            elapsed = time.time() - st
            self.log("%d versions loaded in %.3f seconds" % ( len(self.source_versions), elapsed ))
            for src_v in self.source_versions:
                dest_v = Version()
                dest_v['project'] = src_v.link
                src_v.remap(dest_v, **remappings)
                dest_v['signature']   = src_v.signature
                dest_v['synced_on']   = SDate().gmt
                dest_v['sync_needed'] = None
                rc = dest_v.create()
                if rc:
                    self.log("Created %s from sync" % dest_v['name'])
                src_v['sync_needed'] = False
                rc = src_v.update()
                if rc:
                    self.log("Source %s unmarked" % src_v['name'])
            time.sleep(5)

if __name__ == "__main__":
    app = App()
    app.run()
    sys.exit(0)
