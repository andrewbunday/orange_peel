
"Simple EditList entity"

if True:
    import m_entity

class EditList(m_entity.mEntity):
    DEFAULT = [ 'id', 'code', 'project' ]
    MAP = dict(name='code', created='created_at') # friendly name map, either can be used
    INTERNAL = "EditList"
    READ_ONLY = [ 'created_at', 'created_by', 'updated_at', 'updated_by' ]
    def __init__(self,  _id = None,  _code = None, **kwa):
        "Setup translations for calling "
        _type = EditList.INTERNAL      # What shotgun calls it, ie: Task, CustomEntity10, etc.
        _map = EditList.MAP
        _magicmap = dict()              # map of auto-translating functions
        self._auxOnly = True
        self._default = EditList.DEFAULT[:] # default fields to load if none specified
        super(EditList,  self).__init__(_type,  _map,  _magicmap,  _id,  _code, **kwa) # Et Voila!
        # No post super
        self._class = EditList
        self._read_only = EditList.READ_ONLY

if __name__ == "__main__":
    import sys, pprint
    def test_cases():
        editlists = EditList().match(projectId=96) # Get all default editlist fields for this project
        for editlist in editlists:
            print str(editlist)
    args = sys.argv[:]
    pname = args.pop(0)
    if len(args) < 1:
        test_cases()
        sys.exit(0)
    _id = None
    if args[0] == '-m':
        args.pop(0)
        _id = int(args.pop(0))
        entity = EditList(_id)
        entity.parse_keylist(args)
        if _id: rc = entity.update()
        else: rc = entity.create()
        pprint.pprint(rc)
    elif args[0] == '-i':
        args.pop(0)
        _id = int(args.pop(0))
        entity = EditList(_id, _fields=['_all'])
        print str(entity)
    sys.exit(0)

