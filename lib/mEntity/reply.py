
"Simple Reply entity"

if True:
    import m_entity

class Reply(m_entity.mEntity):
    DEFAULT = [ 'id', 'content' ]
    MAP = dict(link='entity', parent='entity', reply_text='content') # friendly name map, either can be used
    INTERNAL = "Reply"
    READ_ONLY = [ 'created_at', 'created_by', 'updated_at', 'updated_by' ]
    def __init__(self,  _id = None,  _code = None, **kwa):
        "Setup translations for calling "
        _type = Reply.INTERNAL      # What shotgun calls it, ie: Task, CustomEntity10, etc.
        _map = Reply.MAP
        _magicmap = dict()              # map of auto-translating functions
        # No pre super
        self._default = Reply.DEFAULT[:] # default fields to load if none specified
        super(Reply,  self).__init__(_type,  _map,  _magicmap,  _id,  _code, **kwa) # Et Voila!
        # No post super
        self._class = Reply
        self._read_only = Reply.READ_ONLY

if __name__ == "__main__":
    import sys, pprint
    def test_cases():
        replys = Reply().match(projectId=96) # Get all default reply fields for this project
        for reply in replys:
            print str(reply)
    args = sys.argv[:]
    pname = args.pop(0)
    if len(args) < 1:
        test_cases()
        sys.exit(0)
    _id = None
    if args[0] == '-m':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Reply(_id)
        entity.parse_keylist(args)
        if _id: rc = entity.update()
        else: rc = entity.create()
        pprint.pprint(rc)
    elif args[0] == '-i':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Reply(_id, _fields=['_all'])
        print str(entity)
    sys.exit(0)

