
"Simple Cut entity"

if True:
    import m_entity

class Cut(m_entity.mEntity):
    DEFAULT = [ 'id', 'code', 'project' ]
    MAP = dict(name='code', bfg_source='sg_bfg_source', edl='sg_bfg_edl', rv_session='sg_bfg_rv_session',
               sequence='sg_sequence', seq='sg_sequence', cut_type='sg_cut_type',
               created='created_at') # friendly name map, either can be used
    INTERNAL = "Cut"
    READ_ONLY = [ 'created_at', 'created_by', 'updated_at', 'updated_by' ]
    def __init__(self,  _id = None,  _code = None, **kwa):
        "Setup translations for calling "
        _type = Cut.INTERNAL      # What shotgun calls it, ie: Task, CustomEntity10, etc.
        _map = Cut.MAP
        _magicmap = dict()              # map of auto-translating functions
        # No pre super
        self._default = Cut.DEFAULT[:] # default fields to load if none specified
        super(Cut,  self).__init__(_type,  _map,  _magicmap,  _id,  _code, **kwa) # Et Voila!
        # No post super
        self._class = Cut
        self._read_only = Cut.READ_ONLY

if __name__ == "__main__":
    import sys, pprint
    def test_cases():
        cuts = Cut().match(projectId=96) # Get all default cut fields for this project
        for cut in cuts:
            print str(cut)
    args = sys.argv[:]
    pname = args.pop(0)
    if len(args) < 1:
        test_cases()
        sys.exit(0)
    _id = None
    if args[0] == '-m':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Cut(_id)
        entity.parse_keylist(args)
        if _id: rc = entity.update()
        else: rc = entity.create()
        pprint.pprint(rc)
    elif args[0] == '-i':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Cut(_id, _fields=['_all'])
        print str(entity)
    sys.exit(0)

