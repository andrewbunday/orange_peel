
"Simple Source entity"

if True:
    import m_entity

class Source(m_entity.mEntity):
    DEFAULT = [ 'id', 'code', 'project' ]
    MAP = dict(name='code', created='created_at') # friendly name map, either can be used
    INTERNAL = "Source"
    READ_ONLY = [ 'created_at', 'created_by', 'updated_at', 'updated_by' ]
    def __init__(self,  _id = None,  _code = None, **kwa):
        "Setup translations for calling "
        _type = Source.INTERNAL      # What shotgun calls it, ie: Task, CustomEntity10, etc.
        _map = Source.MAP
        _magicmap = dict()              # map of auto-translating functions
        self._auxOnly = True
        self._default = Source.DEFAULT[:] # default fields to load if none specified
        super(Source,  self).__init__(_type,  _map,  _magicmap,  _id,  _code, **kwa) # Et Voila!
        # No post super
        self._class = Source
        self._read_only = Source.READ_ONLY

if __name__ == "__main__":
    import sys, pprint
    def test_00():
        src = Source(_debug=True).include(code='Test_01')
        src.create()
        print src

    def test_01():
        src = Source(None, "Test_01", _debug=True)
        print src

    def test_02():
        src = Source()
        rl = src.match(code='Test_01')
        for srcObj in rl: print srcObj

    def test_cases():
        sources = Source().match(projectId=96) # Get all default source fields for this project
        for source in sources:
            print str(source)

    args = sys.argv[:]
    pname = args.pop(0)
    if len(args) < 1:
        test_00()
        test_01()
        test_02()
        sys.exit(0)
    _id = None
    if args[0] == '-m':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Source(_id)
        entity.parse_keylist(args)
        if _id: rc = entity.update()
        else: rc = entity.create()
        pprint.pprint(rc)
    elif args[0] == '-i':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Source(_id, _fields=['_all'])
        print str(entity)
    sys.exit(0)

