
"Simple Sequence entity"

if True:
    import m_entity

class Sequence(m_entity.mEntity):
    DEFAULT = [ 'id', 'code', 'project' ]
    MAP = dict(name='code', parent='entity', created='created_at', sequence='sg_sequence') 
    INTERNAL = "Sequence"
    READ_ONLY = [ 'created_at', 'created_by', 'updated_at', 'updated_by' ]
    def __init__(self,  _id = None,  _code = None, **kwa):
        "Setup translations for calling "
        _type = Sequence.INTERNAL      # What shotgun calls it, ie: Task, CustomEntity10, etc.
        _map = Sequence.MAP
        _magicmap = dict()              # map of auto-translating functions
        # No pre super
        self._default = Sequence.DEFAULT[:] # default fields to load if none specified
        super(Sequence,  self).__init__(_type,  _map,  _magicmap,  _id,  _code, **kwa) # Et Voila!
        # No post super
        self._class = Sequence
        self._read_only = Sequence.READ_ONLY

if __name__ == "__main__":
    import sys, pprint
    def test_cases():
        sequences = Sequence().match(projectId=96) # Get all default sequence fields for this project
        for sequence in sequences:
            print str(sequence)
    args = sys.argv[:]
    pname = args.pop(0)
    if len(args) < 1:
        test_cases()
        sys.exit(0)
    _id = None
    if args[0] == '-m':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Sequence(_id)
        entity.parse_keylist(args)
        if _id: rc = entity.update()
        else: rc = entity.create()
        pprint.pprint(rc)
    elif args[0] == '-i':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Sequence(_id, _fields=['_all'])
        print str(entity)
    sys.exit(0)

