#!/usr/bin/env /usr/bin/python

""" egen -- entity generator
    % egen [-s sgname] [-d default_field,default_field,...] [-f friendly:native,friendly:native,...] entity_name > entity_name.py
    """

import sys

template = """
"Simple %(class)s entity"

if True:
    import entity

class %(class)s(entity.Entity):
    DEFAULT = [ %(DEFAULT)s ]
    MAP = dict(%(MAP)s) # friendly name map, either can be used
    INTERNAL = "%(etype)s"
    def __init__(self,  _id = None,  _code = None, **kwa):
        "Setup translations for calling "
        _type = %(class)s.INTERNAL      # What shotgun calls it, ie: Task, CustomEntity10, etc.
        _map = %(class)s.MAP
        _magicmap = dict()              # map of auto-translating functions
        %(pre_super)s
        self._default = %(class)s.DEFAULT[:] # default fields to load if none specified
        super(%(class)s,  self).__init__(_type,  _map,  _magicmap,  _id,  _code, **kwa) # Et Voila!
        %(post_super)s
        self._class = %(class)s

if __name__ == "__main__":
    import sys, pprint
    def test_cases():
        # %(et_lower)s = %(class)s(None, '%(test_code)s') # This should blow up, no such version name
        # id_%(class_lc)s = %(class)s(None, 357) # This should blow up unless there is a version 357 somewhere
        %(class_lc)ss = %(class)s().match(projectId=96) # Get all default %(class_lc)s fields for this project
        for %(class_lc)s in %(class_lc)ss:
            print str(%(class_lc)s)
    args = sys.argv[:]
    pname = args.pop(0)
    if len(args) < 1:
        test_cases()
        sys.exit(0)
    _id = None
    if args[0] == '-m':
        args.pop(0)
        _id = int(args.pop(0))
        entity = %(class)s(_id)
        entity.parse_keylist(args)
        if _id: rc = entity.update()
        else: rc = entity.create()
        pprint.pprint(rc)
    elif args[0] == '-i':
        args.pop(0)
        _id = int(args.pop(0))
        entity = %(class)s(_id, _fields=['_all'])
        print str(entity)
    sys.exit(0)
"""
# example: python entity_generator.py --fc Session CustomEntity01 > session.py

args = sys.argv[:]
pname = args.pop(0)
if not args:
    print """
egen [--fc FriendlyClassName] [--map friendly_name=shotgun_name,...] EntityType [> entity_type.py]
"""
    sys.exit(0)
friendly_class_name = None
if ('--fc') in args:
    swx = args.index('--fc')
    args.pop(swx)
    friendly_class_name = args.pop(swx)

mappings = [ ]
if '--map' in args:
    mappings = [ ]
    swx = args.index('--map')
    args.pop(swx)
    mappairs = args.pop(swx).split(',')
    for pair in mappairs:
        nm, v = pair.split('=', 1)
        v = "'" + v + "'"
        mappings.append((nm, v))

etype = args.pop(0)
et_lower = etype.lower()
_map = mappings if mappings else "name='code', created='created_at'"
_default = "'id', 'code', 'project'"
dct = dict(etype=etype, et_lower=et_lower, test_code='foo',
           DEFAULT = _default, MAP = _map, 
           post_super="# No post super", pre_super="# No pre super")
dct['class'] = etype
if friendly_class_name:
    dct['class'] = friendly_class_name
    dct['post_super'] = "self._ename = '%s'" % friendly_class_name
dct['class_lc'] = dct['class'].lower()
print template % dct
sys.exit(0)

