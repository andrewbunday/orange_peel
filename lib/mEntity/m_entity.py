#!/bin/env python

if True:
    import json
    import types
    import yaml

    import entity

class mEntity(entity.Entity):
    def __init__(self, *args, **kwa):
        super(mEntity, self).__init__(*args, **kwa)

    def produce(self, _class, *args, **kwa):
        e = _class(*args, **kwa)
        return e

    def remap(src, dest, **kwa):
        """Remap fields from src to dest, based on fields in kwa, if key points to None,
           data is passed straight across, returns null dict if no update took place"""
        if not isinstance(dest, src._class):
            raise TypeError("mEntity.remap: source and destination classes don't match")
        update = kwa.get('_update', False)
        copy_remaining = kwa.get('_copy_remaining', False)
        if copy_remaining:
            print "m_entity.remap -- UNDEBUGGED AND RAW!"
            raw_fields   = set(src.rawFields)
            src_fields   = set(src.keys())
            dest_fields  = set(dest.keys())
            intersection = dest_fields.intersection(raw_fields)
            do_fields    = intersection.difference(src_fields)
            do_dict      = dict([(k, None) for k in do_fields])
            kwa.update(do_dict)
        copies = 0
        for k in kwa:    		# If k is not in src, no complaint is emitted
            if k.startswith('_'): continue
            if k in self._magic: continue
            # if k != k.lower(): continue # Mixed case means magic method, no remap for now;
            orig_k = k
            if k in src._map: k = src._map[k]
            if k in src:
                mapfunc = kwa[orig_k]
                if mapfunc:
                    mapfunc(src, dest, k)
                else:
                    dest[k] = src[k]
                copies += 1
        if copies:
            dest['sg_sync_signature'] = src.signature
            dest['sg_synced_on'] = src.now # Produce now string as GMT in SG format
        if update:      		# I get that this is too many double negatives
            print "mEntity.remap: Updating"
            return dest.update()
        return {}

    def json(self, *keys, **kwa):
        include_entity = kwa.get('include_entity', False)
        as_dict = kwa.get('as_dict', False)
        if not keys:
            keys = self.keys()
        dct = dict()
        for k in keys: dct[k] = self[k]
        dct['_entity'] = self._etype
        if as_dict: return dct
        jdoc = json.dumps(dct, indent=4)
        return jdoc

    def yaml(self, *keys, **kwa):
        as_dict = kwa.get('as_dict', False)
        if not keys: keys = self.keys()
        yaml_ = dict()
        for k in keys: yaml_[k] = self[k]
        yaml_['_entity'] = self._etype
        if as_dict: return yaml_
        doc = yaml.dump(yaml_)
        return doc

if __name__ == "__main__":
    # Inline tests
    import sys
    test_name = "mEntity"
    _type = "Project"
    _map = { 'nickname':'code',  'description':'sg_description',
             'due':'sg_due', 'type':'sg_type',  'status':'sg_status', }
    _magicmap = dict()
    _id = 1058
    _code = "vfx_catbird"
    print "[Testing %s for project %s]" % ( test_name, _code )
    ent_by_code = mEntity(_type, _map, _magicmap, None, _code)
    ent_by_id = mEntity(_type, _map, _magicmap, _id, None)
    assert ent_by_code['nickname'] == _code,  "Wrong ent_by_code[name]"
    assert ent_by_id['nickname'] == _code,  "Wrong ent_by_id[name]"
    assert ent_by_code['id'] == _id, "Wrong ent_by_code[id]"
    assert ent_by_id['id'] == _id,  "Wrong ent_by_id[id]"
    print "Project %s verified" % ent_by_id['nickname']

    print "[%s testing ALTERNATE support]" % test_name
    alt_by_code = mEntity(_type, _map, _magicmap, None, _code, _alternate=("la", "prod"))
    assert alt_by_code['nickname'] == _code, "Wrong alt_by_code[name]"
    print str(alt_by_code)
    sys.exit(0)

    
