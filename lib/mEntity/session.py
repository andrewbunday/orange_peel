
"Simple Session entity"

if True:
    import m_entity

class Session(m_entity.mEntity):
    DEFAULT = [ 'id', 'code', 'project' ]
    MAP = dict(name='code', created='created_at') # friendly name map, either can be used
    INTERNAL = "Session"
    READ_ONLY = [ 'created_at', 'created_by', 'updated_at', 'updated_by' ]
    def __init__(self,  _id = None,  _code = None, **kwa):
        "Setup translations for calling "
        _type = Session.INTERNAL      # What shotgun calls it, ie: Task, CustomEntity10, etc.
        _map = Session.MAP
        _magicmap = dict()              # map of auto-translating functions
        self._auxOnly = True
        self._default = Session.DEFAULT[:] # default fields to load if none specified
        super(Session,  self).__init__(_type,  _map,  _magicmap,  _id,  _code, **kwa) # Et Voila!
        # No post super
        self._class = Session
        self._read_only = Session.READ_ONLY

if __name__ == "__main__":
    import sys, pprint
    def test_cases():
        sessions = Session().match(projectId=96) # Get all default session fields for this project
        for session in sessions:
            print str(session)
    args = sys.argv[:]
    pname = args.pop(0)
    if len(args) < 1:
        test_cases()
        sys.exit(0)
    _id = None
    if args[0] == '-m':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Session(_id)
        entity.parse_keylist(args)
        if _id: rc = entity.update()
        else: rc = entity.create()
        pprint.pprint(rc)
    elif args[0] == '-i':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Session(_id, _fields=['_all'])
        print str(entity)
    sys.exit(0)

"""
A Session is a collection entity type. It provides a means of gathering a number
of similar playlists together. This allows a user to subscribe to Session and
receive all playlists that are contained in it, or that are added to it. An
example of this may be where a Session is setup for a shared project and new
playlists are generated each day to review.

Entity name: Session

Field Name	Data Type	Example					Notes*
name		string		"vfx_monthlies"
*std shotgun fields n/a		created_by, last_updated, date_created
playlists	array of Playlists [Playlist(), Playlist()...]		* should only be "Shared Playlists", not Shotgun ones.
"""

