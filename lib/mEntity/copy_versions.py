#!/bin/env python

import sys
import time

from sdate import SDate

from asset import Asset
from project import Project
from version import Version

class TypedCache(object):
    def __init__(self):
        self._cache = dict() # ordered by type, then dict of id=dct entries;

    def add(self, _dict):
        _type = _dict['type']
        _id = _dict['id']
        if _type not in self._cache: self._cache[_type] = dict()
        self._cache[_type][_id] = _dict

    def build(self, items, selector):
        for item in items:
            dct = item[selector]
            if not dct: continue
            self.add(dct)

    def fields(self, _type = "Version", selector = 'name'):
        if _type not in self._cache: return []
        nms = [item[_id][selector] for item in self._cache[_type]]
        return nms
            
    def find(self, target, _type = "Version", selector = 'name'):
        "return a list where item[selector] matches target"
        if _type not in self._cache: return None
        _items = [item for item in self._cache[_type] if item[selector] == target]
        return items

    def all_values(self, _type = "Version", selector = 'name'):
        if _type not in self._cache: return []
        dct = self._cache[_type]
        _list = [dct[_id][selector] for _id in dct]
        return _list

class App(object):
    def __init__(self, src_name, dest_name, base_id=1058):
        self.elapsed = 0
        self._limit = 200
        self._errors = [ ]
        self._src_versions = [ ]
        pfields = [ 'name', 'code' ]
        self._src_proj  = Project(None, src_name, _fields = pfields)
        self._dest_proj = Project(None, dest_name, _fields = pfields)
        self._base = Project(base_id, _fields = pfields)
        self._src_cache = TypedCache()
        self._get_fields = dict(Version=[ 'name', 'from_path', 'frame_range', 'frame_count',
                                          'description', 'link', 'status', 'user', 'updated',
                                          'sync_needed',
                                          'vtype' ])
        self._remap_fields = dict([ (k, None) for k in self._get_fields['Version'] ])
        self._remap_fields['link'] = self.remap_link

    def __len__(self): return len(self._src_versions)

    def add_asset(self, remote_asset_dict):
        newAsset = Asset(project=self._dest_proj.link)
        handled = [ ]
        dont = [ 'type', 'id' ]
        for k in remote_asset.data():
            if k in dont: continue
            if k in newAsset: continue
            if type(remote_asset[k]) == types.DictType and (k not in handled):
                print "remote_asset %s %s is link, handled" % ( remote_asset, k )
                sys.exit(0)
            newAsset[k] = remote_asset_dict[k]
        
    def load(self):
        st = time.time()
        self._src_versions = Version().match(project = self._base.link,
                                             _fields = self._get_fields['Version'],
                                             _limit = self._limit)
        self._mapping = self.make_link_mapping()
        self.elapsed = time.time() - st
        return self._src_versions

    # Ok, we need to equate the source version links to the dest version links,
    # because they might have the same names, but they have different id's
    def make_link_mapping(self):
        st = time.time()
        self._src_cache.build(self._src_versions, 'link')
        names = self._src_cache.all_values("Asset", "name")
        print "names", names
        mapping = dict() # src_id to dest_id
        for nm in names:
            v = Version().match(project=self._src_proj.link, name=nm)
            if not v: 				# No matching name
                print "Can't match %s to source show %s" % ( nm, self._src_proj['name'])
                sys.exit(0); continue
            link = src_cache.find(nm, "Version", "name")[0]
            print "links from cache:", link ; sys.exit(0)
            mapping[link['id']] = v.link
        self.elapsed = st - time.time()
        return mapping

    def remap_link(self, src, dest, k):
        # Implicit use of 'link' (which is what k should be)
        mapping = self._mapping
        if not src[k]:
            dest[k] = src[k]
            return
        _id = src[k]['id']
        if _id not in mapping: return
        dest['link'] = mapping[_id]

    def copy_with_remap(self):
        st = 0
        copies = 0
        link = self._dest_proj.link
        for v in self.source:
            nv = Version()
            nv['project'] = link
            v.remap(nv, **self._remap_fields)
            rc = nv.create()
            if not rc:
                self._errors.append("Didn't create %s" % nv)
            else:
                copies += 1
        return copies

    @property
    def source(self): return self._src_versions

    @property
    def dest(self): return self._dest_versions

    @property
    def errors(self): return self._errors

# ________________________________________________________________
# Main

PROJ_NM = 'zzCatbird'
PROJ_LA = '%s_la' % PROJ_NM
PROJ_VN = '%s_vancouver' % PROJ_NM

app = App(PROJ_LA, PROJ_VN)
app.load()
print "[%d versions loaded in %.3f seconds]" % ( len(app), app.elapsed )


n = app.copy_with_remap()
if app.errors:
    print "\n".join(app.errors)
print "[%d versions copied in %.3f seconds]" % ( n, app.elapsed )
sys.exit(0)

