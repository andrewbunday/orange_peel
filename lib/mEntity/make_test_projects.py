#!/bin/env python

from project import Project

PROJ_NM = 'zzCatbird'
PROJ_LA = '%s_la' % PROJ_NM
PROJ_VN = '%s_vancouver' % PROJ_NM

# Both of these build off the dev servers: Aug 31, 2013;
proj_la = Project()
proj_vn = Project()

proj_la['name'] = PROJ_LA
proj_vn['name'] = PROJ_VN

if not Project(None, PROJ_LA):
    rc = proj_la.create()
    print "[Created %s: %s]" % ( proj_la, rc )

if not Project(None, PROJ_VN):
    proj_vn.create()
    print "[Created %s: %s]" % ( proj_vn, rc )
