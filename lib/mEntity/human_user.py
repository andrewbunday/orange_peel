
"Simple User entity"

if True:
    import m_entity

class User(m_entity.mEntity):
    DEFAULT = [ 'id', 'name', 'sg_status_list' ]
    MAP = dict(code='name', first='firstname', last='lastname', status='sg_status_list')
    INTERNAL = "HumanUser"
    READ_ONLY = [ 'created_at', 'created_by', 'updated_at', 'updated_by' ]
    def __init__(self,  _id = None,  _code = None, **kwa):
        "Setup translations for calling "
        _type = User.INTERNAL      # What shotgun calls it, ie: Task, CustomEntity10, etc.
        _map = User.MAP
        _magicmap = dict()              # map of auto-translating functions
        # No pre super
        self._default = User.DEFAULT[:] # default fields to load if none specified
        super(User,  self).__init__(_type,  _map,  _magicmap,  _id,  _code, **kwa) # Et Voila!
        self._ename = 'User'
        self._class = User
        self._read_only = User.READ_ONLY

if __name__ == "__main__":
    import sys, pprint
    def test_cases():
        # person = User(None, 'foo') # This should blow up, no such version name
        # id_user = User(357) # This should blow up unless there is a version 357 somewhere
        users = User().match(projectId=96) # Get all default user fields for this project
        for user in users:
            print str(user)
    args = sys.argv[:]
    pname = args.pop(0)
    if len(args) < 1:
        test_cases()
        sys.exit(0)
    _id = None
    if args[0] == '-m':
        args.pop(0)
        _id = int(args.pop(0))
        entity = User(_id)
        entity.parse_keylist(args)
        if _id: rc = entity.update()
        else: rc = entity.create()
        pprint.pprint(rc)
    elif args[0] == '-i':
        args.pop(0)
        _id = int(args.pop(0))
        entity = User(_id, _fields=['_all'])
        print str(entity)
    sys.exit(0)

