
"Simple Synchronizer entity"

if True:
    import m_entity

class Synchronizer(m_entity.mEntity):
    DEFAULT = [ 'id', 'code', 'project' ]
    MAP = dict(name='code', created='created_at',
               completed_targets='sg_completed_targets',
               downstream_submission_id='sg_downstream_submission_id',
               local_entity = 'sg_local_entity',
               needs_sync = 'sg_needs_sync',
               source_entity_id='sg_source_entity_id',
               source_location='sg_source_location', source_project='project',
               source_updater_name='sg_source_updater_name',
               synchronizer_name='code',
               target_locations='sg_target_locations', target_project='sg_target_project',
               transfer_entity_type='sg_transfer_entity_type')
    INTERNAL = "CustomEntity10"
    READ_ONLY = [ 'created_at', 'created_by', 'updated_at', 'updated_by' ]
    def __init__(self,  _id = None,  _code = None, **kwa):
        "Setup translations for calling "
        _type = Synchronizer.INTERNAL      # What shotgun calls it, ie: Task, CustomEntity10, etc.
        _map = Synchronizer.MAP
        _magicmap = dict()              # map of auto-translating functions
        # No pre super
        self._default = Synchronizer.DEFAULT[:] # default fields to load if none specified
        super(Synchronizer,  self).__init__(_type,  _map,  _magicmap,  _id,  _code, **kwa) # Et Voila!
        self._ename = 'Synchronizer'
        self._class = Synchronizer
        self._read_only = Synchronizer.READ_ONLY

if __name__ == "__main__":
    import sys, pprint
    def test_cases():
        # customentity10 = Synchronizer(None, 'foo') # This should blow up, no such version name
        # id_synchronizer = Synchronizer(357) # This should blow up unless there is a version 357 somewhere
        synchronizers = Synchronizer().match(projectId=96) # Get all default synchronizer fields for this project
        for synchronizer in synchronizers:
            print str(synchronizer)
    args = sys.argv[:]
    pname = args.pop(0)
    if len(args) < 1:
        test_cases()
        sys.exit(0)
    _id = None
    if args[0] == '-m':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Synchronizer(_id)
        entity.parse_keylist(args)
        if _id: rc = entity.update()
        else: rc = entity.create()
        pprint.pprint(rc)
    elif args[0] == '-i':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Synchronizer(_id, _fields=['_all'])
        print str(entity)
    sys.exit(0)

