
"Simple Task entity"

if True:
    import m_entity

class Task(m_entity.mEntity):
    DEFAULT = [ 'id', 'code', 'project' ]
    MAP = dict(cc='addressings_cc', latest_version='sg_latest_version', name='content',
	       note='sg_notes', parent='entity', thumbnail='image', version='sg_version')
    INTERNAL = "Task"
    READ_ONLY = [ 'created_at', 'created_by', 'updated_at', 'updated_by',
                  'smart_cut_summary_display', 'smart_working_duration',
                  'open_notes', 'open_notes_count', ]
    def __init__(self,  _id = None,  _code = None, **kwa):
        "Setup translations for calling "
        _type = Task.INTERNAL      # What shotgun calls it, ie: Task, CustomEntity10, etc.
        _map = Task.MAP
        _magicmap = dict()              # map of auto-translating functions
        # No pre super
        self._default = Task.DEFAULT[:] # default fields to load if none specified
        super(Task,  self).__init__(_type,  _map,  _magicmap,  _id,  _code, **kwa) # Et Voila!
        # No post super
        self._class = Task
        self._read_only = Task.READ_ONLY

if __name__ == "__main__":
    import sys, pprint
    def test_cases():
        tasks = Task().match(projectId=96) # Get all default task fields for this project
        for task in tasks:
            print str(task)
    args = sys.argv[:]
    pname = args.pop(0)
    if len(args) < 1:
        test_cases()
        sys.exit(0)
    _id = None
    if args[0] == '-m':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Task(_id)
        entity.parse_keylist(args)
        if _id: rc = entity.update()
        else: rc = entity.create()
        pprint.pprint(rc)
    elif args[0] == '-i':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Task(_id, _fields=['_all'])
        print str(entity)
    sys.exit(0)

