
"Simple Playlist entity"

if True:
    import m_entity

class Playlist(m_entity.mEntity):
    DEFAULT = [ 'id', 'code', 'project' ]
    MAP = dict(name='code', created='created_at') # friendly name map, either can be used
    INTERNAL = "Playlist"
    READ_ONLY = [ 'created_at', 'created_by', 'updated_at', 'updated_by' ]
    def __init__(self,  _id = None,  _code = None, **kwa):
        "Setup translations for calling "
        _type = Playlist.INTERNAL      # What shotgun calls it, ie: Task, CustomEntity10, etc.
        _map = Playlist.MAP
        _magicmap = dict()              # map of auto-translating functions
        # No pre super
        self._default = Playlist.DEFAULT[:] # default fields to load if none specified
        super(Playlist,  self).__init__(_type,  _map,  _magicmap,  _id,  _code, **kwa) # Et Voila!
        # No post super
        self._class = Playlist
        self._read_only = Playlist.READ_ONLY

if __name__ == "__main__":
    import sys, pprint
    def test_cases():
        playlists = Playlist().match(projectId=96) # Get all default playlist fields for this project
        for playlist in playlists:
            print str(playlist)
    args = sys.argv[:]
    pname = args.pop(0)
    if len(args) < 1:
        test_cases()
        sys.exit(0)
    _id = None
    if args[0] == '-m':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Playlist(_id)
        entity.parse_keylist(args)
        if _id: rc = entity.update()
        else: rc = entity.create()
        pprint.pprint(rc)
    elif args[0] == '-i':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Playlist(_id, _fields=['_all'])
        print str(entity)
    sys.exit(0)

