
"Simple Version entity"

if True:
    import m_entity

class Version(m_entity.mEntity):
    DEFAULT = [ 'id', 'code', 'project' ]
    MAP = dict(name='code', created='created_at', parent='entity', link='entity',
               count='frame_count', range='frame_range', first='sg_first_frame',
               last='sg_last_frame', 
               from_path='sg_path_to_frames', synchronizer='sg_synchronizer')
    INTERNAL = "Version"
    READ_ONLY = [ 'created_at', 'created_by', 'updated_at', 'updated_by' ]
    def __init__(self,  _id = None,  _code = None, **kwa):
        "Setup translations for calling "
        _type = Version.INTERNAL      # What shotgun calls it, ie: Task, CustomEntity10, etc.
        _map = Version.MAP
        _magicmap = dict()              # map of auto-translating functions
        # No pre super
        self._default = Version.DEFAULT[:] # default fields to load if none specified
        super(Version,  self).__init__(_type,  _map,  _magicmap,  _id,  _code, **kwa) # Et Voila!
        # No post super
        self._class = Version
        self._read_only = Version.READ_ONLY

    def guess_best_tasks(self):
        return [ ]

if __name__ == "__main__":
    import sys, pprint
    def test_cases():
        # version = Version(None, 'foo') # This should blow up, no such version name
        # id_version = Version(None, 357) # This should blow up unless there is a version 357 somewhere
        versions = Version().match(projectId=96) # Get all default version fields for this project
        for version in versions:
            print str(version)
    args = sys.argv[:]
    pname = args.pop(0)
    if len(args) < 1:
        test_cases()
        sys.exit(0)
    _id = None
    if args[0] == '-m':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Version(_id)
        entity.parse_keylist(args)
        if _id: rc = entity.update()
        else: rc = entity.create()
        pprint.pprint(rc)
    elif args[0] == '-i':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Version(_id, _fields=['_all'])
        print str(entity)
    sys.exit(0)

