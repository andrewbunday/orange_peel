#!/usr/bin/env python
"Simple Representation entity"

if True:
    import m_entity

class Representation(m_entity.mEntity):
    DEFAULT = [ 'id', 'code', 'project' ]
    MAP = dict(name='code', path='sg_path', status='sg_status_list', artist='sg_artist')
    INTERNAL = "CustomEntity08"
    READ_ONLY = [ 'created_at', 'created_by', 'updated_at', 'updated_by' ]
    def __init__(self,  _id = None,  _code = None, **kwa):
        "Setup translations for calling "
        _type = Representation.INTERNAL      # What shotgun calls it, ie: Task, CustomEntity10, etc.
        _map = Representation.MAP
        _magicmap = dict()              # map of auto-translating functions
        # No pre super
        self._default = Representation.DEFAULT[:] # default fields to load if none specified
        super(Representation,  self).__init__(_type,  _map,  _magicmap,  _id,  _code, **kwa) # Et Voila!
        # No post super
        self._class = Representation
        self._read_only = Representation.READ_ONLY

if __name__ == "__main__":
    import sys, pprint
    def test_cases():
        representations = Representation().match(projectId=96) # Get all default representation fields for this project
        for representation in representations:
            print str(representation)
    args = sys.argv[:]
    pname = args.pop(0)
    if len(args) < 1:
        test_cases()
        sys.exit(0)
    _id = None
    if args[0] == '-m':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Representation(_id)
        entity.parse_keylist(args)
        if _id: rc = entity.update()
        else: rc = entity.create()
        pprint.pprint(rc)
    elif args[0] == '-i':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Representation(_id, _fields=['_all'])
        print str(entity)
    sys.exit(0)

