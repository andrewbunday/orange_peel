#!/usr/bin/env python2.7
"""backing_store.py: A Generic backing store API used by the Uptake Entity System;"""

# Copyright (c) 2013 Wook

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

if True:
    import time
    import types
    import sys

    from shotgun_api3 import Shotgun
    import pymongo                      # As the auxilary storage method

class Access(object):
    def __init__(self):
        self._master = locations = dict(la=dict(), ny=dict(), van=dict())
        mongo_base = dict(host='bucket01.methodstudios.com', port=27017)
        dev_mongo = mongo_base.copy(); prod_mongo = dev_mongo.copy()
        dev_mongo['collection'] = 'sg_aux'
        dev_mongo['table'] = 'la_sg_dev'
        prod_mongo['collection'] = 'sg_aux'
        prod_mongo['table'] = 'la_sg_prod'
        for k in locations:
            for mode in [ 'prod', 'dev', 'ha_a', 'ha_b', 'ha_proxy', 'hosted_test' ]:
                locations[k][mode] = dict(url = None, keys = dict(), mongo = dict())
        # Now the big dump of internal knowledge
        self._set('la', 'prod', 'url', 'http://shotgun.methodstudios.com')
        self._set('la', 'dev', 'url', 'http://shotgun02.methodstudios.com')
        self._set('la', 'ha_a', 'url', 'http://cs-shotgun01.methodstudios.com')
        self._set('la', 'ha_b', 'url', 'http://la-mc13.methodstudios.com')
        self._set('la', 'ha_proxy', 'url', 'http://ha-shotgun.methodstudios.com')
        self._set('la', 'hosted_test', 'url', 'https://breakme-methodstudios.shotgunstudio.com')
        self._set_key('la', 'prod', 'keys', 'default', 'mEntity_default', '8ce5dad8e9e2e3594e083e785368e6f8d33ac37e')
        self._set_key('la', 'prod', 'keys', 'msend',   'msend_api', '76d4e8cf58fdcd84e5cb185e5685549c6fa4f76a')
        self._set_key('la', 'prod', 'keys', 'notes',   'mISD_Notes_Daemon', 'd2a0bc6020b5645fee96ea81f6ccd1c7c7ec0d04')
        self._set_key('la', 'ha_proxy', 'keys', 'default', 'mEntity_default', '8ce5dad8e9e2e3594e083e785368e6f8d33ac37e')
        self._set_key('la', 'ha_a', 'keys', 'default', 'mEntity_default', '8ce5dad8e9e2e3594e083e785368e6f8d33ac37e')
        self._set_key('la', 'ha_b', 'keys', 'default', 'mEntity_default', '8ce5dad8e9e2e3594e083e785368e6f8d33ac37e')
        self._set_key('la', 'hosted_test', 'keys', 'default',
                      'mEntity_default', '65bfa732981d5f594757fb753a46748afd60b084')
        self._set_key('la', 'dev', 'keys', 'default',  'mEntity_default',   '8ce5dad8e9e2e3594e083e785368e6f8d33ac37e')
        self._set_key('la', 'dev', 'keys', 'notes',    'mISD_Notes_Daemon', '8dc61eef136a527223a66d2cf8d038f54ce54afc')
        self._set('la', 'dev', 'mongo', dev_mongo)
        self._set('la', 'prod', 'mongo', prod_mongo)
        self._set('la', 'ha_a', 'mongo', prod_mongo)
        self._set('la', 'ha_b', 'mongo', prod_mongo)
        self._set('la', 'ha_proxy', 'mongo', prod_mongo)
        self._set('la', 'hosted_test', 'mongo', dev_mongo)

        self._set('van', 'prod', 'url', 'http://van-shotgun.methodstudios.net')
        #self._set('van', 'dev', 'url', 'http://van-shotgun-staging.methodstudios.net')
        self._set_key('van', 'prod', 'keys', 'default', 'RevolverFind', '11e0e9e88860e93dc750a55c8c2b864a9c0ed37f')
        #self._set_key('van', 'dev', 'keys', 'default', 'mEntity_default', '11e0e9e88860e93dc750a55c8c2b864a9c0ed37f')
        self._set('van', 'prod', 'mongo', dev_mongo)

    def __getitem__(self, k): return self._master[k]
    def __contains__(self, k): return k in self._master

    def _set(self, *indices):
        list_ = list(indices)
        v = list_.pop()
        last = list_.pop()
        table = self[list_.pop(0)]
        for index in list_:
            table = table[index]
        table[last] = v

    def __call__(self, *indices):
        list_ = list(indices)
        table = self[list_.pop(0)]
        for index in list_:
            table = table[index]
        return table

    def _set_key(self, *args):
        args = list(args)
        script_key = args.pop()
        script_name = args.pop()
        keyname = args.pop()
        self(*args)[keyname] = dict(name=script_name, key=script_key)

class BackingStore(object):
    # Need to introduce a persistent cache here:
    #  boote02 has a mongo instance which will need a db called sg_cache, which has collections named:
    # msg_cache.la, msg_cache.van, msg_cache.ny, msg_cache.lon, msg_cache.chi, msg_cache.det, msg_cache.atl
    # There needs to be a control record in each collection which specifies which entities get cached:
    #  msg_cache.la['__controlRecord__']:
    #   { entities: [ Version, Representation, Session ],
    #     time_to_live: <seconds>,
    #     max_size: <doc count>,
    #   }
    # Each cached entity needs a _when_cached entry, and other meta info, ie:
    #   { _when_cached: time.time(),
    #     _valid: True,			# if cached_entity is not valid, reload it from SG/Aux before returning, invalidation can happen via sg_message_daemon
    #     _sg_id: <int>,
    #     <sg_data>,
    #     <?aux_data?>,
    #   }
    # should data be encapsulated, ie: sg_data:{ doc }, aux_data:{ doc }?
    # V0: Don't have aux data be cached, as mongo is probably fast enough;
    BS_PROD = "prod"
    BS_DEV = "dev"
    BS_EXP = "experimental"
    BS_BACKUP = "backup"
    BS_SG = "shotgun"
    BS_PG = "postgres"
    BS_MS = "mysql"
    BS_MO = "mongo"
    HERE = 'van'
    TERMINAL_FAULTS = [ 104, 400, 401, 403, 404, 500 ]
    RETRY_TIME = 0.225
    schema = None
    retries = 0
    def __init__(self, bs_type = "prod", **kwa):
        self._access = Access()
        self._here = BackingStore.HERE
        self.error = None
        url = kwa.get('url', None)
        if bs_type in [ BackingStore.BS_PROD, BackingStore.BS_DEV, 'ha_a', 'ha_b', 'ha_proxy', 'hosted_test' ]:
            self._setup_shotgun(url, bs_type, **kwa)
            self._setup_mongo(bs_type, **kwa)
            # if not BackingStore.schema:  BackingStore.schema = self._db.schema(bs_type)
        elif bs_type == BackingStore.BS_EXP:
            raise NotImplemented("No experimental DB support yet")
        elif bs_type == BackingStore.BS_BACKUP:
            raise NotImplemented("No backup for backing store yet")

    def _setup_shotgun(self, url = None, bs_type = 'prod', **kwa):
        "Setup a connection to the primary production database;"
        here = kwa.get('_alternate', self._here)
        if not url: url = kwa.get('url', self._access(here, bs_type, 'url'))
        name = 'pipeline_infrastructure'
        key = r'x'
        api_key = kwa.get('_key', 'default')
        if api_key not in self._access(here, bs_type, 'keys'):
            raise RuntimeError("Illegal API Key selector %s" % api_key)
        name = self._access(here, bs_type, 'keys', api_key, 'name')
        key  = self._access(here, bs_type, 'keys', api_key, 'key')
        if not url: url = key_url
        self._db = Shotgun(url, name, key)
        self._bstype = BackingStore.BS_SG
        return

    def _setup_mongo(self, mode, **kwa):
        "Setup a connection to the Mongo database;"
        here = kwa.get('_alternate', self._here)
        mongo_info = self._access(here, mode, 'mongo')
        host = kwa.get('host', mongo_info['host'])
        port = kwa.get('port', mongo_info['port'])
        self._mongo = pymongo.Connection(host, port, safe=True)
        self._auxProd = self._mongo[mongo_info['collection']]
        self.auxProd = self._auxProd[mongo_info['table']]

    def __call__(self,  identifier, *args):
        """Access the auxiliary (Mongo) database;
           bs(identifier) -- return cursor to all records where identifier matches;
           bs(identifier, key) -- return value for key in first document found matching identifier;
           bs(identifier, key, value) -- set/save value for key in first document found matching identifier;"""
        if len(args) == 0:
            return self.auxProd.find(identifier)
        elif len(args) == 1:
            doc = self.auxProd.find_one(identifier)
            k = args[0]
            return doc[k]
        elif len(args) == 2:
            key,  value = args
            if '_id' in identifier:
                doc = self.auxProd.find_one({'_id': identifier['_id']})
            else:
                doc = self.auxProd.find_one(identifier)
            doc[key] = value
            print "update doc: ", doc
            doc.pop('_id', None)
            rc = self.auxProd.update(identifier, { "$set": doc }, upsert=False, w=True)
            print "update rc: ", rc
            return rc
        else:
            raise ValueError("Too many arguments in __call__(%s)" % args)

    def schema(self, etype):
        "Return the schema for the current entity type;"
        self._schema = self._db.schema_field_read(etype)
        return self._schema

    def download_stream(self, _id):
        "Support streamed item download;"
        return self._db.download(_id)

    def _retry(self, op, *args,  **kwa):
        "Operation retry support for less reliable network connections;"
        attempts, succ = 0, False
        progress_cb = kwa.get('progress_cb', None)
        while (not succ) and (attempts < 3):
            try:
                rv = op(*args,  **kwa)
                if progress_cb:
                    progress_cb(rv, op, args, kwa)
                return rv
            except Exception, e:
                if 'faultCode' in e.__dict__:
                    # 500=Internal Server Error, 400=Bad Request
                    if e.faultCode in BackingStore.TERMINAL_FAULTS:
                        raise RuntimeError(str(e))
                elif 'errcode' in e.__dict__:
                    print "backing_store error", e.errcode, e.errmsg # raise RuntimeError(str(e.errmsg))
                    pass
                elif 'code' in e.__dict__:
                    code = e.__dict__['code']
                    if code == 5:
                        self.io_error = "Too much data, truncated after %d" % e.__dict__.get('offset', -1)
                        raise RuntimeError(self.io_error)
                elif hasattr(e, 'args'):
                    if e.args:
                        self.io_error = str(e.args[0])
                    raise RuntimeError(str(e))
                    return { }
                attempts += 1
                BackingStore.retries += 1
                time.sleep(BackingStore.RETRY_TIME)
                continue
            raise RuntimeError(str(e))

    def create(self, etype, dct):
        "Create a new entity in the backing store"
        if self._bstype == BackingStore.BS_SG:
            for k in [ 'id',  'type']:
                if k in dct:
                    del dct[k]
            return self._retry(self._db.create, etype, dct)
        else:
            raise NotImplemented("Unsupported backing store type %s" % self._bstype)

    def field_create(self, etype, field_type, display_name, properties_dict):
        "Create a new field for this etype in backing store, caveat emptor: you may not get the column name you want"
        if self._bstype == BackingStore.BS_SG:
            return self._retry(self._db.schema_field_create(etype, field_type, display_name, properties_dict))
        else:
            raise NotImplemented("Unsupported backing store type %s" % self._bstype)

    def field_update(self, etype, field_name, properties_dict):
        "Update the properties for a particular column name within a particular entity type"
        if self._bstype == BackingStore.BS_SG:
            return self._retry(self._db.schema_field_update(etype, field_name, properties_dict))
        else:
            raise NotImplemented("Unsupported backing store type %s" % self._bstype)

    def field_delete(self, etype, field_name):
        "Delete a column for this entity, which will make this column name unusable"
        if self._bstype == BackingStore.BS_SG:
            return self._retry(self._db.schema_field_delete(etype, field_name))
        else:
            raise NotImplemented("Unsupported backing store type %s" % self._bstype)

    def update(self, etype, dct, **kwa):
        "Update an existing entity in the backing store"
        dct.update(kwa)
        _id = dct['id']
        del dct['id']
        if 'type' in dct: del dct['type']
        if not dct: return { }
        if self._bstype == BackingStore.BS_SG:
            if not kwa.get('delete_first',  False):
                return self._retry(self._db.update, etype, _id, dct)
            rc = self.delete(etype,  _id)
            for k in [ 'delete_first', 'id' ]:
                if k in dct:
                    del dct[k]
            return self._retry(self._db.create, etype,  dct)
        else:
            raise NotImplemented("Unsupported backing store type %s" % self._bstype)

    def delete(self, etype, _id):
        if self._bstype == BackingStore.BS_SG:
            return self._retry(self._db.delete, etype, _id)
        elif self._bstype in [ BackingStore.BS_MO ]:
            pass
        else:
            raise NotImplemented("Unsupported backing store type %s" % self._bstype)

    def aux_delete(self, etype, _id):
        identifier = dict(_id=_id)
        return self.auxProd.remove(identifier)

    def read(self, etype, _id,  fields, order=None, filter_operator=None, retired_only=False):
        if self._bstype == BackingStore.BS_SG:
            filters = [['id',  'is',  _id]]
            return self._retry(self._db.find_one, etype, filters,  fields,  order,  filter_operator,  retired_only)
        else:
            raise NotImplemented("Unsupported backing store type %s" % self._bstype)

    def find(self,  etype, filters,  fields,  **kwa):
        "Echos shotguns functionality, without support for retired_only"
        #  array order, string filter_operator, int limit, boolean retired_only, int page
        order = kwa.get('order',  None)
        filter_operator = kwa.get('filter_operator', None)
        limit = kwa.get('limit',  0)
        retired = kwa.get('retired', None)
        # page = kwa.get('page',  0) # Not supported at Method Studios
        if self._bstype == BackingStore.BS_SG:
            return self._retry(self._db.find, etype, filters,  fields,  order, filter_operator, limit, retired)
        else:
            raise NotImplemented("Unsupported backing store type %s" % self._bstype)

    def find_one(self, etype, filters,  fields, order=None, filter_operator=None):
        "Echos shotguns functionality, without support for retired_only"
        if self._bstype == BackingStore.BS_SG:
            return self._retry(self._db.find_one, etype, filters,  fields,  order,  filter_operator)
        else:
            raise NotImplemented("Unsupported backing store type %s" % self._bstype)

    def find_aux(self, etype, filters, fields, **kwa):
        identifier = dict()
        limit = kwa.get('limit', 0)
        # qdict['$maxScan'] = kwa.get('limit', 0) -- BORKED -- only 'scans' limit docs;
        if ('order' in kwa) and kwa['order']:
            query['$order'] = int(kwa['order'])
        identifier['type'] = etype
        for f in fields: identifier[f] = { "$exists":True }
        for f in filters:
            if type(f) != types.DictType: continue
            identifier.update(f)
        query = { "$query":identifier }
        # print "bs.find_aux query", query
        # print "filters", filters, "fields", fields, "kwa", kwa 
        cursor = self.auxProd.find(query)
        if limit > 0:
            cursor = cursor.limit(limit)
        rl = [rec for rec in cursor]
        # if rl: print rl[0]
        return rl

    def find_one_aux(self, etype, filters, fields, order=None, filter_operator=None):
        dct = dict()
        return dct

    def set_session_uuid(self, uuid):
        print dir(self._db)
        print dir(Shotgun) ; return False
        return Shotgun.set_session_uuid(uuid)

    def batch(self, lst):
        if self._bstype == BackingStore.BS_SG:
            return self._retry(self._db.batch, lst)
        else:
            raise NotImplemented("Unsupported backing store type %s" % self._bstype)

    @property
    def primary_host(self):
        return self._db.base_url

if __name__ == "__main__":
    import sys
    def clean_dict(dct):
        for k in dct.keys():
            if not dct[k]: del dct[k]

    def dump(dct):
        print "\n".join(["%s:%s" % (k,  dct[k]) for k in dct])

    def open(mode):
        bs = BackingStore(mode)
        return bs

    def info(txt): print "[%s]" % txt

    def test_0():
        prod_bs = open('prod')
        info("Opened a connection to the prod server on %s" % prod_bs.primary_host)
        dev_bs = open('dev')
        info("Opened a connection to the prod server on %s" % dev_bs.primary_host)

    def test_1():
        bs = open("dev")
        info("Find all projects")
        filters = [[ 'id', 'is_not',  0]]
        fields = [ 'id', 'code']
        rlist = bs.find("Project", filters,  fields,  order=[dict(field_name='code',  direction='asc')])
        print "\n".join(["%-22s: %4d" % ( e['code'],  e['id'] ) for e in rlist if e['code']])

    def test_02m():
        "Test basic prod mongo IO"
        bs = open("prod")
        print "Collections"
        print " ".join(bs._auxProd.collection_names())

    def test_2():
        pipetest_id = 95
        info("Verify a known good object: project id=%d" % pipetest_id)
        etype = "Project"
        pt_dct = dct = bs.read(etype,  pipetest_id,  [ 'id', 'code',  'sg_type',  'users'])
        assert dct['id'] == pipetest_id,  "Retrieved wrong project!"
        assert dct['type'] == etype,  "Wrong etype: %s != %s" % (etype,  dct['type'])
        print "\n".join(["%s:%s" % (k,  dct[k]) for k in dct if type(dct[k]) == types.StringType])
        for k in dct:
            if k == 'users':
                users = [ d['name'] for d in dct[k] ]
                users.sort()
                print "Users: %s" % ",".join(users)

    def test_3():
        dct = { }
        new_project = { 'type':'Project',  'name':'zzScratch_99' }
        dct['project'] = new_project
        dct['sg_best'] = True
        clean_dict(dct)
        rdict = bs.create(etype,  dct)
        print rdict ; dump(rdict) ; sys.exit(0)

    def test_4():
        # rdict['description'] = 'backing store update test'
        rdict = bs.update(etype, rdict, description='backing store update test')

    def test_5():
        # test: Delete that bad boy
        rc = bs.delete(etype,  rdict['id'])
        if not rc:
            print "Delete failed"

    if True:
        test_0()
        test_1()
        test_02m()
