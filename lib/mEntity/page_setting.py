
"Simple PageSetting entity"

if True:
    import m_entity

class PageSetting(m_entity.mEntity):
    DEFAULT = [ 'id', 'code', 'project' ]
    MAP = dict(name='code', created='created_at') # friendly name map, either can be used
    INTERNAL = "PageSetting"
    READ_ONLY = [ 'created_at', 'created_by', 'updated_at', 'updated_by' ]
    def __init__(self,  _id = None,  _code = None, **kwa):
        "Setup translations for calling "
        _type = PageSetting.INTERNAL      # What shotgun calls it, ie: Task, CustomEntity10, etc.
        _map = PageSetting.MAP
        _magicmap = dict()              # map of auto-translating functions
        # No pre super
        self._default = PageSetting.DEFAULT[:] # default fields to load if none specified
        super(PageSetting,  self).__init__(_type,  _map,  _magicmap,  _id,  _code, **kwa) # Et Voila!
        # No post super
        self._class = PageSetting
        self._read_only = PageSetting.READ_ONLY

if __name__ == "__main__":
    import sys, pprint
    def test_cases():
        # pagesetting = PageSetting(None, 'foo') # This should blow up, no such version name
        # id_pagesetting = PageSetting(357) # This should blow up unless there is a version 357 somewhere
        pagesettings = PageSetting().match(projectId=96) # Get all default pagesetting fields for this project
        for pagesetting in pagesettings:
            print str(pagesetting)
    args = sys.argv[:]
    pname = args.pop(0)
    if len(args) < 1:
        test_cases()
        sys.exit(0)
    _id = None
    if args[0] == '-m':
        args.pop(0)
        _id = int(args.pop(0))
        entity = PageSetting(_id)
        entity.parse_keylist(args)
        if _id: rc = entity.update()
        else: rc = entity.create()
        pprint.pprint(rc)
    elif args[0] == '-i':
        args.pop(0)
        _id = int(args.pop(0))
        entity = PageSetting(_id, _fields=['_all'])
        print str(entity)
    sys.exit(0)

