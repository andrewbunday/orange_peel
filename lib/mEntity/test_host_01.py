from host import Host

hosts = {'batman': ['waynemanor', '1.1.1.1', 'waynemanor.gotham.com', 'gotham', [], 'all'],
         'superman': ['dailyplanet', '2.2.2.2', 'dailyplanet.metropolis.com', 'metropolis', [], 'frames'],
         'spiderman': ['dailybugle', '3.3.3.3', 'dailybugle.newyork.com', 'nyc', [], 'movies'],
         }

def create_dummy_hosts():
    for h in ['batman', 'superman', 'spiderman']:
        host = Host()
        host('resident', h)
        host('hostname', hosts[h][0])
        host('ip_address', hosts[h][1])
        host('fqdn', hosts[h][2])
        host('location', hosts[h][3])
        host('sessions', hosts[h][4])
        host('synced_media_type', hosts[h][5])
        print host.auxData()
        host.create()

# Now we assume that we don't anything about which hosts exist.
# So i'd like to query to get back all of them. I've tried something like this, and been failing:
def match_dummy_hosts():
    _host = Host()
    all_hosts = _host.match()
    for h in all_hosts:
        if 'hostname' not in h: continue
        print h
        print h('hostname')

def remove_dummy_hosts():
    all_hosts = Host().match()
    for h in all_hosts:
        if 'resident' not in h: continue
        if h('resident') in hosts:
            h.delete()
    
if __name__ == "__main__":
    import sys
    args = sys.argv[:]
    pname = args.pop(0)
    if '-c' in args:
        create_dummy_hosts()
    if '-m' in args:
        match_dummy_hosts()
    if '-x' in args:
        remove_dummy_hosts()


