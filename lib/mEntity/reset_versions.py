#!/bin/env python

import sys
import time

from project import Project
from version import Version

PROJ_NM = 'zzCatbird'
PROJ_LA = '%s_la' % PROJ_NM
PROJ_VN = '%s_vancouver' % PROJ_NM

la = Project(None, PROJ_LA)
vn = Project(None, PROJ_VN)

args = sys.argv[:]
pname = args.pop(0)
reset_proj = vn
if args:
    nm = args.pop(0)
    if nm == 'vn': reset_proj = vn
    elif nm == 'la': reset_proj = la
    else:
        print "Unknown target %s" % nm
        sys.exit(1)

st = time.time()
try:
    versions = Version().match(project=reset_proj.link, _fields=['id'])
except RuntimeError, e:
    print "Oh crap, too much data! Try paging?", str(e)
    sys.exit(1)
elapsed = time.time() - st
print "[%d versions loaded in %.3f seconds]" % ( len(versions), elapsed )

st = time.time()
for v in versions:
    rc = v.delete()
    if not rc:
        print "Couldn't delete %s:Version %d" % ( proj_link['code'], v['id'] )

elapsed = time.time() - st
print "[%d versions deleted in %.3f seconds]" % ( len(versions), elapsed )
sys.exit(0)

