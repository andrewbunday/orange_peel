#!/bin/env python

import sys
import time

from sdate import SDate

from project import Project
from version import Version

sd = SDate('2013-08-01')
PROJ_NM = 'zzCatbird'
PROJ_LA = '%s_la' % PROJ_NM
PROJ_VN = '%s_vancouver' % PROJ_NM

def remap_frompath(src, dest, k):
    if not src[k]: return
    dest_path = src[k].replace('/jobs/vfx_catbird/', '/jobs/vfx_catbird_la/')
    dest[k] = dest_path
    # print "Remapped %s --> %s" % ( src[k], dest[k] )
    # Repackaging goes here

la = Project(None, PROJ_LA)
vn = Project(None, PROJ_VN)

fields = [ 'name', 'frompath', 'link', 'status', 'artist', 'updated', 'vtype', 'frame_range' ]
st = time.time()
try:
    vn_versions = Version().match(project=vn.link, _fields=fields)
except RuntimeError, e:
    print "Oh crap, too much data! Try paging?", str(e)
    sys.exit(1)
elapsed = time.time() - st
print "[%d versions loaded in %.3f seconds]" % ( len(vn_versions), elapsed )

st = time.time()
for v in vn_versions:
    nv = Version()
    nv['project'] = la.link
    rc = v.remap(nv,
                 name=None, frompath=remap_frompath, status=None, artist=None, vtype=None, frame_range=None,
                 _copy_remaining = False) # No update because the dest object doesn't exist yet
    rc = nv.create()
    if not rc:
        print "Didn't create %s" % nv
elapsed = time.time() - st
print "[%d versions created in %.3f seconds]" % ( len(vn_versions), elapsed )
sys.exit(0)

