
"Simple PermissionRuleSet entity"

if True:
    import m_entity

class PermissionRuleSet(m_entity.mEntity):
    DEFAULT = [ 'id', 'code', 'project' ]
    MAP = dict(name='code', created='created_at') # friendly name map, either can be used
    INTERNAL = "PermissionRuleSet"
    def __init__(self,  _id = None,  _code = None, **kwa):
        "Setup translations for calling "
        _type = PermissionRuleSet.INTERNAL      # What shotgun calls it, ie: Task, CustomEntity10, etc.
        _map = PermissionRuleSet.MAP
        _magicmap = dict()              # map of auto-translating functions
        # No pre super
        self._default = PermissionRuleSet.DEFAULT[:] # default fields to load if none specified
        super(PermissionRuleSet,  self).__init__(_type,  _map,  _magicmap,  _id,  _code, **kwa) # Et Voila!
        # No post super
        self._class = PermissionRuleSet

if __name__ == "__main__":
    import sys, pprint
    def test_cases():
        # permissionruleset = PermissionRuleSet(None, 'foo') # This should blow up, no such version name
        # id_permissionruleset = PermissionRuleSet(None, 357) # This should blow up unless there is a version 357 somewhere
        permissionrulesets = PermissionRuleSet().match(projectId=96) # Get all default permissionruleset fields for this project
        for permissionruleset in permissionrulesets:
            print str(permissionruleset)
    args = sys.argv[:]
    pname = args.pop(0)
    if len(args) < 1:
        test_cases()
        sys.exit(0)
    _id = None
    if args[0] == '-m':
        args.pop(0)
        _id = int(args.pop(0))
        entity = PermissionRuleSet(_id)
        entity.parse_keylist(args)
        if _id: rc = entity.update()
        else: rc = entity.create()
        pprint.pprint(rc)
    elif args[0] == '-i':
        args.pop(0)
        _id = int(args.pop(0))
        entity = PermissionRuleSet(_id, _fields=['_all'])
        print str(entity)
    sys.exit(0)

