
"Simple Permission entity"

if True:
    import m_entity

class Permission(m_entity.mEntity):
    DEFAULT = [ 'id', 'code', 'project' ]
    MAP = dict(display='display_name', name='code') # friendly name map, either can be used
    INTERNAL = "PermissionRuleSet"
    READ_ONLY = [ 'created_at', 'created_by', 'updated_at', 'updated_by' ]
    def __init__(self,  _id = None,  _code = None, **kwa):
        "Setup translations for calling "
        _type = Permission.INTERNAL      # What shotgun calls it, ie: Task, CustomEntity10, etc.
        _map = Permission.MAP
        _magicmap = dict()              # map of auto-translating functions
        # No pre super
        self._default = Permission.DEFAULT[:] # default fields to load if none specified
        super(Permission,  self).__init__(_type,  _map,  _magicmap,  _id,  _code, **kwa) # Et Voila!
        self._ename = 'Permission'
        self._class = Permission
        self._read_only = Permission.READ_ONLY

if __name__ == "__main__":
    import sys, pprint
    def test_cases():
        # permissionruleset = Permission(None, 'foo') # This should blow up, no such version name
        # id_permission = Permission(357) # This should blow up unless there is a version 357 somewhere
        permissions = Permission().match(projectId=96) # Get all default permission fields for this project
        for permission in permissions:
            print str(permission)
    args = sys.argv[:]
    pname = args.pop(0)
    if len(args) < 1:
        test_cases()
        sys.exit(0)
    _id = None
    if args[0] == '-m':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Permission(_id)
        entity.parse_keylist(args)
        if _id: rc = entity.update()
        else: rc = entity.create()
        pprint.pprint(rc)
    elif args[0] == '-i':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Permission(_id, _fields=['_all'])
        print str(entity)
    sys.exit(0)

