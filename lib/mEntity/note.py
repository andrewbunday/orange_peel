
"Simple Note entity"

if True:
    import m_entity

class Note(m_entity.mEntity):
    DEFAULT = [ 'id', 'project', 'content', 'subject', 'author' ]
    MAP = dict(author='user', body='content', links='note_links', latest='sg_latest_note',
               parent='entity', link='entity',
               note_type = 'sg_note_type', created='created_at', updater='updated_by')
    INTERNAL = "Note"
    READ_ONLY = [ 'created_at', 'created_by', 'updated_at', 'updated_by' ]
    def __init__(self,  _id = None,  _code = None, **kwa):
        "Setup translations for calling "
        _type = Note.INTERNAL      # What shotgun calls it, ie: Task, CustomEntity10, etc.
        _map = Note.MAP
        _magicmap = dict()              # map of auto-translating functions
        # No pre super
        self._default = Note.DEFAULT[:] # default fields to load if none specified
        super(Note,  self).__init__(_type,  _map,  _magicmap,  _id,  _code, **kwa) # Et Voila!
        # No post super
        self._class = Note
        self._read_only = Note.READ_ONLY

if __name__ == "__main__":
    import sys, pprint
    def test_cases():
        # note = Note(None, 'foo') # This should blow up, no such name
        # id_note = Note(357) # This should blow up unless there is a 357 on this system
        notes = Note().match(projectId=96) # Get all default note fields for this project
        for note in notes:
            print str(note)
    args = sys.argv[:]
    pname = args.pop(0)
    if len(args) < 1:
        test_cases()
        sys.exit(0)
    _id = None
    if args[0] == '-m':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Note(_id)
        entity.parse_keylist(args)
        if _id: rc = entity.update()
        else: rc = entity.create()
        pprint.pprint(rc)
    elif args[0] == '-i':
        args.pop(0)
        _id = int(args.pop(0))
        entity = Note(_id, _fields=['_all'])
        print str(entity)
    sys.exit(0)
