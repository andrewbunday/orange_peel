"""
:Copyright:
    2013 Method Studios. All rights reserved

:Contact:
    andrew.bunday@methodstudios.com

:Synopsis:
    REST control and query interface to the SessionManager
"""

import flask
from cherrypy import wsgiserver

app = flask.Flask(__name__)

@app.route('/', methods=["GET"])
def index():
    return flask.render_template("index.html")

def start(*args, **kwargs):
    """
    Start the Flask web application.
    """

    global server

    address = kwargs.get('address', '0.0.0.0')
    port = kwargs.get('port', '5000')

    dispatcher = wsgiserver.WSGIPathInfoDispatcher({'/': app})
    server = wsgiserver.CherryPyWSGIServer((address, port, dispatcher))

    try:
        server.start()
    except KeyboardInterrupt:
        logger.info('shutting down')
        server.stop()
