"""
:Copyright:
    2013 Method Studios. All rights reserved

:Contact:
    andrew.bunday@methodstudios.com

:Synopsis:

"""

import flask
from cherrypy import wsgiserver

app = flask.Flask(__name__)

def start(*args, **kwargs):
    """
    Start the Flask web application.
    """
    global server

    address = kwargs.get('address', '127.0.0.1')
    port = kwargs.get('port', 8000)

    # dispatcher = wsgiserver.WSGIPathInfoDispatcher({'/': app})
    # server = wsgiserver.CherryPyWSGIServer((address,
    #                                         port),
    #                                         dispatcher)
    app.run(address, port, debug=True)

    try:
        server.start()
    except KeyboardInterrupt:
        server.stop()