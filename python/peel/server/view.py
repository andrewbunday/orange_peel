"""
:Copyright:
    2013 Method Studios. All rights reserved

:Contact:
    andrew.bunday@methodstudios.com

:Synopsis:

"""

import flask

#----------------------------------------------------------------------------------------------------------------------
# Application imports
#----------------------------------------------------------------------------------------------------------------------

from service import app

#----------------------------------------------------------------------------------------------------------------------
# View Endpoints
#----------------------------------------------------------------------------------------------------------------------

@app.route('/', methods=["GET"])
def index():
    return flask.render_template("index.html")

@app.route('/settings', methods=["GET"])
def settings():
    return flask.render_template("settings.html")

@app.route('/subscriptions', methods=["GET"])
def subscriptions():
    return flask.render_template("subscriptions.html")

@app.route('/sessions', methods=["GET"])
def sessions():

    data = [{'name': 'catbird',
             'user': 'abunday',
             'date': '9pm',
             'id': '4985749579275948',
             'playlists': [{'name': 'animation', 'id': 1},
                           {'name': 'afternoon dailies', 'id': 2},
                           {'name': 'morning dailies', 'id': 3},
                           {'name': 'client review', 'id': 4} ]
             },

            {'name': 'vancouver/ny',
             'user': 'abunday',
             'date': '3pm',
             'id': '0697806978059487',
             'playlists': [{'name': 'robocop', 'id': 5},
                           {'name': 'NATM', 'id': 6},
                           {'name': 'morning dailies', 'id': 7}]
             },

            {'name': 'animation',
             'user': 'bob',
             'date': '1am',
             'id': '847934764084',
             'playlists': [{'name': 'layout', 'id': 8},
                           {'name': 'finals', 'id': 9},
                           {'name': 'client review', 'id': 10}]
            },
            ]

    return flask.render_template("sessions.html", data=data)

@app.route('/hosts', methods=["GET"])
def hosts():

    data = {'hosts': {'va1ws5122': {'site': 'van'},
                      'ln1ws5143': {'site': 'lon'},
                      'ny1ws4323': {'site': 'nyc'},
                      },
            }

    return flask.render_template("hosts.html", data=data)

@app.route('/static/<path:filename>')
def send_static(filename):
    return flask.send_from_directory('./static/', filename)

@app.route('/ajax/', methods=["GET"])
def list_api():

    links = []
    for rule in app.url_map.iter_rules():
        # Filter out rules we can't navigate to in a browser
        # and rules that require parameters

        print rule
        print dir(rule)

        print rule.methods
        print rule.defaults
        print rule.arguments

        # if "GET" in rule.methods and len(rule.defaults) >= len(rule.arguments):
        #     url = url_for(rule.endpoint)
        #     if re.match('.*\.json$', url):
        #         links.append((url, rule.endpoint))

    return flask.render_template("api.html")
