"""
:Copyright:
    2013 Method Studios. All rights reserved

:Contact:
    andrew.bunday@methodstudios.com

:Synopsis:
    REST control and query interface to the SessionManager
"""

import service
import view
import ajax