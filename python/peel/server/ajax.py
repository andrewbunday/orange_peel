"""
:Copyright:
    2013 Method Studios. All rights reserved

:Contact:
    andrew.bunday@methodstudios.com

:Synopsis:

"""

import flask
from flask.views import MethodView
from flask import request
from flask import json

#----------------------------------------------------------------------------------------------------------------------
# Application imports
#----------------------------------------------------------------------------------------------------------------------

from service import app
from database import Database

#----------------------------------------------------------------------------------------------------------------------
# AJAX Endpoints
#----------------------------------------------------------------------------------------------------------------------

@app.route('/ajax/sessions.json', methods=["GET"])
def get_sessions():
    return flask.Response(Database.get_sessions(), status=200, content_type='application/json')

@app.route('/sessions/<int:sid>/<int:pid>', methods=["GET"])
def playlist_detail(sid, pid):
    sname = request.args.get('sname', '')
    pname = request.args.get('pl', '')
    return flask.Response(json.dumps({'session':sname,'playlist':pname}), status=200, content_type='application/json')