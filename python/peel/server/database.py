"""
:Copyright:
    2014 Method Studios. All rights reserved

:Contact:
    andrew.bunday@methodstudios.com

:Synopsis:
    server/database.py - keeps all of the data marshalling between mEntity and webapp
    contained in a single module.
"""

from bson import json_util
from mEntity.session import Session


class Database(object):
    """
    mEntity database interaction class. Provides the basis for the creation of views.
    Callable from any of the view method/classes.

    By convention all of the methods return lists including queries for a single item.
    """

    @classmethod
    def sessions(cls):
        """
        @returns...
        """
        response = []
        for session in Session().match_aux(_fields=['_all']):
            response.append(json_util.dumps(session.auxData()))

        return response

    @classmethod
    def sessions(cls, id):
        """
        @returns - details about a given session, session is selected by ObjectID
        """
        return [{}]

    @classmethod
    def hosts(cls):
        """
        @returns a list of all of the known hosts, id and name
        """
        return []

    @classmethod
    def hosts(cls, id):
        return []

    @classmethod
    def playlists(cls, session):
        """
        @returns a list of playlist ids and names for a given session
        """
        return []

    @classmethod
    def playlists(cls, session, id):
        return []

